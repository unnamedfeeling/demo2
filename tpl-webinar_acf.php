<?php /* Template Name: Вебинар (acf) */
global $post, $options, $tpl, $pmeta, $p, $design, $author, $ameta;

switch (true) {
	case (!empty($design)):
		get_header($design);
		break;

	default:
		get_header();
		break;
}

$pmeta=(empty($pmeta))?get_post_meta( $post->ID, '', false ):$pmeta;
// print_r($pmeta);
$author=$ameta=null;
if (!empty($pmeta[$p.'webauthor'][0])&&$pmeta[$p.'webauthor'][0]!==0&&$pmeta[$p.'webauthor'][0]!=='none') {
	$author=get_post( $pmeta[$p.'webauthor'][0], OBJECT, 'raw' );
	$ameta=get_post_meta( $pmeta[$p.'webauthor'][0], '', false );
}

	if (have_posts()): while (have_posts()) : the_post();

		if (!empty($pmeta['_wds_builder_template-'.$design][0])) {
			$parts=get_field( '_wds_builder_template-'.$design );
			// print_r($parts);
			$i=1;
			if (count($parts)>0&&!empty($parts[0]['block'])&&!empty($parts[0]['block']['template_group'])) {
				$form=(!empty($pmeta[$p.'regform'][0]))?get_post($pmeta[$p.'regform'][0]):null;
				$endTime=(!empty($pmeta[$p.'webinardate'][0]))?strtotime($pmeta[$p.'webinardate'][0]):0;

				if (!empty($pmeta[$p.'timezone'][0])) {
					$tz=new DateTimeZone($pmeta[$p.'timezone'][0]);
					date_default_timezone_set($tz->getName());
				}

				foreach ($parts as $key => $val) {
					$val=$val['block'];
					if ($val['template_group']=='none'&&$val['template_group']=='') {
						continue;
					}

					switch (true) {
						case (!empty($val[$p.'_blockTtl'])||!empty($val[$p.'_blockCont'])||!empty($val[$p.'_blockStyle'])||!empty($val[$p.'_blockImg'])||!empty($val[$p.'_blockVideoId'])):
							$blockTtl=(!empty($val[$p.'_blockTtl']))?$val[$p.'_blockTtl']:null;
							$blockCont=(!empty($val[$p.'_blockCont']))?$val[$p.'_blockCont']:null;
							$style=(!empty($val[$p.'_blockStyle']))?' style="'.$val[$p.'_blockStyle'].'"':null;
							$img=(!empty($val[$p.'_blockImg']))?wp_get_attachment_image_src( $val[$p.'_blockImg']['ID'], 'large', false )[0]:'//via.placeholder.com/463x560.png';
							$video=[
								'id'=>(!empty($val[$p.'_blockVideoId']))?$val[$p.'_blockVideoId']:null,
								'type'=>(!empty($val[$p.'_blockVideoType']))?$val[$p.'_blockVideoType']:null,
								'img'=>(!empty($val[$p.'_blockVideoImg']))?wp_get_attachment_image_src( $val[$p.'_blockVideoImg']['ID'], 'large', false )[0]:null,
								'modaltype'=>'vid'
							];
							break;

						default:
							$blockTtl=(!empty($val[$p.$val['template_group'].'_blockTtl']))?$val[$p.$val['template_group'].'_blockTtl']:null;
							$blockCont=(!empty($val[$p.$val['template_group'].'_blockCont']))?$val[$p.$val['template_group'].'_blockCont']:null;
							$style=(!empty($val[$p.$val['template_group'].'_blockStyle']))?' style="'.$val[$p.$val['template_group'].'_blockStyle'].'"':null;
							$img=(!empty($val[$p.$val['template_group'].'_blockImg']))?wp_get_attachment_image_src( $val[$p.$val['template_group'].'_blockImg']['ID'], 'large', false )[0]:'//via.placeholder.com/463x560.png';
							$video=[
								'id'=>(!empty($val[$p.$val['template_group'].'_blockVideoId']))?$val[$p.$val['template_group'].'_blockVideoId']:null,
								'type'=>(!empty($val[$p.$val['template_group'].'_blockVideoType']))?$val[$p.$val['template_group'].'_blockVideoType']:null,
								'img'=>(!empty($val[$p.$val['template_group'].'_blockVideoImg']))?wp_get_attachment_image_src( $val[$p.$val['template_group'].'_blockVideoImg']['ID'], 'large', false )[0]:'//via.placeholder.com/960x475.png',
								'modaltype'=>'vid'
							];
							break;
					}

					switch ($video['type']) {
						case 'youtube':
							$video['url']='https://www.youtube.com/watch?v=';
							break;

						default:
							$video['url']='https://home.wistia.com/medias/';
							$video['modaltype']='wistia';
							break;
					}

					// print_r($blockCont);
					include(get_stylesheet_directory().'/assets/php/blocks/acf/'.$pmeta[$p.'design'][0].'/part-'.$val['template_group'].'.php');
					$i++;
				}
			}
		}

		if (!empty($post->post_content)) {
			get_template_part( '/assets/php/blocks/block', 'content' );
		}

		switch (true) {
			case (!empty($pmeta[$p.'custom_css'][0])):
				printf('<style>%s</style>', $pmeta[$p.'custom_css'][0]);
			case (!empty($pmeta[$p.'custom_js'][0])):
				printf('<script>%s</script>', $pmeta[$p.'custom_js'][0]);
			case (!empty($pmeta[$p.'custom_js_tag'][0])):
				echo $pmeta[$p.'custom_js_tag'][0];
			case (!empty($pmeta[$p.'custom_noscript'][0])):
				echo $pmeta[$p.'custom_noscript'][0];
		}

		if (!empty($pmeta[$p.'segmentApiKey'][0])) {
			printf('<script>hbApp={segmentKey:"%s"}</script>', $pmeta[$p.'segmentApiKey'][0]);
		}

	endwhile;
	else: ?>
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'heartweb' ); ?></h2>
	</article>
	<?php endif;

switch (true) {
	case (!empty($design)):
		get_footer($design);
		break;

	default:
		get_footer();
		break;
}

date_default_timezone_set('Europe/London');
