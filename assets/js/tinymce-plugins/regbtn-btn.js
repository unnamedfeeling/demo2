(function() {
  // add button to insert regbtn shortcode
  tinymce.PluginManager.add('heartwebRegButton', function( editor, url ) {
    // Add a button that opens a window
    editor.addButton('heartwebRegButton', {
      text: 'Insert registration button',
      onClick: function () {
        // Open window
        openDialog({editor: editor, type: 'regbutton'});
      }
    });
  });
})();