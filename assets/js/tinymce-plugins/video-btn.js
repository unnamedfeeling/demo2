(function() {
  // add button to insert video shortcode
  tinymce.PluginManager.add('heartwebVideo', function( editor, url ) {
    // Add a button that opens a window
    editor.addButton('heartwebVideo', {
      text: 'Insert video',
      onClick: function () {
        // Open window
        openDialog({editor: editor, type: 'video'});
      }
    });
  });
})();