var docData=document.body.dataset,
	countryData,
	countryCode,
	webinarType=docData.type,
	webID=docData.webid,
	scriptsLoaded=[],
	scriptCb=function(params){
		if (typeof params=='undefined') {
			return console.log('No params provided for scriptCb');
		}
		var callback=params.el.cb,
			cbparams=(typeof params.el.cbparams!=='undefined')?params.el.cbparams:null;
		if (typeof params.script!=='undefined') {
			if (params.script.readyState) { // IE, incl. IE9
				params.script.onreadystatechange = function() {
					if (params.script.readyState == "loaded" || params.script.readyState == "complete") {
						params.script.onreadystatechange = null;
						if (typeof callback!=='undefined') callback(cbparams);
						return scriptsLoaded.push(params.el.id);
					}
				};
			} else {
				params.script.onload = function(){
					if (typeof callback!=='undefined') callback(cbparams);
					return scriptsLoaded.push(params.el.id);
				};
			}
		} else {
			var scriptIsLoaded=(scriptsLoaded.indexOf(params.el.id) > -1);
			if(typeof params.el.cb!=='undefined'&&params.el.cb !== null&&scriptIsLoaded){
				if (typeof callback!=='undefined') callback(cbparams);
			} else {
				setTimeout(function(){scriptCb(params)}, 500);
			}
		}
	},
	// usage: loadscripts([{id: String elid, src: String elsrc, cb: function func, cbparams: Object params}]);
	// checks if script is loaded allready by id and writes back to array scriptsLoaded
	loadscripts=function (arr){
		var params, i;
		for (i = 0; i < arr.length; i++) {
			if (document.getElementById(arr[i].id)==null&&scriptsLoaded.indexOf(arr[i].id) < 0) {
				var s=arr[i],
					script = document.createElement('script');
				script.id=s.id;
				script.src = s.src;
				scriptCb({el:s, script:script});
				document.getElementsByTagName('head')[0].appendChild(script);
			} else {
				scriptCb({el:arr[i]});
			}
		}
	},
	// usage: loadstyles([{id: String elid, src: String elsrc}]);
	loadstyles=function(arr){
		for (var i = 0; i < arr.length; i++) {
			if (document.getElementById('#'+arr[i].id)==null) {
				var head = document.getElementsByTagName('head')[0],
					lnk = document.createElement('link'),
					func = function () {
						head.appendChild(lnk);
					};
				lnk.rel = 'stylesheet';
				lnk.id = arr[i].id;
				lnk.href = arr[i].src;

				var img = document.createElement('img');
				img.onerror = function(){
					func();
				}
				img.src = arr[i].src;
			}
		}
	},
	sendData=function (data, opts) {
		var XHR = new XMLHttpRequest();
		// var urlEncodedData = data;
		var FD  = new FormData();
		var dfr=$.Deferred();
		var res='';

		// Define what happens on successful data submission
		XHR.addEventListener('loadend', function(event) {
			if (XHR.status != 200) {
				res=XHR.status + ': ' + XHR.statusText;
				dfr.reject(res);
			} else {
				res=XHR.responseText;
				dfr.resolve(res);
			}
			// console.log(res);
			// console.log(event);
		});

		// Define what happens in case of error
		XHR.addEventListener('error', function(event) {
			dfr.reject(XHR);
			// console.log(event);
		});

		// Push our data into our FormData object
		for(name in data) {
			FD.append(name, data[name]);
		}

		// Set up our request
		opts.url=(typeof opts.url !== 'undefined')?opts.url:null;
		opts.type=(typeof opts.type !== 'undefined')?opts.type:'POST';
		XHR.open(opts.type, opts.url);

		// Add the required HTTP header for form data POST requests
		// XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

		// Finally, send our data.

		// console.log(data);
		XHR.send(FD);
		return dfr.promise();
	},
	apngTest=function() {
		"use strict";
		var apngTest = new Image(),
			ctx = document.createElement("canvas").getContext("2d"),
			d=$.Deferred();
		apngTest.onload = function () {
			ctx.drawImage(apngTest, 0, 0);
			self.APNG = ( ctx.getImageData(0, 0, 1, 1).data[3] === 0 );
			d.resolve();
		};
		apngTest.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAACGFjVEwAAAABAAAAAcMq2TYAAAANSURBVAiZY2BgYPgPAAEEAQB9ssjfAAAAGmZjVEwAAAAAAAAAAQAAAAEAAAAAAAAAAAD6A+gBAbNU+2sAAAARZmRBVAAAAAEImWNgYGBgAAAABQAB6MzFdgAAAABJRU5ErkJggg==";
		// frame 1 (skipped on apng-supporting browsers): [0, 0, 0, 255]
		// frame 2: [0, 0, 0, 0]
		return d.promise();
	},
	urlParams,
	setFormValsFromUrl=function(params){
		if (typeof params.data=='undefined') {
			return console.log('No data provided to set');
		}
		$('form').each(function(index, el) {
			var form=$(this);
			for (var key in params.data) {
				var re= /(hb_|om_)/,
					fieldName=(key.indexOf('_name')>0)?'userName':key.replace(re, ''),
					elem=form.find('[name="'+fieldName+'"]');
				if (elem.length&&elem.val()=='') {
					if (elem.hasClass('js-phone')) {
						 continue;
					}
					elem.val(urlParams[key]);
				}
			}
		});
	},
	formatDate=function(params){
		if (typeof params=='undefined') {
			return 'Wrong params for formatDate'
		}
		var date = (params.date!==undefined)?params.date : new Date();
		var dd = date.getDate();

		var mm = date.getMonth()+1;
		var yyyy = date.getFullYear();
		var delimiter=(params.delimiter!==undefined)?params.delimiter:'.'
		if(dd<10)
		{
		    dd='0'+dd;
		}

		if(mm<10)
		{
		    mm='0'+mm;
		}

		var result=dd+delimiter+mm+delimiter+yyyy

		return result
	},
	formatTime=function(params){
		if (typeof params=='undefined') {
			return 'Wrong params for formatTime'
		}
		var date = (params.date!==undefined)?params.date : new Date();
		var hh = date.getHours();

		var mm = date.getMinutes();
		var delimiter=(params.delimiter!==undefined)?params.delimiter:'.'

		if(mm<10)
		{
		    mm='0'+mm;
		}

		var result=hh+delimiter+mm

		return result
	},
	insertAfter=function (newElement,targetElement) {
    // target is what you want it to go after. Look for this elements parent.
    var parent = targetElement.parentNode;

    // if the parents lastchild is the targetElement...
    if (parent.lastChild == targetElement) {
        // add the newElement after the target element.
        parent.appendChild(newElement);
    } else {
        // else the target has siblings, insert the new element between the target and it's next sibling.
        parent.insertBefore(newElement, targetElement.nextSibling);
    }
	},
	Timer=(typeof easytimer!=='undefined')?easytimer.Timer:undefined,
	timer = new Timer(),
	dateNow=new Date(),
	timeNow=Math.ceil(dateNow.getTime()/1000),
	timerDateObj=function(endTime){
		if (endTime<timeNow) {
			return {days: 0, hours: 0, minutes: 0, seconds: 0}
		}

		var diff=endTime-timeNow,
			d=Math.floor(diff/86400),
			h=Math.floor((diff-d*86400)/3600),
			m=Math.floor((diff-d*86400-h*3600)/60),
			s=Math.floor(diff-d*86400-h*3600-m*60),
			tObj={
				days: d,
				hours: h,
				minutes: m,
				seconds: s
			}

		return tObj
	};

	(window.onpopstate = function () {
		var match,
			pl     = /\+/g,  // Regex for replacing addition symbol with a space
			search = /([^&=]+)=?([^&]*)/g,
			decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
			query  = window.location.search.substring(1);

		urlParams = {};
		while (match = search.exec(query))
		urlParams[decode(match[1])] = decode(match[2]);
	})();

// console.log(urlParams);

// $(document).on('click', '.js-ancor', function (event){
// 	// console.log($(this).attr('href'));
// 	// console.log($(this).attr('href').length);
// 	if ((typeof $(this).attr('href')!=='undefined'&&$(this).attr('href').length<2)||typeof $(this).data('href')=='undefined') {
// 		event.preventDefault();
// 	}
// 	var id=(typeof $(this).data('href')=='undefined')?$(this).attr('href'):$(this).data('href');
// 	$('html, body').animate({
// 		scrollTop: $(id).offset().top-$('header').innerHeight()-20
// 	}, 1000);
// 	event.preventDefault();
// });
$(document).on('click', '.js-ancor', function (event){
	event.preventDefault();
	var id=(typeof $(this).data('target')!=='undefined')?$(this).data('target'):$(this).attr('href');
	if (typeof id=='undefined'||id=='undefined') {
		return false;
	}
	$('html, body').animate({
		scrollTop: $(id).offset().top-$('header').innerHeight()-20
	}, 1000);
});
$(document).on('click', '.js-gotohref', function (event){
	// console.log($(this).attr('href'));
	// console.log($(this).attr('href').length);
	if (typeof $(this).data('href')!=='undefined'&&$(this).data('href').length<2) {
		return;
	}
	window.location.href=$(this).data('href');
	event.preventDefault();
});

$(document).on('click', '.js-callModal', function(event){
	$.magnificPopup.open({
		items: {
			src: $(event.target.dataset.mfpSrc),
			type: 'inline'
		},
		midClick: true,
		fixedContentPos: 'true',
		overflowY: 'scroll',
		callbacks: {
			open: function(){
				document.body.classList.add('modal-open');
			},
			close: function(){
				document.body.classList.remove('modal-open');
			}
		}
	});

});


$(document).ready(function(){
	if ($('#wpadminbar').length) {
		$('header').css('top', document.getElementById('wpadminbar').clientHeight+'px');
	}

	var telInput = $(".js-phone");
		// errorMsg = $("#error-msg"),
		// validMsg = $("#valid-msg");

	// initialise plugin
	var reset = function(elem) {
		// console.log(elem);
		elem.closest('.form-group').removeClass("error");
		// errorMsg.addClass("hide");
		// validMsg.addClass("hide");
	},
	initTelInputs=function(){
		telInput.each(function(){
			var th=$(this);
			th.intlTelInput({
				allowDropdown: true,
				initialCountry: 'auto',
				geoIpLookup: function(callback) {
					// $.get("https://ipinfo.io", function() {}, "jsonp").always(function(resp) {

						// if (countryCode!=='UA') {
						// 	window.addEventListener('load', function(){
						// 		(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-206992-aQJM1';
						// 	})
						// }
						callback(countryCode);
				},
				// utilsScript: ajax_func.tpld+"/assets/js/utils.js"
			});

			setTimeout(function(){
				if (typeof urlParams.hb_phone!=='undefined') {
					th.val(urlParams.hb_phone);
					th.blur();
				}
			}, 700)

			// on blur: validate
			th.blur(function() {
				reset(th);
				// console.log(th);
				var errString='Ваш телефон не принят потому что содержит ошибки! Мы зарегистрируем Вас без телефона!';
				if ($.trim(th.val())) {
					// console.log(th.val());
					if (th.intlTelInput("isValidNumber")) {
						// validMsg.removeClass("hide");
						th.closest('.form-group').removeClass("error");
						$('.js-addinfo').text($('.js-addinfo').text().replace(errString, ''));
						$('.js-countryData').val(JSON.stringify(th.intlTelInput("getSelectedCountryData")));
						// telInput.closest('form').find('.js-phonedata').val(telInput.intlTelInput("getNumber", intlTelInputUtils.numberFormat.NATIONAL).replace(/\s/g, ''));
						th.closest('form').find('.js-phonedata').val(th.intlTelInput("getNumber"));
					} else {
						th.closest('.form-group').addClass("error");
						th.closest('form').find('.js-phonedata').val('');
						if ($('.js-addinfo').text().search(errString)<1) {
							$('.js-addinfo').append('<br>'+errString);
						}
						// errorMsg.removeClass("hide");
					}
				}
			});

			// on keyup / change flag: reset
			th.on("keyup change", function(){reset(th)});
		})
	},
	initPageData=function(){
		initTelInputs()

		if (typeof urlParams!=='undefined'&&$('form').length) {
			setFormValsFromUrl({data:urlParams});
		}


		var msg={
				// func: (webID.indexOf(',')>0)?'webinar-multiple':'webinar',
				webid: (typeof webID!=='undefined')?webID:'',
				postId:(typeof docData.pid!=='undefined')?docData.pid:'',
				webinarType: webinarType,
			},
			initData=sendData(msg, {url:'/wp-json/heartweb/v1/webinarData', type:'POST'});

		$.when( initData ).then(
			function( result ) {
				result=JSON.parse(result)

				var resData=result.data,
					webinarData=resData.sessions,
					start_time,
					forms=document.querySelectorAll('.hf-form'),
					formsParams={
						postId: document.body.dataset.pid,
						webid: document.body.dataset.webid,
						webinarName: '',
					}

				if (webinarData.length<1) {
					console.error('No webinar data was retrieved!');
					outputRegistrationEnd(forms)
					return false
				}

				switch (true) {
					case (msg.webinarType=='webinarjam'&& typeof webinarData.webinar!=='undefined'):
						webName=webinarData.webinar.name;
						formsParams.webinarName=webName
						start_time=(typeof webinarData.start_time!=='undefined')?webinarData.start_time:''
						break;
					// case (msg.webinarType=='bigmarker'&& typeof webinarData.conference_address!=='undefined'):
					// 	webName=webinarData.title;
					// 	formsParams.webinarName=webName
					// 	start_time=(typeof webinarData.start_time!=='undefined')?webinarData.start_time:''
					// 	break;
					case (msg.webinarType=='bigmarker'&&webinarData.length>0):
						webName=webinarData[0].sessionData.title;
						formsParams.webinarName=webName
						webID=webinarData[0].id;
						start_time=(typeof webinarData[0].sessionData.start_time!=='undefined')?webinarData[0].sessionData.start_time:''
						break;
					default:
						webName=''
						start_time=''
				}

				if (webinarData.length>1) {
					var firstWebinarTime=new Date(webinarData[0].sessionData.start_time)

					if (dateNow.getHours()<firstWebinarTime.getHours()) {
						setupWebinarSelect(forms, webinarData)
					}
				}

				formsParams.webid=webinarData[0].session

				var arr=resData.webid.split(','),
					skipped=false,
					nextwebinar={}

				for (var i = 0; i < webinarData.length; i++) {
					var elem=webinarData[i],
						regCount='<h3>Занято мест: '+elem.regCount+' из '+elem.limit+'</h3>'
					if (elem.limit<=elem.regCount) {
						if (i==(webinarData.length-1)) {
							outputRegistrationEnd(forms);
							break;
						}

						skipped=true
						nextwebinar=elem
						continue;
					}

					if (typeof nextwebinar!=='undefined') {
						formsParams={
							postId: (nextwebinar.nextwebinarId!==undefined)?nextwebinar.nextwebinarId:formsParams.postId,
							webid: elem.session,
							// webinarName: nextwebinar.nextwebinarName,
							webinarName: (nextwebinar.nextwebinarName!==undefined)?nextwebinar.nextwebinarName:elem.sessionData.title,
						}

						if (typeof nextwebinar.nextwebinarLink!=='undefined') {
							var text=$('<h4 class="text-center">Регистрация на этот вебинар окончена. Но мы можем вас зарегистрировать на вебинар <a href="'+nextwebinar.nextwebinarLink+'" target="_blank">'+nextwebinar.nextwebinarName+'</a> прямо сейчас</h4>')[0],
								nwebId=nextwebinar.nextwebinarSessionId,
								im=i+1;

							if (im<=(webinarData.length-1)) {
								regCount='<h3>Занято мест: '+webinarData[im].regCount+' из '+webinarData[im].limit+'</h3>'
								$('.js-regCount').html(regCount)
							}

							for (var x = 0; x < forms.length; x++) {
								var txt=text.cloneNode(true);
								forms[x].parentNode.insertBefore(txt, forms[x])
							}

							break;
						}

						$('.js-regCount').html(regCount)

						break;

					}
				}

				setupFormsData(formsParams);

				var date=(start_time!=='') ? new Date(start_time) : ''
				if ($('.js-timer').length&&webinarData!==undefined) {
					$('.js-timer').each(function(index, el) {
						// if (this.dataset.dend==='0') {
							this.dataset.dend=date.getTime()/1000
						// }
						if (this.dataset.dend>timeNow) {
							initTimer({target: this, endDate: timerDateObj($(this).data('dend'))})
						} else {
							this.innerHTML=''
						}
					});
				}

				if (date!=='') {
					var dateEls=document.querySelectorAll('.js-webinarDate')

					forms.forEach(function(elem, i, arr){
						var timeInp=document.createElement('input')
						timeInp.type='hidden'
						timeInp.value=date.getTime()/1000

						elem.appendChild(timeInp)
					})

					if (dateEls.length) {
						outputWebinarDate({target: dateEls, start: date, countryData: ''})
					}
				}
			},
			function( result ) {
				// console.log('2');
				console.log(result);
			}
		).always(function(){
			$('.js-preloader').fadeOut('500');
		});
	},
	countryCookie=getCookie('countryData')

	if (countryCookie==undefined) {
		$.getJSON('https://api.ipgeolocation.io/ipgeo?apiKey=493630a2c7b24325a3265499d1419473', function(resp) {
			countryData=resp;
			setCookie('countryData', JSON.stringify(countryData), {path: '/'})
			countryCode = (resp && resp.country_code2) ? resp.country_code2 : "UA";
		}).always(function(){
			initPageData()
		})
	} else {
		countryData=JSON.parse(countryCookie)
		countryCode = (countryData && countryData.country_code2) ? countryData.country_code2 : "UA";

		initPageData()
	}


});

