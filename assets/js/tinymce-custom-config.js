var openDialog = function (params) {
  var options={
    video:{
      title: 'Instert custom video into content',
      body: [
        {
          type: 'label',
          text: 'Insert video anywhere',
          style: "font-size:20px !important; font-weight: bold !important;"
        },
        {
          name: 'heartweb_videotype',
          type: 'listbox',
          label: 'Video Type',
          values: [
            {
              text: 'wistia',
              value: 'wistia'
            },
            {
              text: 'youtube',
              value: 'youtube'
            }
          ],
          minWidth: 200
        },
        {
          type: 'textbox',
          name: 'heartweb_videoid',
          label: 'Video ID',
          minWidth: 200
        },
        {
          type: 'textbox',
          name: 'heartweb_videoimage',
          id: 'heartweb_videoimage',
          label: 'Video Image',
          minWidth: 200
        },
        {
          type: 'button',
          text: 'Choose an image',
          onclick: function(e){
              e.preventDefault();
              var hidden = jQuery('#heartweb_videoimage');
              var custom_uploader = wp.media.frames.file_frame = wp.media({
                  title: 'Choose an image',
                  button: {text: 'Add an image'},
                  multiple: false
              });
              custom_uploader.on('select', function() {
                  var attachment = custom_uploader.state().get('selection').first().toJSON();
                  hidden.val(attachment.url);
              });
              custom_uploader.open();
          }
        }
      ],
      onSubmit: function (api) {
        var img=(typeof api.data.heartweb_videoimage!=='undefined'&&api.data.heartweb_videoimage!=='')?api.data.heartweb_videoimage:'//via.placeholder.com/945x597.png'
        params.editor.insertContent('[hbvideo type="'+api.data.heartweb_videotype+'" id="'+api.data.heartweb_videoid+'" poster="'+img+'"]');
        tinymce.activeEditor.windowManager.close()
      }
    },
    regbutton: {
      title: 'Instert registration button into content',
      body: [
        {
          type: 'label',
          text: 'Insert registration button anywhere',
          style: "font-size:20px !important; font-weight: bold !important;"
        },
        {
          name: 'heartweb_buttontype',
          type: 'listbox',
          label: 'Button Type',
          values: [
            {
              text: 'modal',
              value: 'modal'
            },
            {
              text: 'scroll',
              value: 'scroll'
            }
          ],
          minWidth: 200
        },
        {
          type: 'textbox',
          name: 'heartweb_buttontext',
          label: 'Button text',
          minWidth: 200
        },
      ],
      onSubmit: function (api) {
        params.editor.insertContent('[hbregbutton type="'+api.data.heartweb_buttontype+'" text="'+api.data.heartweb_buttontext+'"]');
        tinymce.activeEditor.windowManager.close()
      }
    }
  }
    return params.editor.windowManager.open(options[params.type]);
  };

jQuery( document ).on( 'tinymce-editor-init', function( event, editor ) {
  // register the formats
  var slyadnevTtlArr=[
    {
        block : 'h1',
        classes : 'slyadnev-ttl',
    },
    {
        block : 'h2',
        classes : 'slyadnev-ttl',
    },
    {
        block : 'h3',
        classes : 'slyadnev-ttl',
    },
    {
        block : 'h4',
        classes : 'slyadnev-ttl',
    },
    {
        block : 'h5',
        classes : 'slyadnev-ttl',
    },
    {
        block : 'h6',
        classes : 'slyadnev-ttl',
    },
  ]
  slyadnevTtlArr.forEach(function(el, i){
    var im=i+1
    tinymce.activeEditor.formatter.register('slyadnev-ttl-'+im, el);
  });

});