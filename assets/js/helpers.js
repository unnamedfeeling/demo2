

// https://www.labnol.org/internet/light-youtube-embeds/27941/
// Dependencies: blazy(*), slick slider
// <div class="js-youtube-player" data-id="*id youtube*" data-poster="*poster link (can be ommitted)*"></div>
/*Youtube videos*/
var ytimg={
		player: {},
		init: function(){
			var div, n,
			v = document.getElementsByClassName("js-youtube-player");
			for (n = 0; n < v.length; n++) {
				div = ytimg.createInnerElement(v[n]);
				v[n].appendChild(div);
			}
		},
		createInnerElement: function(el){
			var div = document.createElement("div");
			div.classList.add('yt-holder');
			div.id=el.dataset.id;
			div.setAttribute("data-id", div.id);
			div.innerHTML = ytimg.labnolThumb(div.id);
			// div.onclick = ytimg.labnolIframe(div);
			div.addEventListener('click', ytimg.labnolIframe)

			return div;
		},
		labnolThumb: function(id){
			var thumb = '<img class="video-img" src="https://i.ytimg.com/vi/'+id+'/maxresdefault.jpg">',
			play = '<div class="topimg_btn"><img src="//cdn.baxtep.com/img/svg/play_video.svg" onerror="this.onerror=null; this.src=\'//cdn.baxtep.com/img/icon/icon_play-90x90.png\'" alt="play_video" class="topimg_btn-bgimg"></div>'
			return thumb+play;
		},
		labnolIframe: function(event){
			var wd=this.offsetWidth,
				hg=this.offsetHeight-6,
				v_id=this.dataset.id,
				params={
					height: hg,
					width: wd,
					videoId: v_id,
					autoplay: 0,
					events: {
						'onReady': ytimg.onPlayerReady
					}
				};
			if(typeof YT === 'undefined'){
				var els = document.getElementsByTagName("script")[0],
				scp = document.createElement("script"),
				func = function () { els.parentNode.insertBefore(scp, els); };
				scp.type = "text/javascript";
				scp.id = 'youtube';
				scp.async = true;
				scp.src = "//www.youtube.com/iframe_api";
				if (window.opera == "[object Opera]") {
					document.addEventListener("DOMContentLoaded", func, false);
				} else { func(); }
				if (scp.readyState) {
					scp.onreadystatechange = function() {
						setTimeout(function(){
							ytimg.player = new YT.Player(v_id, params);
						}, 500)
					}
				} else {
					scp.onload = function() {
						setTimeout(function(){
							ytimg.player = new YT.Player(v_id, params);
						}, 500)
					}
				}
			} else {
				setTimeout(function(){
					ytimg.player = new YT.Player(v_id, params);
				}, 500)
			}
		},
		onPlayerReady: function(event) {
			event.target.playVideo();
		},
		stopVideo: function() {
			ytimg.player.stopVideo();
		},
		pauseVid: function(event) {
			ytimg.player.pauseVideo();
		},
		destroy: function(el){
			var iframe=el.querySelector('.yt-holder');
			el.removeChild(iframe);

			var newHolder=ytimg.createInnerElement(el)
			el.appendChild(newHolder)
		}
	}
ytimg.init();


function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	// console.log(matches);
	var retCookie='';
	try {
		retCookie= matches ? matches[1] : undefined;
	} catch(e) {
		retCookie= matches ? decodeURIComponent(matches[1]) : undefined;
	}
	// console.log(retCookie);
	return retCookie;
  // return matches ? decodeURIComponent(matches[1]) : undefined;
  // return matches ? matches[1] : undefined;
  // return matches;
}

function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
	var d = new Date();
	d.setTime(d.getTime() + expires * 1000);
	expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
	options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
	updatedCookie += "; " + propName;
	var propValue = options[propName];
	if (propValue !== true) {
	  updatedCookie += "=" + propValue;
	}
  }

  document.cookie = updatedCookie;
}

function createCookie(name,value,days) {
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days*24*60*60*1000));
		expires = "; expires=" + date.toUTCString();
	}
	document.cookie = name + "=" + value + "; path=/" + expires;
}



/* viewport width */
function vp() {
	var e = window,
		a = 'inner';
	if (!('innerWidth' in window)) {
		a = 'client';
		e = document.documentElement || document.body;
	}
	return {
		width: e[a + 'Width'],
		height: e[a + 'Height']
	}
}

function initTimer(params){
	if (params.endDate.seconds>0) {
		try {
			// init all timers
			var timerElements= (params.target==undefined)?document.querySelectorAll('.js-timer'):params.target,
				thisinit=function(timer, t){
					try {
						var curT=timer.getTimeValues(),
							digits=t.querySelectorAll('.digit'),
							returnTime=function(curT){
								var d=curT.days.toString(),
									h=(curT.hours.toString().length<2)?'0'+curT.hours.toString():curT.hours.toString(),
									m=(curT.minutes.toString().length<2)?'0'+curT.minutes.toString():curT.minutes.toString(),
									s=(curT.seconds.toString().length<2)?'0'+curT.seconds.toString():curT.seconds.toString(),
									arr=[d, h, m, s]

								return arr
							},
							timeArr=returnTime(curT),
							i=0,
							timerLocale=timerText(timeArr);

						thisSetTime(digits, timeArr, timerLocale)

						timer.addEventListener('secondsUpdated', function (e) {
							curT=timer.getTimeValues()
							timeArr=returnTime(curT)
							timerLocale=timerText(timeArr)

							thisSetTime(digits, timeArr, timerLocale)
						});
					} catch (e) {
						console.log(e);
						t.classList.add('hidden')
					}
				},
				thisSetTime=function(digits, timeArr, timerLocale){
					for (i = 0; i < digits.length; i++) {
						digits[i].innerHTML = (timeArr[i]!==undefined) ? timeArr[i] : ''
						digits[i].parentNode.querySelector('div:not(.digit)').innerText=timerLocale[i]
					}
				}

			timer.start({
				precision: 'seconds',
				countdown: true,
				startValues: (params.endDate!==undefined)?params.endDate:{days: 1}
			})

			if (!Array.isArray(timerElements)) {
				thisinit(timer, timerElements)
			} else {
				timerElements.forEach(function(t){
					thisinit(timer, t)
				})
			}
		} catch (e) {
			console.log(e)
		}
	}
}

function timerText(timeArr){
	var output=[],
		numArr=['2','3','4']
	timeArr.forEach(function(elem, index, arr){
		var txt=''
		elem=parseInt(elem)
		switch (index) {
			case 0:
				switch (true) {
					case (elem===1||(elem>19&&elem.toString().substr(-1)==='1')):
						txt='день'
						break;
					case (numArr.indexOf(elem.toString())>=0||(elem>19&&numArr.indexOf(elem.toString().substr(-1))>=0)):
						txt='дня'
						break;
					default:
						txt='дней'
				}
				break;
			case 1:
				switch (true) {
					case (elem===1||(elem>19&&elem.toString().substr(-1)==='1')):
						txt='час'
						break;
					case (numArr.indexOf(elem.toString())>=0||(elem>19&&numArr.indexOf(elem.toString().substr(-1))>=0)):
						txt='часа'
						break;
					default:
						txt='часов'
				}
				break;
			case 2:
				switch (true) {
					case (elem===1||(elem>19&&elem.toString().substr(-1)==='1')):
						txt='минута'
						break;
					case (numArr.indexOf(elem.toString())>=0||(elem>19&&numArr.indexOf(elem.toString().substr(-1))>=0)):
						txt='минуты'
						break;
					default:
						txt='минут'
				}
				break;
			case 3:
				switch (true) {
					case (elem===1||(elem>19&&elem.toString().substr(-1)==='1')):
						txt='секунда'
						break;
					case (numArr.indexOf(elem.toString())>=0||(elem>19&&numArr.indexOf(elem.toString().substr(-1))>=0)):
						txt='секунды'
						break;
					default:
						txt='секунд'
				}
		}

		output.push(txt)
	})

	return output
}

function outputRegistrationEnd(forms){
	for (var i = 0; i < forms.length; i++) {
		var parent=forms[i].parentNode
		parent.removeChild(forms[i])
		if (parent.querySelector('.js-contdown')!==null) {
			parent.querySelector('.js-contdown').parentNode.removeChild(parent.querySelector('.js-contdown'))
		}

		var message=document.createElement('p')
		message.classList.add('ccomf_wh2')
		message.style='text-align:center'
		message.innerHTML='Регистрация на это событие окончена! Предлагаем просмотреть курсы, которые предлагает платформа для обучения<br><a href="https://heartbeat.education" target="_blank">Heartbeat Education</a>'

		parent.appendChild(message)
	}
}

function setupFormsData(params){
	var forms=document.querySelectorAll('.hf-form')
	if (params.postId==undefined||params.webid==undefined) {
		outputRegistrationEnd(forms)
		return console.error('Cannot setup formdata!')
	}

	for (var param in params) {
		if (params.hasOwnProperty(param)) {
			forms.forEach(function(elem, index, arr){
				elem.querySelector(['[name="'+param+'"]']).value=params[param]
			})
		}
	}
}

function setupWebinarSelect(forms, webinarData){
	var webinarSelect = document.createElement('select'),
		selectWrapper=document.createElement('div'),
		options,
		choices=[],
		firstWebinarId,
		check=true;

	webinarData.forEach(function(elem, i, arr){
		if (elem.regCount>=elem.limit) {
			return check=false
		}
	})

	if (!check) {
		return check
	}

	selectWrapper.className='wind_inpholder text-center'

	webinarSelect.className='selectWebinar js-selectWebinar'
	webinarSelect.name='selectWebinar'



	for (var i = 0; typeof webinarData[i]=='object'; i++) {
		var option=document.createElement('option'),
			webinarDate=new Date(webinarData[i].sessionData.start_time)

		if (i===0) {
			firstWebinarId=webinarData[i].session
		}

		option.value=webinarData[i].session
		option.innerHTML=formatDate({date: webinarDate, delimiter: '.'})

		webinarSelect.appendChild(option)
	}

	selectWrapper.appendChild(webinarSelect)

	for (var i = 0; i < forms.length; i++) {
		var formSelect=selectWrapper.cloneNode(true),
			child=document.createElement('div'),
			target=forms[i].querySelector('.hf-fields-wrap'),
			targetElems=target.querySelectorAll('.dfc'),
			idField=forms[i].querySelector('.js-webinarId')

		child.className='dfc'
		child.appendChild(formSelect)
		insertAfter(child, targetElems[targetElems.length-1])

		choices.push(new Choices(target.querySelector('.js-selectWebinar')))

		forms[i].querySelector('.js-selectWebinar').addEventListener('change', function(event){
			var target=event.target,
				form=target.closest('form'),
				idField=form.querySelector('.js-webinarId'),
				nameField=form.querySelector('.js-webinarName'),
				selection=webinarData.filter(function(elem){
					return elem.session==target.value
				})[0]

			idField.value=target.value

			form.querySelector('.js-regCount h3').innerText='Занято мест: '+selection.regCount+' из '+selection.limit
		})
	}
}

function outputWebinarDate(params){
	if (typeof params=='undefined') {
		return console.error('Wrong params provided for outputWebinarDate func');
	}

	if (params.target!==undefined) {
		var start=params.start,
			month= new Intl.DateTimeFormat('en-US', { month: 'short'}).format(start),
			weekday= new Intl.DateTimeFormat('en-US', { weekday: 'short'}).format(start),
			timeText=start.getDate() + ' ' + month.strtr(ajax_func.datestrings) + ', ' + weekday.strtr(ajax_func.datestrings) + ' <span class="sep"></span> ' + formatTime({date: start, delimiter: ':'})

		params.target.forEach(function(elem){
			elem.innerHTML=timeText
		})
	}
}


$(document).on('click', '.js-wistia-placeholder .playbtn',
	function(event){
		var cont=$(this).parent(),
			wistiaCb=function(){
				cont.fadeOut(500, function() {
					$(this).parent().find('.js-wistia-video').fadeIn(300, function() {
						//Stuff to do *after* the animation takes place
					})
				})
			};
		cont.css({'opacity':0.8});
		var video=cont.parent().find('video').get(0)
		if (video!==undefined) {
			try {
				video.play()
				wistiaCb()
			} catch (e) {
				console.log(e);
			}
		} else {
			loadscripts([{id: 'wistia-js', src: "//fast.wistia.com/assets/external/E-v1.js", cb: wistiaCb}]);
		}
	}
);



/**
* strtr() for JavaScript
* Translate characters or replace substrings
*
* @author Dmitry Sheiko
* @version strtr.js, v 1.0.1
* @license MIT
* @copyright (c) Dmitry Sheiko http://dsheiko.com
**/

String.prototype.strtr = function ( dic ) {
  const str = this.toString(),
    makeToken = ( inx ) => `{{###~${ inx }~###}}`,

    tokens = Object.keys( dic )
      .map( ( key, inx ) => ({
        key,
        val: dic[ key ],
        token: makeToken( inx )
      })),

    tokenizedStr = tokens.reduce(( carry, entry ) =>
      carry.replace( entry.key, entry.token ), str );

  return tokens.reduce(( carry, entry ) =>
    carry.replace( entry.token, entry.val ), tokenizedStr );
};

function clonePreloader(){
  var preloader = document.querySelector('.js-preloader'),
    clone=preloader.cloneNode(true)

  return clone
}