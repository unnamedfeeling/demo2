
var _dcq = _dcq || [];
var _dcs = _dcs || {};
_dcs.account = '5349109';
(function() {
	var dc = document.createElement('script');
	dc.type = 'text/javascript'; dc.async = true;
	dc.src = '//tag.getdrip.com/5349109.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(dc, s);
})();
var couponCheckerPath, coursePath, courseUrl, fillCouponElements, getParameterData, overrideHeaderSignup, ready, scrollToPayments, selectProduct, shouldGetCouponOrProductData, signupScrollBottomIfNeeded, getData, disc,initSegment, ddata,
code=(getUrlParameter('code')!=='')?getUrlParameter('code'):(getUrlParameter('coupon_code')!=='')?getUrlParameter('coupon_code'):getUrlParameter('coupon');

function uuidv4() {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
		return v.toString(16);
	});
}
getParameterData = function() {
	return shouldGetCouponOrProductData() ? $.get(couponCheckerPath().toString(), getQueryString()).then(fillDataFromParameters) : void 0
};
shouldGetCouponOrProductData = function() {
	var query;
	return window.location.pathname.match(/purchased/) || window.location.pathname.match(/sign_in/) ? !1 : (query = window.location.search.toLowerCase(),
		courseUrl() && query.match(/product_id=/) || query.match(/code=/) || query.match(/coupon=/) ? !0 : void 0)
};
coursePath = function() {
	return $.url(courseUrl()).attr("path")
};
courseUrl = function() {
	return $("meta[id=fedora-data]").attr("data-course-url")
};
couponCheckerPath = function() {
	return coursePath() + "/coupon_and_product_data"
};
getData = function(data){
	disc=data.discount_percent;
	ddata=data;
};
$(document).on('click', 'a[href^="#"].js-ancor', function (event){
	event.preventDefault();
	var id=$(this).attr('href');
	$('html, body').animate({
		scrollTop: $(id).offset().top-$('header').innerHeight()-20
	}, 1000);
});
function heightsEqualizer(selector) {
	var elements = document.querySelectorAll(selector),
	max_height = 0,
	len = 0,
	i;
	if ( (elements) && (elements.length > 0) ) {
		len = elements.length;
		for (i = 0; i < len; i++) { 
			elements[i].style.height = ''; 
			if (elements[i].clientHeight > max_height) {
				max_height = elements[i].clientHeight;
			}
		}
		for (i = 0; i < len; i++) { 
			elements[i].style.height = max_height + 'px';
		}
	}
}
function getUrlParameter(name) {
	name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	var results = regex.exec(location.search);
	return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
/* viewport width */
function viewport(){
	var e = window,
	a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
}
function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
		));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}
function setCookie(name, value, options) {
	options = options || {};
	var expires = options.expires;
	if (typeof expires == "number" && expires) {
		var d = new Date();
		d.setTime(d.getTime() + expires * 1000);
		expires = options.expires = d;
	}
	if (expires && expires.toUTCString) {
		options.expires = expires.toUTCString();
	}
	value = encodeURIComponent(value);
	var updatedCookie = name + "=" + value;
	for (var propName in options) {
		updatedCookie += "; " + propName;
		var propValue = options[propName];
		if (propValue !== true) {
			updatedCookie += "=" + propValue;
		}
	}
	document.cookie = updatedCookie;
}
/* viewport width */
$(function(){
	if(typeof analytics!=='undefined'&&!getCookie('userSeg')){
		var id=uuidv4();
		setCookie('userSeg', id, {path:'/'});
		analytics.identify(id, {
			name: id,
			traits: {
				createdAt: Date.now()
			}
		});
	}
	/* placeholder*/
	$('.js-mbtn').click(function(event) {
		/* Act on the event */
		$(this).toggleClass('active');
		$('.js-hmenu-hidde').toggleClass('active');
	});
	$('.collapse_item').click(function(event) {
		/* Act on the event */
		$(this).toggleClass('active');
	});
	$('.js-uncollapse').click(function(event) {
		/* Act on the event */
		$(this).toggleClass('active');
		if ($(this).hasClass('active')) {
			$('#'+this.dataset.colltarget).children().each(function(index, el) {
				$(this).addClass('active')
			});
		}
		else {
			$('#'+this.dataset.colltarget).children().each(function(index, el) {
				$(this).removeClass('active');
			});
		}
	});
	var auto=false,ap=1000;
	if($('.package_slider').length){
		heightsEqualizer('.package_slider .package_list');
	}
	if($('.featured_courses').length||$('.course-list').length||$('.team').length||window.location.href.indexOf('fgd')>1){
		$('.courses_slick').find('.courses_ell').on('click', function(event){
			event.preventDefault();
			var th=$(this),
			lnk=th.data('lnk'),
			args={
				product_id: th.data('course-id'),
				sku: th.data('course-id'),
				name: th.data('course-name'),
				brand: 'Heartbeat education',
				variant: 'course',
				price: parseInt(th.find('.courses_price').text().replace(/[^\w\s]/gi, '')),
				quantity: 1,
				coupon: code,
				url: document.location.href+lnk,
				image_url: th.find('.courses_img').attr('src')
			};
			analytics.track('Product Clicked', args)
			window.location=lnk;
		})
		$('.team_slick').slick({
			autoplay:auto,
			autoplaySpeed:ap,
			arrows: false,
			dots:true,
			swipe:true,
			swipeToSlide:true,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [{
				breakpoint: 944,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					dots: true
				}
			},
			{
				breakpoint: 660,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}]
		});
	}
	$('.js_hidde').each(function(index, el) {
		$(this).attr('data-h',this.clientHeight+20+'px');
		$(this).css('max-height',0+'px');
	});
	$(document).on('click', '.js_hidder', function(event) {
		/* Act on the event */
		event.preventDefault();
		el = $(this).parentsUntil('.js_hidder-parent').parent().find('.js_hidde');
		el.toggleClass('active');
		if(el.hasClass('active')){
			el.css({
				'max-height': 'initial',
			});
		}
		else
		{
			el.css({
				'max-height': 0+'px',
			});
		}
	});
});
var handler = function(){
	var height_footer = $('footer').innerHeight(),
	height_header = $('header').innerHeight(),
	minh=(document.documentElement.clientHeight-height_footer-height_header)+'px';
	$('main').css({'margin-bottom':height_footer, 'padding-top':height_header});
		//
			//
			if($('.course-directory').length){
				$('.course-directory').css({'min-height' : minh});
			}
		},
		mload = function(){
			var trackThanks = function() {
				var an_data = JSON.parse(document.getElementById('analytics-keys').dataset.events)[0].properties,
				seg_data;
				$.get(couponCheckerPath().toString(), 'coupon_code='+an_data.coupon).then(getData).then(function(){
					seg_data = {
						checkout_id: an_data.orderId,
						order_id: an_data.orderId,
						tax: 0,
						coupon: an_data.coupon,
						currency: an_data.currency,
						products: [{
							product_id: an_data.products[0].id,
							sku: an_data.products[0].sku,
							name: an_data.products[0].variant,
							price: an_data.products[0].price,
							quantity: an_data.products[0].quantity,
							category: 'Courses'
						}]
					};
					seg_data.discount= (disc!==100) ? seg_data.products[0].price * disc / 100 : seg_data.products[0].price;
					seg_data.revenue= seg_data.products[0].price - seg_data.discount;
					seg_data.total= seg_data.revenue;
					analytics.track('Order Completed', seg_data);
				});
			};
			$('body').css('paddingBottom', 0);
			if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
				document.body.classList.add('ios');
			};
			if(document.location.href.indexOf('cdn')>1||document.location.href.indexOf('dev')>1){
				$('body').removeClass('loaded');
			}
			if (document.location.href.indexOf('purchased') > 1 || document.location.href.indexOf('thanks') > 1) {
				setTimeout(trackThanks(), 200);
			}
			if(window.location.search!==''){
				if(code!==''){
					customCouponApply({coupon:'coupon='+code, percent: false, couponAddText: 'Скидка до '});
				}
			}
		},
		mscroll = function(){
			var top = document.documentElement.scrollTop || document.body.scrollTop;
			if($('.circle').length && top+window.innerHeight>=$('.circle').offset().top && !$('.circle').hasClass('loaded')){
				$('.circle').addClass('loaded')
				$('.circle').each(function(index, el) {
					$(this).circleProgress({
						startAngle: Math.PI / 2,
						thickness: 1,
						value: (Number(this.dataset.cerclevalue)/5),
						size:118,
						emptyFill: "rgba(0, 0, 0, 0)",
						fill: {color: '#FF6400'}
					});
				}).on('circle-animation-progress', function(event, progress, stepValue) {
					$(this).find('.circle_digit').text((stepValue*5).toFixed(1));
				});
			}

			$('[data-dadd]').each(function(index, el) {
				$(this).isrich(top+window.innerHeight);
				$(this).dadd();
			});
		};
		$(window).bind('load', mload);
		$(window).bind('scroll', mscroll);

//
/*Youtube videos*/
var player;
function ytimg(){
	var div, n,
	v = document.getElementsByClassName("youtube-player");
	for (n = 0; n < v.length; n++) {
		div = document.createElement("div");
		div.classList.add('yt-holder');
		div.id=v[n].dataset.id;
		div.setAttribute("data-id", v[n].dataset.id);
		div.innerHTML = labnolThumb(v[n].dataset.id);
		div.onclick = labnolIframe;
		v[n].appendChild(div);
	}
}
function labnolThumb(id) {
	var thumb, play = '<div class="topimg_btn"><img src="//cdn.baxtep.com/img/svg/play_video.svg" onerror="this.onerror=null; this.src=\'//cdn.baxtep.com/img/icon/icon_play-90x90.png\'" alt="play_video" class="topimg_btn-bgimg"></div>';
	thumb='<img class="video-img" src="https://i.ytimg.com/vi/'+id+'/maxresdefault.jpg">';
	return thumb+play;
}
function labnolIframe() {
	var wd=this.offsetWidth,
	hg=this.offsetHeight-6,
	v_id=this.dataset.id,
	params={
		height: hg,
		width: wd,
		videoId: v_id,
		events: {
			'onReady': onPlayerReady
		}
	};
	if(typeof YT === 'undefined'){
		var els = document.getElementsByTagName("script")[0],
		scp = document.createElement("script"),
		func = function () { els.parentNode.insertBefore(scp, els); };
		scp.type = "text/javascript";
		scp.id = 'youtube';
		scp.async = true;
		scp.src = "//www.youtube.com/iframe_api";
		if (window.opera == "[object Opera]") {
			document.addEventListener("DOMContentLoaded", f, false);
		} else { func(); }
		if (scp.readyState) { 
			scp.onreadystatechange = function() {
				setTimeout(function(){
					player = new YT.Player(v_id, params);
				}, 500)
			}
		} else {
			scp.onload = function() {
				setTimeout(function(){
					player = new YT.Player(v_id, params);
				}, 500)
			}
		}
	} else {
		setTimeout(function(){
			player = new YT.Player(v_id, params);
		}, 500)
	}
}
function onPlayerReady(event) {
	event.target.playVideo();
}
function stopVideo() {
	player.stopVideo();
}
function pauseVid(event) {
	player.pauseVideo();
}
ytimg();
var buybtnClick=function(params){
	var target=(typeof params!=='undefined'&&typeof params.target!=='undefined')?params.target:$('.buybtn');
	$(target).each(function(index){
		$(this).on('click', function(event) {
			event.preventDefault();
			var loc = 'https://sso.teachable.com/secure/127625/checkout/confirmation?product_id=' + this.dataset.prodid + '&course_id=' + this.dataset.courseid,
			data = [],
			prodCoupon=(typeof this.dataset.coupon!=='undefined')?this.dataset.coupon:null;
			if (loc.indexOf('coupon_code')==-1 && code !== '' && typeof code !== 'undefined') {
				loc += '&coupon_code=' + code;
			}
			if (loc.indexOf('coupon_code')==-1&&prodCoupon!==''&&prodCoupon!==null) {
				loc += '&coupon_code=' + prodCoupon;
			}
			if (loc.indexOf('coupon_code')==-1&&typeof params=='object'&&typeof params.coupon!=='undefined') {
				loc += '&coupon_code=' + params.coupon;
			}
			var th = $(this),
			q = 1,
			pr = parseInt(th.data('price')) / 100,
			name = $('body').data('course-name'),
			args = {
				product_id: $('body').data('course-id'),
				sku: th.data('prodid'),
				name: name,
				brand: 'Heartbeat education',
				variant: th.parent().find('.package_h2').text(),
				price: pr,
				quantity: 1,
				coupon: code,
				currency: 'USD',
				value: pr * q,
				url: document.location.href,
				image_url: $('body').find('.topimg_bg').attr('src')
			};
			args.value -= args.value * disc / 100;
			analytics.track('Product Added', args);
			window.location.href = loc;
		});
	})
};
if ($('.course-directory').length) {
	var products = [],
	cat = ($('.course-directory .filter-title').find('h2').text() !== '' && $('.course-directory .filter-title').find('h2').text() !== 'undefined') ? $('.course-directory .filter-title').find('h2').text() : 'All courses',
	an_data = (document.getElementById('analytics-keys').length && JSON.parse(document.getElementById('analytics-keys').dataset.events)[0].properties.type !== '') ? JSON.parse(document.getElementById('analytics-keys').dataset.events)[0].properties :
	null,
	args = {};
	$('.course-directory').find('.courses_ell').each(function() {
		var c = {};
		c.product_id = (an_data !== null) ? an_data.id : $(this).data('course-id');
		c.sku = (an_data !== null) ? an_data.id : $(this).data('course-id');
		c.name = $(this).data('course-name');
		c.price = parseInt($(this).find('.courses_price').text().replace(/[^\w\s]/gi, ''));
		c.category = (an_data !== null) ? an_data.category : cat;
		c.url = 'http://heartbeat.education/' + $(this).data('lnk');
		c.image_url = $(this).find('img').attr('src');
		products.push(c);
	})
	args.list_id = cat;
	args.category = cat;
	args.products = products;
	if (args !== '' && cat !== '' && products !== '' && getUrlParameter('query') === '') {
		analytics.track('Product List Viewed', { 
			list_id: cat, 
			category: cat, 
			products: products
		});
	}
	var form = $('.course-directory').find('form[action="/courses"]');
	if (form.length) {
		form.on('submit', function() {
			analytics.track('Products Searched', { 
				query: $('#search-courses').val() 
			});
		})
	}
}

if (window.location.href.indexOf('sso') === -1) {
	if (typeof $('body').data('course-id') !== 'undefined' && !$('#purchase_id').length) {
		var th = $('body'),
		q = 1,
		pr = parseInt(th.data('price')) / 100,
		name = th.data('course-name'),
		args = {
			product_id: th.data('course-id'),
			sku: th.data('course-id'),
			name: name,
			brand: 'Heartbeat education',
			variant: 'course',
			price: pr,
			quantity: 1,
			coupon: code,
			currency: 'USD',
			value: pr * q,
			url: document.location.href,
			image_url: th.find('.topimg_bg').attr('src')
		};
		analytics.track('Product Viewed', args);
	}
}
var couponapply=false;
var customCouponApply=function(params){
	if (typeof params !=='object') {
		console.log('Wrong params!');
		return;
	}
	couponapply=true;
	if (typeof params.coupon!=='undefined') {
		$.get(couponCheckerPath().toString(), params.coupon).then(getData).then(function(){
			if($('.package_el').length){
				var thtext=strpt=fullstr=prcWDisc='', discpr='', prid;
				if (ddata.discounted_products.length==1) {
					$('.package_el').find('.package_price').each(function(){
						thtext=strpt=fullstr=prcWDisc='';
						if (typeof ddata.discounted_products[0].product_id!=='undefined') {
							prid='.buybtn[data-prodid="'+ddata.discounted_products[0].product_id+'"]';
							if ($(this).closest('.package_el').find(prid).length) {
								fullstr+=ddata.discounted_products[0].formatted_final_price;
								strpt='<br><span class="package_yellow"><del>'+$(this).text()+'</del>, скидка '+disc+'%</span>';
								fullstr+=strpt;
								$(this).html(fullstr);
								var btn=$(this).closest('.package_el').find(prid);
								btn.off('click');
								btn.addClass('couponapply');
								var args={coupon:params.coupon.replace('coupon=', ''), target:btn};
								buybtnClick(args);
							}
						}
					})
				} else {
					var ddataprodids=[];
					$(ddata.discounted_products).each(function(index) {
						ddataprodids.push(this.product_id);
					});
					$('.package_el').find('.package_price').each(function(index){
						var ind=index,
						thisprodid=parseInt($(this).closest('.package_el').find('.buybtn').data('prodid'));
						ind=(ddataprodids.indexOf(thisprodid)!==ind)?ddataprodids.indexOf(thisprodid):ind;
						thtext=strpt=fullstr=prcWDisc='';
						if (typeof ddata.discounted_products[ind]!=='undefined'&&typeof ddata.discounted_products[ind].product_id!=='undefined') {
							prid='.buybtn[data-prodid="'+ddata.discounted_products[ind].product_id+'"]';
							if ($(this).closest('.package_el').find(prid).length) {
								discpr=ddata.discounted_products[ind];
								fullstr+=discpr.formatted_final_price;
								strpt='<br><span class="package_yellow"><del>'+$(this).text()+'</del>, скидка '+discpr.discount_percent+'%</span>';
								fullstr+=strpt;
								$(this).html(fullstr);
								var btn=$(this).closest('.package_el').find(prid);
								btn.off('click');
								btn.addClass('couponapply');
								var args={coupon:params.coupon.replace('coupon=', ''), target:btn};
								buybtnClick(args);
							}
						}
					})
				}
			}
			var bannerEl=$('.coupon_banner'),
			addText=(typeof params.couponAddText!=='undefined')?params.couponAddText:'';
			if (bannerEl.length) {
				if (typeof params.percent=='undefined'||params.percent) {
					bannerEl.find('span.coupon-percent').text(ddata.discount_percent+'%');
				} else {
					var filterDisc=ddata.discounted_products[0].formatted_discount.replace('$', '')+' долл.';
					bannerEl.find('span.coupon-percent').text(addText+filterDisc);
				}
				bannerEl.find('.coupon-invalid-text').remove();
				bannerEl.css('display', 'block');
				bannerEl.find('.hidden').css('display', 'block').removeClass('hidden');
			}
		})
	}
};

if (document.body.dataset.courseId=='249839') {
	customCouponApply({coupon:'coupon=GRACE120', percent: false, couponAddText: 'Скидка до '});
}
if (document.body.dataset.courseId=='258612') {
	customCouponApply({coupon:'coupon=OPTIMGRACE180', percent: false, couponAddText: 'Скидка до '});
}
if (document.body.dataset.courseId=='258623') {
	customCouponApply({coupon:'coupon=OPTIMGRACE139', percent: false, couponAddText: 'Скидка до '});
}
if (document.body.dataset.courseId=='260469') {
	customCouponApply({coupon:'coupon=GRACE180', percent: false, couponAddText: 'Скидка до '});
}
if (document.body.dataset.courseId=='261010') {
	customCouponApply({coupon:'coupon=GRACE139', percent: false, couponAddText: 'Скидка до '});
}
setTimeout(function(){
	if (couponapply!==true) {
		buybtnClick();
	} else {
		buybtnClick({target:$('.buybtn').not('.couponapply')});
	}
}, 1000);
function sendData(data, opts) {
	var XHR = new XMLHttpRequest();
	var urlEncodedData = "";
	var urlEncodedDataPairs = [];
	XHR.addEventListener('load', function(event) {
		console.log(event);
	});
	XHR.addEventListener('error', function(event) {
		console.log(event);
	});
	opts.url=(typeof opts.url !== 'undefined')?opts.url:null;
	opts.type=(typeof opts.type !== 'undefined')?opts.type:'POST';
	XHR.open(opts.type, opts.url);
	XHR.send(urlEncodedData);
}
$('.js-customreg').on('submit', function(event){
	event.preventDefault();
	var rdata=$('.js-customreg').serialize();
	sendData(rdata, {url:'https://hooks.zapier.com/hooks/catch/2331074/8bqtae/', type:'GET'});
	setTimeout(function(){
		window.location.href='https://heartbeat.education/p/5012';
	})
});
