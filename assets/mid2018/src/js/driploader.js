var scriptsLoaded=[],
	scriptCb=function(params){
		if (typeof params=='undefined') {
			return console.log('No params provided for scriptCb');
		}
		var callback=params.el.cb,
			cbparams=(typeof params.el.cbparams!=='undefined')?params.el.cbparams:null;
		if (typeof params.script!=='undefined') {
			if (params.script.readyState) { // IE, incl. IE9
				params.script.onreadystatechange = function() {
					if (params.script.readyState == "loaded" || params.script.readyState == "complete") {
						params.script.onreadystatechange = null;
						if (typeof callback!=='undefined') callback(cbparams);
						return scriptsLoaded.push(params.el.id);
					}
				};
			} else {
				params.script.onload = function(){
					if (typeof callback!=='undefined') callback(cbparams);
					return scriptsLoaded.push(params.el.id);
				};
			}
		} else {
			var scriptIsLoaded=(scriptsLoaded.indexOf(params.el.id) > -1);
			if(typeof params.el.cb!=='undefined'&&params.el.cb !== null&&scriptIsLoaded){
				if (typeof callback!=='undefined') callback(cbparams);
			} else {
				setTimeout(function(){scriptCb(params)}, 500);
			}
		}
	},
		// usage: loadscripts([{id: String elid, src: String elsrc, cb: function func, cbparams: Object params}]);
	// checks if script is loaded allready by id and writes back to array scriptsLoaded
	loadscripts=function (arr){
		var params, i;
		for (i = 0; i < arr.length; i++) {
			if (document.getElementById(arr[i].id)==null&&scriptsLoaded.indexOf(arr[i].id) < 0) {
				var s=arr[i],
					script = document.createElement('script');
				script.id=s.id;
				script.src = s.src;
				scriptCb({el:s, script:script});
				document.getElementsByTagName('head')[0].appendChild(script);
			} else {
				scriptCb({el:arr[i]});
			}
		}
	};

var _dcq = _dcq || [],
	_dcs = _dcs || {},
	func=function(){
		$(document).on('click', '#confirm-purchase', function(event){
			var params=[email, email.value, username, username.value],
				check=false;
			for (var i = 1; i <= params.length; i++) {
				if(typeof params[i] == 'undefined'){check=false; break;}
				check=true;
			}
			if (check===true) {
				_dcq.push(["identify", {
					email: email.value,
					first_name: username.value,
					success: function(response) {
						_dcq.push(
							[
								"track", "Покупка марафона",
								{ value: parseInt(document.querySelectorAll('.spc__row.spc__row--total .spc__summary-item.text-right span')[1].innerHTML.replace('$', ''))*100 }
							]
						);
					}
				}]);
			}
		})

		_dcq.push(['track', 'Visited a page']);
	},
	prodid=location.pathname.match(/\/checkout\/(\d+)/)[1];

switch (prodid) {
	case '694480':
	case '694479':
	case '697081':
		_dcs.account = '6689252';

		// func();
		break;
	default:
		_dcs.account = '5349109';
		// callback=null;
}

// loadscripts(['//tag.getdrip.com/'+_dcs.account+'.js']);
loadscripts([{id: 'drip-js', src: '//tag.getdrip.com/'+_dcs.account+'.js', cb: func}]);
