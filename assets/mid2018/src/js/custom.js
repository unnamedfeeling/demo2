// WebFont.load({
// 	google: {
// 		families: ['Ubuntu', 'Source Sans Pro']
// 	}
// });

var fedoraData=document.getElementById('fedora-data'),
	segmentApiKey = (typeof hbApp!='undefined'&&typeof hbApp.segmentKey!='undefined')?hbApp.segmentKey:'EXvIntfSLHco2KpNqC23karCFxcgBRnA',
	vp=vp(),
	_dcq = _dcq || [],
	_dcs = _dcs || {},
	// initLeadScripts=function(){
	// 	if (typeof MC !=='undefined') {
	// 		var widgetInstanceElement = document.querySelectorAll('.mc')
	// 		const widget = MC.getWidget(widgetInstanceElement)
	// 		widget.setPayload('custom_payload')
	// 	}
	// },
	initSegment = function(config) {
		// console.log(config.key);
		// var analytics, analyticsKeys, i, key, ref, ref1, segmentApiKey;
		var analytics, analyticsKeys, i, key, ref, ref1;
		if (analytics = window.analytics = window.analytics || [],
		!analytics.initialize) {
			// if (analytics.invoked)
			// 	return void (window.console && console.error && console.error("Segment snippet included twice."));
			for (analytics.invoked = !0,
			analytics.methods = ["trackSubmit", "trackClick", "trackLink", "trackForm", "pageview", "identify", "reset", "group", "track", "ready", "alias", "page", "once", "off", "on"],
			analytics.factory = function(method) {
				return function() {
					var args;
					args = Array.prototype.slice.call(arguments),
					args.unshift(method);
					try {
						analytics.push(args)
					} catch (error) {}
					return analytics
				}
			}
			,
			i = 0; i < analytics.methods.length; )
				key = analytics.methods[i],
				analytics[key] = analytics.factory(key),
				i++;
			analytics.load = function(key) {
				var first, script;
				script = document.createElement("script"),
				script.type = "text/javascript",
				script.async = !0,
				script.src = document.location.protocol + "//cdn.segment.com/analytics.js/v1/" + key + "/analytics.min.js",
				first = document.getElementsByTagName("script")[0],
				first.parentNode.insertBefore(script, first)
			}
			,
			analytics.SNIPPET_VERSION = "3.1.0",
			analyticsKeys = null != (ref = $("meta[name='analytics']").data()) ? ref.analytics : void 0,
			// segmentApiKey = null != analyticsKeys && null != (ref1 = analyticsKeys["Segment.io"]) ? ref1.apiKey : void 0,
			// segmentApiKey = 'EXvIntfSLHco2KpNqC23karCFxcgBRnA',
			config.key && analytics.load(config.key);
			analytics.page();
		}

		// initLeadScripts();
	};
	// couponCheckerPath, coursePath, courseUrl, fillCouponElements, getParameterData, overrideHeaderSignup, ready, scrollToPayments, selectProduct, shouldGetCouponOrProductData, signupScrollBottomIfNeeded, getData, disc, ddata,
	// code=(getUrlParameter('code')!=='')?getUrlParameter('code'):(getUrlParameter('coupon_code')!=='')?getUrlParameter('coupon_code'):getUrlParameter('coupon'),
	// uuidv4 = function() {
	//   return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	// 	var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
	// 	return v.toString(16);
	//   });
	// };

// https://stackoverflow.com/questions/610406/javascript-equivalent-to-printf-string-format
// usage: String.format('{0} is dead, but {1} is alive! {0} {2}', 'ASP', 'ASP.NET');
if (!String.format) {
	String.format = function(format) {
		var args = Array.prototype.slice.call(arguments, 1);
		return format.replace(/{(\d+)}/g, function(match, number) {
			return typeof args[number] != 'undefined' ? args[number] : match;
		});
	};
}

// if (fedoraData!==null) {
// 	switch (true) {
// 		case (window.location.href.search('funnel-test')>-1):
// 		// case (window.location.href.search('landing')>-1):
// 			_dcs.account = '2090226';
// 			break;
// 		case fedoraData.dataset.courseId=='354743':
// 			_dcs.account = '6689252';
//
// 			if (vp.width<600) {
// 				window.addEventListener('load', function(){
// 					setTimeout(function(){
// 						_dcq.push(['showForm', { id: '758044947' }]);
// 					}, 15000);
// 				})
// 			}
// 			break;
// 		default:
// 			_dcs.account = '5349109';
// 	}
// } else {
// 	_dcs.account = '5349109';
// }
//
//
// loadscripts([{id:'drip-js', src:'//tag.getdrip.com/'+_dcs.account+'.js'}]);


// getParameterData = function() {
// 	return shouldGetCouponOrProductData() ? $.get(couponCheckerPath().toString(), getQueryString()).then(fillDataFromParameters) : void 0
// };
// shouldGetCouponOrProductData = function() {
// 	var query;
// 	return window.location.pathname.match(/purchased/) || window.location.pathname.match(/sign_in/) ? !1 : (query = window.location.search.toLowerCase(),
// 	courseUrl() && query.match(/product_id=/) || query.match(/code=/) || query.match(/coupon=/) ? !0 : void 0)
// };
// coursePath = function() {
// 	return $.url(courseUrl()).attr("path")
// };
// courseUrl = function() {
// 	return $("meta[id=fedora-data]").attr("data-course-url")
// };
// couponCheckerPath = function() {
// 	return coursePath() + "/coupon_and_product_data"
// };
// getData = function(data){
// 	// console.log(data);
// 	disc=data.discount_percent;
// 	ddata=data;
// 	// return discObj=data;
// };
$(document).on('click', '.js-scrollToFirstForm', function (event){
	event.preventDefault();
	var form=$('form').first().closest('.wind');
	if (typeof form=='undefined'||form=='undefined') {
		return false;
	}
	$('html, body').animate({
		scrollTop: form.offset().top-$('header').innerHeight()-20
	}, 1000);
});
// function heightsEqualizer(selector) {
//     var elements = document.querySelectorAll(selector),
//         max_height = 0,
//         len = 0,
//         i;
//
//     if ( (elements) && (elements.length > 0) ) {
//         len = elements.length;
//
//         for (i = 0; i < len; i++) { // get max height
// 			elements[i].style.height = ''; // reset height attr
//             if (elements[i].clientHeight > max_height) {
//                 max_height = elements[i].clientHeight;
//             }
// 			// console.log(i);
//         }
//
//         for (i = 0; i < len; i++) { // set max height to all elements
//             elements[i].style.height = max_height + 'px';
//         }
//     }
// }
function getUrlParameter(name) {
	name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	var results = regex.exec(location.search);
	return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
	"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
	var d = new Date();
	d.setTime(d.getTime() + expires * 1000);
	expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
	options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
	updatedCookie += "; " + propName;
	var propValue = options[propName];
	if (propValue !== true) {
	  updatedCookie += "=" + propValue;
	}
  }

  document.cookie = updatedCookie;
}
/* viewport width */
$(function(){
	// $('.faq_el').each(function(index, el) {
	// 	var p = $(this);
	// 	$(this).click(function(event) {
	// 		if(!event.target.href) {
	// 			p.toggleClass('active');
	// 		}
	//
	// 	});
	// });

	/* placeholder*/
	$('.js-mbtn').click(function(event) {
		/* Act on the event */
		$(this).toggleClass('active');
		$('.js-hmenu-hidde').toggleClass('active');
	});
	$('.collapse_rs .collapse_date').each(function(index, el) {
		var ul = $(this).parents('.collapse_item').find('.collapse_list'),
		sum=0;
		ul.find('.collapse_date').each(function(index, el) {
			sum+=$(el).toSec();
		});
		this.textContent = toTime(sum);

	});
	$('.collapse_item').click(function(event) {
		/* Act on the event */
		$(this).toggleClass('active');
	});
	$('.js-uncollapse').click(function(event) {
		/* Act on the event */
		$(this).toggleClass('active');
		if ($(this).hasClass('active')) {
			$('#'+this.dataset.colltarget).children().each(function(index, el) {
				$(this).addClass('active')
			});
		}
		else {
			$('#'+this.dataset.colltarget).children().each(function(index, el) {
				$(this).removeClass('active');
			});
		}
	});
	// var auto=false,ap=1000;
	// $('.package_slider').slick({
	// 	autoplay:auto,
	// 	autoplaySpeed:ap,
	// 	arrows: false,
	// 	dots:true,
	// 	swipe:true,
	// 	swipeToSlide:true,
	// 	slidesToShow: 3,
	// 	slidesToScroll: 1,
	// 	responsive: [{
	// 		breakpoint: 944,
	// 		settings: {
	// 			slidesToShow: 2,
	// 			slidesToScroll: 1,
	// 			dots: true
	// 		}
	// 	},
	// 	{
	// 		breakpoint: 660,
	// 		settings: {
	// 			slidesToShow: 1,
	// 			slidesToScroll: 1
	// 		}
	// 	}]
	// });
	// if($('.package_slider').length){
	// 	heightsEqualizer('.package_slider .package_list');
	// }
	// if($('.featured_courses').length||$('.course-list').length||$('.team').length||window.location.href.indexOf('fgd')>1){
	// 	// console.log($('.featured_courses').length);
	// 	// if($('.featured_courses').length){
	// 	// 	$('.featured_courses').find('.courses_slide').wrapAll('<div class="courses_slick row dfc" />');
	// 	// } else {
	// 	// 	$('.course-list').find('.courses_slide').wrapAll('<div class="courses_slick row dfc" />');
	// 	// }
	// 	// $('.courses_slick').slick({
	// 	// 	autoplay:auto,
	// 	// 	autoplaySpeed:ap,
	// 	// 	arrows: false,
	// 	// 	dots:true,
	// 	// 	swipe:true,
	// 	// 	swipeToSlide:true,
	// 	// 	slidesToShow: 3,
	// 	// 	slidesToScroll: 1,
	// 	// 	responsive: [{
	// 	// 		breakpoint: 944,
	// 	// 		settings: {
	// 	// 			slidesToShow: 2,
	// 	// 			slidesToScroll: 1,
	// 	// 			dots: true
	// 	// 		}
	// 	// 	},
	// 	// 	{
	// 	// 		breakpoint: 660,
	// 	// 		settings: {
	// 	// 			slidesToShow: 1,
	// 	// 			slidesToScroll: 1
	// 	// 		}
	// 	// 	}]
	// 	// });
	// 	$('.courses_slick').find('.courses_ell').on('click', function(event){
	// 		event.preventDefault();
	// 		if (typeof analytics!=='undefined') {
	// 			var th=$(this),
	// 				lnk=th.data('lnk'),
	// 				args={
	// 					product_id: th.data('course-id'),
	// 					sku: th.data('course-id'),
	// 					name: th.data('course-name'),
	// 					brand: 'Heartbeat education',
	// 					variant: 'course',
	// 					price: parseInt(th.find('.courses_price').text().replace(/[^\w\s]/gi, '')),
	// 					quantity: 1,
	// 					coupon: code,
	// 					url: document.location.href+lnk,
	// 					image_url: th.find('.courses_img').attr('src')
	// 				};
	// 			// console.log(args);
	// 			analytics.track('Product Clicked', args)
	// 		}
	// 		window.location=lnk;
	// 	})
	// 	$('.team_slick').slick({
	// 		autoplay:auto,
	// 		autoplaySpeed:ap,
	// 		arrows: false,
	// 		dots:true,
	// 		swipe:true,
	// 		swipeToSlide:true,
	// 		slidesToShow: 4,
	// 		slidesToScroll: 1,
	// 		responsive: [{
	// 			breakpoint: 944,
	// 			settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 1,
	// 				dots: true
	// 			}
	// 		},
	// 		{
	// 			breakpoint: 660,
	// 			settings: {
	// 				slidesToShow: 1,
	// 				slidesToScroll: 1
	// 			}
	// 		}]
	// 	});
	// }
	// // $('.star_holder').mystar({click:true});
	// //$('.js-test').mystar({click:true})
	// $('.js_hidde').each(function(index, el) {
	//
	// 	// $(this).attr('data-h',$(this).height()+'px');
	// 	$(this).attr('data-h',this.clientHeight+20+'px');
	// 	$(this).css('max-height',0+'px');
	// });
	// $(document).on('click', '.js_hidder', function(event) {
	// 	// console.log(event.target);
	// 	/* Act on the event */
	// 	event.preventDefault();
	// 	// el = $(this).parent().parent().find('.js_hidde');
	// 	el = $(this).parentsUntil('.js_hidder-parent').parent().find('.js_hidde');
	// 	el.toggleClass('active');
	// 	// debugger;
	// 	if(el.hasClass('active')){
	// 		el.css({
	// 			// 'max-height': el[0].dataset.h,
	// 			'max-height': 'initial',
	// 		});
	// 	}
	// 	else
	// 	{
	// 		el.css({
	// 			'max-height': 0+'px',
	// 		});
	// 	}
	// });
	// var
	// desctop	=1200,
	// tablet	=992,
	// phone	=768;
	// $('.bsld_slick').on('init', function(event, slick,currentSlide){
	// 	$(slick.$slides[slick.options.initialSlide-1]).addClass('bef');
	// 	$(slick.$slides[slick.options.initialSlide+1]).addClass('aft');
	// }).on('beforeChange', function(event, slick, currentSlide, nextSlide){
	// 	$(slick.$slides[nextSlide-1]).addClass('bef').removeClass('aft');
	// 	$(slick.$slides[nextSlide+1]).addClass('aft').removeClass('bef');
	// 	$(slick.$slides[nextSlide]).removeClass('bef').removeClass('aft');
	//
	// }).on('afterChange', function(event, slick, currentSlide, nextSlide){
	//
	// });
	// $('.bsld_slick').slick({
	// 	accessibility: true,
	// 	adaptiveHeight: false,
	// 	appendDots: $('.bsld_slick-posr'),
	// 	arrows:false,
	// 	//autoplay: false,
	// 	//autoplaySpeed: 3000,
	// 	centerMode: true,
	// 	centerPadding: '0px',
	// 	cssEase: 'ease',
	// 	customPaging: function(slider, i) {
	// 		return ''+$(slider.$slides[i]).children()[0].dataset.dt;
	// 	},
	// 	dots: true,
	// 	slidesToShow: 1,
	// 	infinite: false,
	// 	//dotsClass: 'slick-dots',
	// 	variableWidth: true,
	// 	swipeToSlide: true,
	// 	initialSlide: 1,
	// 	focusOnSelect: true,
	// 	responsive: [
	// 	{
	// 		breakpoint: desctop,
	// 		settings: {
	//
	// 		}
	// 	},
	// 	{
	// 		breakpoint: tablet,
	// 		settings: {
	//
	// 		}
	// 	},
	// 	{
	// 		breakpoint: phone,
	// 		variableWidth: false,
	// 		settings: {
	//
	// 		}
	// 	}]
	// });
	// $('.partners-slider').slick({
	// 	arrows: false,
	// 	dots:true,
	// 	centerMode: true,
	// 	centerPadding: '0px',
	// 	slidesToShow: 5,
	// 	slidesToScroll: 1,
	// 	swipeToSlide: true,
	// 	focusOnSelect: true,
	// 	responsive: [
	// 	{
	// 		breakpoint: desctop,
	// 		settings: {
	// 			slidesToShow: 4,
	// 		}
	// 	},
	// 	{
	// 		breakpoint: tablet,
	// 		settings: {
	// 			slidesToShow: 3,
	// 		}
	// 	},
	// 	{
	// 		breakpoint: phone,
	// 		settings: {
	// 			slidesToShow: 2,
	// 		}
	// 	}]
	// });
	$('.frquest_el').click(function(event) {
		$(this).toggleClass('active');
		b = $(this).hasClass('active')
		if(b){
			$(this).find('.frquest_answ').show('300');
		}
		else{
			$(this).find('.frquest_answ').hide('300');
		}
	});

	$('.frform_close').click(function(event) {
		/* Act on the event */
		event.preventDefault();
		$('.frform').addClass('closed');
	});

	// $('.lpack_slider').slick({
	// 	arrows: false,
	// 	autoplay: false,
	// 	autoplaySpeed: 3000,
	// 	customPaging: function(slider, i) {
	// 		return '';
	// 	},
	//
	// 	dots: true,
	// 	slidesPerRow: 1,
	// 	slidesToShow: 4,
	// 	slidesToScroll: 1,
	// 	swipeToSlide: true,
	// 	infinite: false,
	// 	responsive: [
	// 	{
	// 		breakpoint: 1280,
	// 		settings: {
	// 			slidesToShow: 3,
	// 			slidesToScroll: 3,
	// 			dots: true
	// 		}
	// 	},
	// 	{
	// 		breakpoint: 992,
	// 		settings: {
	// 			slidesToShow: 2,
	// 			slidesToScroll: 2
	// 		}
	// 	},
	// 	{
	// 		breakpoint: 640,
	// 		settings: {
	// 			slidesToShow: 1,
	// 			slidesToScroll: 1
	// 		}
	// 	}
	// 		// You can unslick at a given breakpoint now by adding:
	// 		// settings: "unslick"
	// 		// instead of a settings object
	// 		]
	// 	})
	// $('.msauthor_slider').slick({
	//
	// 	arrows: false,
	// 	asNavFor: null,
	// 	autoplay: false,
	// 	//autoplaySpeed: 3000,
	// 	centerMode: true,
	// 	focusOnSelect: true,
	// 	centerPadding: '0px',
	// 	customPaging: function(slider, i) {
	// 		return '';
	// 	},
	// 	dots: true,
	// 	slidesPerRow: 1,
	// 	slidesToShow: 3,
	// 	slidesToScroll: 1,
	// 	responsive: [
	// 	{
	// 		breakpoint: desctop,
	// 		settings: {
	// 			slidesToShow: 3,
	// 			centerPadding: '0px',
	// 		}
	// 	},
	// 	{
	// 		breakpoint: tablet,
	// 		settings: {
	// 			slidesToShow: 1,
	// 			centerPadding: '150px',
	// 		}
	// 	},
	// 	{
	// 		breakpoint: phone,
	// 		settings: {
	// 			slidesToShow: 1,
	// 			centerPadding: '0px',
	// 		}
	// 	}]
	// });
	//
	// $('.expert_slider').slick({
	// 	arrows: false,
	// 	customPaging: function(slider, i) {
	// 		return  '';
	// 	},
	// 	dots: true,
	// 	rows: 2,
	// 	rtl: false,
	// 	slide: '',
	// 	slidesPerRow: 2,
	// 	swipeToSlide: true,
	// 	responsive: [
	// 	{
	// 		breakpoint: desctop,
	// 		settings: {
	//
	// 		}
	// 	},
	// 	{
	// 		breakpoint: tablet,
	// 		settings: {
	//
	// 		}
	// 	},
	// 	{
	// 		breakpoint: phone,
	// 		settings: {
	// 			slidesPerRow: 1,
	// 		}
	// 	}]
	// });
});
var handler = function(){
	var height_footer = ($('footer').length)? $('footer').get(0).clientHeight:0,
		height_header = ($('header').length)? $('header').get(0).clientHeight:0,
		minh=(document.documentElement.clientHeight-height_footer-height_header)+'px';
	$('.js-padboth').css({'padding-bottom':height_footer +'px', 'padding-top':height_header +'px'});
	$('.js-padtop').css({'padding-top':height_header +'px'});
	$('.js-padbott').css({'padding-bottom':height_footer +'px'});

	//	$('main').css({'margin-bottom':height_footer, 'padding-top':height_header});
	$('.content').css('paddingBottom', height_footer+'px');


	// if($('.course-directory').length){
	// 	$('.course-directory').css({'min-height' : minh});
	// }
		// if($('.blocks-page-homepage').length){
		// 	var pad=(document.documentElement.clientHeight-height_header-$('.full_width_image').first().find('.full-width-image-bg').height())/2;
		// 	$('.full_width_image').first().find('.full-width-image-bg').css({'padding-top':pad+'px', 'padding-bottom':pad+'px'});
		// }
		// height: calc(100vh - 90px);
},
mload = function(){
	// $('.js-contdown').each(function(index, el) {
	// 	$(el).ptimer();
	// });

	// var trackThanks = function() {
	// 	var an_data = document.getElementById('analytics-keys').dataset.events,
	// 		seg_data;
	// 	if (typeof an_data=='undefined') {
	// 		return false;
	// 	}
	// 	an_data=JSON.parse(document.getElementById('analytics-keys').dataset.events)[0].properties;
	// 	$.get(couponCheckerPath().toString(), 'coupon_code='+an_data.coupon).then(getData).then(function(){
	// 		// var disc_num=disc.discount_percent;
	// 		// console.log(disc);
	// 		if (typeof analytics!=='undefined') {
	// 			seg_data = {
	// 				checkout_id: an_data.orderId,
	// 				order_id: an_data.orderId,
	// 				// total: an_data.total,
	// 				// revenue: an_data.total-an_data.discount,
	// 				// revenue: 0,
	// 				// revenue: an_data.total,
	// 				tax: 0,
	// 				// discount: an_data.discount,
	// 				coupon: an_data.coupon,
	// 				currency: an_data.currency,
	// 				products: [{
	// 					product_id: an_data.products[0].id,
	// 					sku: an_data.products[0].sku,
	// 					name: an_data.products[0].variant,
	// 					price: an_data.products[0].price,
	// 					quantity: an_data.products[0].quantity,
	// 					category: 'Courses'
	// 				}]
	// 			};
	// 			seg_data.discount= (disc!==100) ? seg_data.products[0].price * disc / 100 : seg_data.products[0].price;
	// 			seg_data.revenue= seg_data.products[0].price - seg_data.discount;
	// 			seg_data.total= seg_data.revenue;
	// 			analytics.track('Order Completed', seg_data);
	// 		}
	// 	});
	// };

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		document.body.classList.add('ios');
	};
	if(document.location.href.indexOf('cdn')>1||document.location.href.indexOf('dev')>1){
		$('body').removeClass('loaded');
	}
	// setTimeout(function(){
	// 	if(typeof analytics==='undefined'||window.location.href.indexOf('\/p\/')>1){
			initSegment({key:segmentApiKey});


			// if(typeof analytics!=='undefined'){
			// 	var userData=(getCookie('userSeg')!==undefined)?JSON.parse(getCookie('userSeg')):{id: uuidv4()};
			//
			// 	if (typeof _dcq!=='undefined') {
			// 		var dripconf=["identify"]
			// 		var conf={success: function(response){console.log(response)}}
			// 		if (typeof userData!=='undefined'&&typeof userData.email!=='undefined') {
			// 			conf.email=userData.email
			// 			dripconf.push(conf)
			// 		}
			// 		_dcq.push(dripconf);
			// 	}
			//
			// 	var userId=userData.id,
			// 		integrationData={
			// 			integrations: {
			// 				Intercom : {
			// 					user_hash : ''
			// 				}
			// 			}
			// 		},
			// 		getHmac,
			// 		hmacUrl='https://content.baxtep.com/wp-json/hbcontent/v1/getHash/';
			//
			// 	switch (true) {
			// 		case (typeof userData!=='undefined'&&typeof userData.email!=='undefined'):
			// 			userId=userData.id;
			// 			var data=[]
			// 			data.userId=userId
			// 			getHmac=sendData(data, {url: hmacUrl, type:'POST'});
			//
			// 			jQuery.when( getHmac ).then(
			// 				function( result ) {
			// 					result=JSON.parse(result)
			// 					var segUserData={
			// 							name: userData.name,
			// 							email: userData.email,
			// 							createdAt: Date.now()
			// 						}
			// 					integrationData.integrations.Intercom.user_hash=result.data.hmac
			//
			// 					analytics.identify(userId, segUserData, integrationData);
			// 				},
			// 				function(result){
			// 					console.log('Something went wrong: ', result);
			// 				}
			// 			)
			//
			// 			break;
			// 		default:
			// 			var data=[]
			// 			data.userId=userId
			// 			getHmac=sendData(data, {url: hmacUrl, type:'POST'});
			//
			// 			jQuery.when( getHmac ).then(
			// 				function( result ) {
			// 					result=JSON.parse(result)
			// 					integrationData.integrations.Intercom.user_hash=result.data.hmac
			//
			// 					analytics.identify(userId, {
			// 							createdAt: Date.now()
			// 						},
			// 						integrationData
			// 					)
			// 				},
			// 				function(result){
			// 					console.log('Something went wrong: ', result);
			// 				}
			// 			)
			// 	}
			//
			// 	setCookie('userSeg', JSON.stringify(userData), {path:'/'});
			// }
	// 	}
	// }, 2000)
	// if (document.location.href.indexOf('purchased') > 1 || document.location.href.indexOf('thanks') > 1) {
	// 	setTimeout(trackThanks(), 200);
	// }
	// if(window.location.search!==''){
	// 		// var disc;
	// 	if(code!==''){
	// 			// var discount, discObj;
	// 			// (function() {
	//
	//
	// 				// discObj=$.get(couponCheckerPath().toString(), getQueryString());
	//
	//
	// 				// $.get(couponCheckerPath().toString(), getQueryString()).then(getData).then(function(){
	// 				// 	console.log(ddata);
	// 				// 	if($('.package_el').length){
	// 				// 		var thtext=strpt=fullstr=prcWDisc='', discpr='', prid;
	// 				// 		if (ddata.discounted_products.length==1) {
	// 				// 			$('.package_el').find('.package_price').each(function(){
	// 				// 				thtext=strpt=fullstr=prcWDisc='';
	// 				// 				prid='.buybtn[data-prodid="'+ddata.discounted_products[0].product_id+'"]';
	// 				// 				// console.log(prid);
	// 				// 				// console.log($(this).closest('.package_el').find(prid));
	// 				// 				if (typeof ddata.discounted_products[0].product_id!=='undefined'&&$(this).closest('.package_el').find(prid).length) {
	// 				// 					prcWDisc=parseInt(this.innerHTML.replace(/[^\w\s]/gi, ''))-(parseInt(this.innerHTML.replace(/[^\w\s]/gi, ''))*parseInt(disc)/100);
	// 				// 					fullstr+='$'+prcWDisc.toFixed(2);
	// 				// 					strpt='<br><span class="package_yellow"><del>'+$(this).text()+'</del>, скидка '+disc+'%</span>';
	// 				// 					fullstr+=strpt;
	// 				// 					$(this).html(fullstr);
	// 				// 				}
	// 				// 			})
	// 				// 		} else {
	// 				// 			$('.package_el').find('.package_price').each(function(index){
	// 				// 				thtext=strpt=fullstr=prcWDisc='';
	// 				// 				prid='.buybtn[data-prodid="'+ddata.discounted_products[index].product_id+'"]';
	// 				// 				// console.log(prid);
	// 				// 				// console.log($(this).closest('.package_el').find(prid));
	// 				// 				if (typeof ddata.discounted_products[index].product_id!=='undefined'&&$(this).closest('.package_el').find(prid).length) {
	// 				// 					// console.log(ddata.discounted_products[index]);
	// 				// 					discpr=ddata.discounted_products[index];
	// 				// 					fullstr+=discpr.formatted_final_price;
	// 				// 					strpt='<br><span class="package_yellow"><del>'+$(this).text()+'</del>, скидка '+discpr.discount_percent+'%</span>';
	// 				// 					fullstr+=strpt;
	// 				// 					$(this).html(fullstr);
	// 				// 				}
	// 				// 			})
	// 				// 		}
	// 				// 		// var thtext=strpt=fullstr=prcWDisc='';
	// 				// 		// if (ddata.discounted_products.length==1) {
	// 				// 		// 	$('.package_el').find('.package_price').each(function(){
	// 				// 		// 		// console.log(this.innerHTML.replace(/[^\w\s]/gi, ''));
	// 				// 		// 		// console.log(parseInt(this.innerHTML.replace(/[^\w\s]/gi, '')));
	// 				// 		// 		prcWDisc=parseInt(this.innerHTML.replace(/[^\w\s]/gi, ''))-(parseInt(this.innerHTML.replace(/[^\w\s]/gi, ''))*parseInt(disc)/100);
	// 				// 		// 		fullstr+='$'+prcWDisc.toFixed(2);
	// 				// 		// 		strpt='<br><span class="package_yellow"><del>'+$(this).text()+'</del>, скидка '+disc+'%</span>';
	// 				// 		// 		fullstr+=strpt;
	// 				// 		// 		$(this).html(fullstr);
	// 				// 		// 		// console.log(str);
	// 				// 		// 	})
	// 				// 		// } else {
	// 				// 		// 	$('.package_el').find('.package_price').each(function(index){
	// 				// 		// 		// console.log(ddata.discounted_products[index]);
	// 				// 		// 		var discpr=ddata.discounted_products[index];
	// 				// 		// 		// console.log(this.innerHTML.replace(/[^\w\s]/gi, ''));
	// 				// 		// 		// console.log(parseInt(this.innerHTML.replace(/[^\w\s]/gi, '')));
	// 				// 		// 		// prcWDisc=parseInt(this.innerHTML.replace(/[^\w\s]/gi, ''))-(parseInt(this.innerHTML.replace(/[^\w\s]/gi, ''))*parseInt(disc)/100);
	// 				// 		// 		fullstr+=discpr.formatted_final_price;
	// 				// 		// 		strpt='<br><span class="package_yellow"><del>'+$(this).text()+'</del>, скидка '+discpr.discount_percent+'%</span>';
	// 				// 		// 		fullstr+=strpt;
	// 				// 		// 		$(this).html(fullstr);
	// 				// 		// 		// console.log(str);
	// 				// 		// 	})
	// 				// 		// }
	// 				// 	}
    //                 //
	// 				// })
	// 		customCouponApply({coupon:'coupon='+code, percent: false, couponAddText: 'Скидка до '});
	//
	// 				// console.log(coursePath() + "/coupon_and_product_data");
	// 				// console.log(getQueryString());
	// 				// $(document).ready(ready),
	// 				// $(document).on("page:load", ready)
	// 			// })();
	//
	// 			// switch (code) {
	// 			// 	case '123':
	// 			// 		disc=5;
	// 			// 		break;
	// 			// 	// default:
	// 			//
	// 			// }
	// 			// disc=disc/100;
	// 			// console.log(disc);
	//
	// 			// console.log(discObj);
	// 			// console.log(typeof discObj);
	// 			// console.log(JSON.stringify(discObj));
	// 	}
	// }

	if($('.mstop')[0]){
		$('body').addClass('masterclass');

	}
	mscroll();

},
	// mresize = function(){
	// 	// console.log('mresize')
	// 	// handler();
	// 	document.body.style.paddingBottom=0;
	// 	console.log('res');
	// },
mscroll = function(){
	var top = document.documentElement.scrollTop || document.body.scrollTop;

	$('[data-dadd]').each(function(index, el) {
		$(this).isrich(top+window.innerHeight);
		$(this).dadd();
	});

	if($('.frform')[0]){
		if($('.frform_cont').isrich(top+(window.innerHeight-$('.frform').innerHeight()))){

			$('.frform').removeClass('scroll');
			$('.frform_cont').height('0');
		}
		else
		{
			$('.frform_cont').height($('.frform').innerHeight());
			$('.frform').addClass('scroll');

		}
	}

	if($('.mstop')[0]){
		$('header').addClass('brite ');
	}

	/*>$('.mstop .wind').offset().top*/
	if($('.mstop')[0] && top>$('header').height() ){
		$('header').addClass('d');
	}
	else{
		$('header').removeClass('d');
	}
	if($('.mstop')[0] && top+$('header').height() >$('.mstop .wind').offset().top ){
		$('header').removeClass('brite ');

	}
	mresize();
};
var mresize = function(){
	$('.lpack_head').css('top',$('header').innerHeight()+'px')

	handler();
}
$(window).bind('resize', mresize);

$(window).bind('load', mload);
$(window).bind('scroll', mscroll);
$( document ).ready(function() {
	// handler();
	var height_footer = ($('footer').length)? $('footer').get(0).clientHeight:0,
		height_header = ($('header').length)? $('header').get(0).clientHeight:0,
		minh=(document.documentElement.clientHeight-height_footer-height_header)+'px';
	$('.js-padboth').css({'padding-bottom':height_footer +'px', 'padding-top':height_header +'px'});
	$('.js-padtop').css({'padding-top':height_header +'px'});
	$('.js-padbott').css({'padding-bottom':height_footer +'px'});

	var modals={
		vidModal: function(){
			$('.js-vidModal').magnificPopup({
				// disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade',
				removalDelay: 160,
				preloader: false,

				fixedContentPos: false
			});
		},
		wistiaModal: function(){
			$('.js-wistiaModal').magnificPopup({
				type: 'iframe',
				iframe: {
					patterns: {
						wistia: {
							index: 'wistia.com',
							id: function(url) {
								var m = url.match(/^.+wistia.com\/(medias)\/([^_]+)[^#]*(#medias=([^_&]+))?/);
									if (m !== null) {
										if(m[4] !== undefined) {
											return m[4];
										}
										return m[2];
									}
									return null;
								},
							src: '//fast.wistia.net/embed/iframe/%id%?autoPlay=1' // https://wistia.com/doc/embed-options#options_list
						}
					}
				},
				// callbacks: {
				// 	open: function() {
				// 		console.log('open');
				// 	},
				// 	// close: function() {
				// 	//
				// 	// }
				// }
			});
		},
		// freeModal: function(){
		// 	$(document).on('click', '.js-startFree', function(event){
		// 		var src='';
		// 		if (typeof $(this).attr('href')!=='undefined') {
		// 			src=$($(this).attr('href'));
		// 		} else if (typeof $(this).data('target')!=='undefined') {
		// 			src=$($(this).data('target'));
		// 		} else {
		// 			src=$.parseHTML('<div class="white-popup testmodal">data</div>');
		// 		}
		// 		if (typeof $(this).data('pricing')!=='undefined') {
		// 			src.find('.js-price').val($(this).data('pricing'));
		// 		}
		// 		$.magnificPopup.open({
		// 			items: {
		// 				src: src, // can be a HTML string, jQuery object, or CSS selector
		// 				type: 'inline'
		// 			}
		// 		});
		// 	})
		// },
		// funnelsModal: function(){
		// 	// записываем id первой модалки
		// 	var initialModalId,
		// 		modalId,
		// 		currModal='',
		// 		prevModal='',
		// 		instance={},
		// 		prevInstance={},
		// 		initImageZoom=function(elem){
		// 			var product = elem.get(0),
		// 				img = product.querySelector('.image-wrap'),
		// 				plus = product.querySelector('.js-controls .control-in'),
		// 				minus = product.querySelector('.js-controls .control-out'),
		// 				isDown = false,
		// 				startX,
		// 				startY,
		// 				x,
		// 				y,
		// 				panX,
		// 				panY,
		// 				curscale=1;
		//
		// 			product.classList.add('active');
		//
		// 			plus.addEventListener('click', function(e){
		// 				// console.log(curscale);
		// 				if (typeof curscale=='undefined') {
		// 					curscale=product.querySelector('img').getBoundingClientRect().width / product.querySelector('img').offsetWidth;
		// 				}
		// 				if (!product.classList.contains('zoom-in')) {
		// 					if (curscale==2.5) {
		// 						product.classList.add('zoom-in');
		// 						product.querySelector('img').style.transform='';
		// 						curscale=2.5;
		// 						plus.classList.add('inactive');
		// 						plus.disabled=true;
		// 					} else {
		// 						curscale=(curscale<1.5)?1.5:curscale+0.5;
		// 						product.querySelector('img').style.transform = 'scale('+curscale+')';
		// 					}
		// 				}
		// 				if (curscale>1) {
		// 					minus.classList.remove('inactive');
		// 					minus.disabled='';
		// 					product.classList.add('zoomed');
		// 				}
		// 			});
		//
		// 			minus.addEventListener('click', function(e){
		// 				// console.log(curscale);
		// 				if (typeof curscale=='undefined') {
		// 					curscale=product.querySelector('img').getBoundingClientRect().width / product.querySelector('img').offsetWidth;
		// 				}
		// 				if (typeof curscale!=='undefined') {
		// 					if (product.classList.contains('zoom-in')) {
		// 						product.classList.remove('zoom-in');
		// 						product.querySelector('img').style.transform='scale(2.5)';
		// 						curscale=2.5;
		// 						plus.classList.remove('inactive');
		// 						plus.disabled='';
		// 					} else {
		// 						// product.classList.remove('zoom-in');
		// 						curscale=(curscale>1)?curscale-0.5:1;
		// 						product.querySelector('img').style.transform = 'scale('+curscale+')';
		// 					}
		// 				}
		// 				if (curscale<1.5) {
		// 					minus.classList.add('inactive');
		// 					minus.disabled=true;
		// 					product.classList.remove('zoomed');
		// 				}
		// 				// img.style.transform = 'translateX(' + 0 + 'px' + ') translateY(' + 0 + 'px' + ')';
		// 			});
		//
		// 			var getPositions = function (e) {
		// 					isDown = true;
		//
		// 					// console.log('getPositions event type: ', typeof e);
		// 					// console.log('getPositions event: ', e);
		// 					// console.log('getPositions: ', [e.pageX, e.pageY, startX, startY, x, y, panX, panY, product.offsetLeft, product.offsetTop]);
		//
		// 					// startX = e.pageX - product.offsetLeft;
		// 					// startY = e.pageY - product.offsetTop;
		// 					switch (typeof e.touches) {
		// 						case 'undefined':
		// 							startX = (typeof panX!=='undefined')? e.pageX - panX - product.offsetLeft : e.pageX - product.offsetLeft;
		// 							startY = (typeof panY!=='undefined')? e.pageY - panY - product.offsetTop : e.pageY - product.offsetTop;
		// 							break;
		// 						default:
		// 							startX = (typeof panX!=='undefined')? e.touches[0].pageX - panX - product.offsetLeft : e.touches[0].pageX - product.offsetLeft;
		// 							startY = (typeof panY!=='undefined')? e.touches[0].pageY - panY - product.offsetTop : e.touches[0].pageY - product.offsetTop;
		// 					}
		//
		//
		// 					// product.classList.add('zoom-in');
		//
		// 				},
		// 				disable= function (e) {
		// 					// console.log('disable event type: ', typeof e);
		// 					// console.log('disable event: ', e);
		// 					// console.log('disable: ', [e.pageX, e.pageY, startX, startY, x, y, panX, panY, product.offsetLeft, product.offsetTop]);
		// 					isDown = false;
		// 				},
		// 				zoomPan= function(e) {
		// 					// console.log('zoomPan event type: ', typeof e);
		// 					// console.log('zoomPan event: ', e);
		// 					// console.log('zoomPan: ', [e.pageX, e.pageY, startX, startY, x, y, panX, panY, product.offsetLeft, product.offsetTop]);
		// 					if(!isDown) return; //stop fn if mouse is not dowm
		//
		// 					e.preventDefault();
		//
		// 					x = ((typeof e.touches!=='undefined')? e.touches[0].pageX : e.pageX) - product.offsetLeft;
		// 					y = ((typeof e.touches!=='undefined')? e.touches[0].pageY : e.pageY) - product.offsetTop;
		// 					panX = x - startX;
		// 					panY = y - startY;
		//
		// 					// startX=panX;
		// 					// startY=panY;
		//
		// 					img.style.transform = 'translateX(' + panX + 'px' + ') translateY(' + panY + 'px' + ')';
		//
		// 				};
		//
		// 			// Desktop
		// 			product.addEventListener("mousedown", getPositions);
		// 			product.addEventListener("mouseup", disable);
		// 			product.addEventListener("mouseleave", disable);
		// 			product.addEventListener("mousemove", zoomPan);
		//
		// 			// Touch
		// 			product.addEventListener("touchstart", getPositions);
		// 			product.addEventListener("touchend", disable);
		// 			product.addEventListener("touchcancel", disable);
		// 			product.addEventListener("touchmove", zoomPan);
		// 		};
		//
		// 	$(document).on('click', '.js-funnelModal', function(event) {
		// 		event.preventDefault();
		// 		// записываем id цели из аттрибута href
		// 		var href=$(this).attr('href'),
		// 			btn=$(this);
		// 		if (typeof initialModalId==='undefined') {
		// 			initialModalId=href;
		// 			// modalId=href;
		// 		}
		// 		// закрываем любую открытую модалку
		// 		$.magnificPopup.close();
		// 		// открываем новую
		// 		$.magnificPopup.open({
		// 			// type: 'inline',
		// 			items: {
		// 				src: href, // can be a HTML string, jQuery object, or CSS selector
		// 				type: 'inline'
		// 			},
		// 			// это обратные вызовы - в процессе работы модалки (и любого другого скрипта) происходят события и грамотный плагин всегда имеет действия по этим событиям. вызов функции по событию называется обратным вызовом
		// 			callbacks: {
		// 				// beforeOpen: function() {
		// 				// 	console.log('Start of popup initialization');
		// 				// },
		// 				// elementParse: function(item) {
		// 				// 	// Function will fire for each target element
		// 				// 	// "item.el" is a target DOM element (if present)
		// 				// 	// "item.src" is a source that you may modify
		// 				//
		// 				// 	// console.log('Parsing content. Item object that is being parsed:', item);
		// 				// 	setTimeout(function(){
		// 				// 		modalId='#'+item.inlineElement[0].id;
		// 				// 	}, 100);
		// 				// },
		// 				// change: function() {
		// 				// 	console.log('Content changed');
		// 				// },
		// 				// resize: function() {
		// 				// 	console.log('Popup resized');
		// 				// 	// resize event triggers only when height is changed or layout forced
		// 				// },
		// 				open: function() {
		// 					// console.log('Popup is opened');
		// 					instance=this;
		// 					currModal=instance.currItem.src;
		// 					// console.log('currModal=', currModal);
		// 					// console.log('prevModal=', prevModal);
		//
		// 					if (btn.closest('.wrap_webinar').length>0) {
		// 						if (currModal!==''&&prevModal!==''&&currModal!==prevModal) {
		// 							if ($(href).find('.js-prevModal').length<1) {
		// 								$(href).find('.wrap_webinar').prepend('<a href="'+prevModal+'" class="js-prevModal js-funnelModal modalVid">вернуться назад</a>');
		// 							} else {
		// 								$(href).find('.js-prevModal').attr('href', prevModal);
		// 							}
		// 						} else {
		// 							$(href).find('.js-prevModal').remove();
		// 						}
		//
		// 						if (currModal==initialModalId) {
		// 							$(href).find('.js-prevModal').remove();
		// 						}
		//
		// 						// проверяем если в нашей целевой модалке есть кнопка "назад" и задаем ей нужный нам аттрибут
		// 						// if ($(href).find('.js-prevModal').length) {
		// 						// 	$(href).find('.js-prevModal').attr('href', initialModalId);
		// 						// }
		// 					}
		//
		// 					if ($(href).find('.js-imageStage').length&&!$(href).find('.js-imageStage').hasClass('active')) {
		// 						initImageZoom($(href).find('.js-imageStage'));
		// 					}
		// 					// console.log(this.content); // Direct reference to your popup element
		// 				},
		// 				beforeClose: function(){
		// 					prevInstance=this;
		// 					prevModal=this.currItem.src;
		// 				}
		// 				//
		// 				// beforeClose: function() {
		// 				// 	// Callback available since v0.9.0
		// 				// 	console.log('Popup close has been initiated');
		// 				// 	console.log('#'+this.content[0].id);
		// 				// 	console.log(modalId);
		// 				// 	console.log(initialModalId);
		// 				// 	console.log(href);
		// 				// 	// if (typeof href!=='undefined') {
		// 				// 	// 	if (('#'+this.content[0].id)===initialModalId&&modalId===initialModalId) {
		// 				// 	// 		initialModalId=undefined;
		// 				// 	// 	}
		// 				// 	// }
		// 				// },
		// 				// close: function() {
		// 				// 	console.log('Popup removal initiated (after removalDelay timer finished)');
		// 				// 	console.log(this);
		// 				// },
		// 				// afterClose: function() {
		// 				// 	console.log('Popup is completely closed');
		// 				// 	console.log(this);
		// 				// 	console.log(initialModalId);
		// 				// },
		// 			},
		// 			// убрал чтобы не мешало работать - если оставить - нужно делать задержку на открытие модалки после закрытия предыдущей
		// 			// removalDelay: 160,
		//
		// 			// остальные опции...
		// 			preloader: false,
		// 			fixedContentPos: false,
		// 			mainClass: 'funnel_popup',
		// 			closeOnBgClick: true,
		// 		});
		// 	});
		// }
	};

	// if ($('.js-funnelModal').length) {
	// 	loadstyles([{id: 'mfp-css', src:'//cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/magnific-popup.min.css'}]);
	// 	loadscripts([{id: 'mfp-js', src:'//cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js', cb:modals.funnelsModal}]);
	// }
	if ($('.js-vidModal').length) {
		loadstyles([{id: 'mfp-css', src:'//cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/magnific-popup.min.css'}]);
		loadscripts([{id: 'mfp-js', src:'//cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js', cb:modals.vidModal}]);
	}

	if ($('.js-wistiaModal').length) {
		loadstyles([{id: 'mfp-css', src:'//cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/magnific-popup.min.css'}]);
		loadscripts([{id: 'mfp-js', src:'//cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js', cb:modals.wistiaModal}]);
	}

	// if ($('.js-startFree').length) {
	// 	loadstyles([{id: 'mfp-css', src:'//cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/magnific-popup.min.css'}]);
	// 	loadscripts([{id: 'mfp-js', src:'//cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js', cb:modals.freeModal}]);
	// }

	if ($('.js-reviews').length) {
		var reviewsInit=function(params){
				var top = document.documentElement.scrollTop || document.body.scrollTop;
				if (params.target.length<1) {
					return false
				} else {
					params.target.each(function(index, el) {
						if (params.courseId==null) {
							params.courseId=(typeof $(this).data('courseid')!=='undefined')?$(this).data('courseid'):258623;
						}
						var th=$(this),
							circle=th.find('.circle'),
							comments=sendData('', {url:'https://content.baxtep.com/wp-json/hbcontent/v1/getComments/'+params.courseId, type:'GET'});
						// if(circle.length && top+window.innerHeight>=circle.offset().top && !circle.hasClass('loaded')){
						$.when( comments ).then(
							function( result ) {
								var res=(result!=='')?JSON.parse(result):'';
								if (res!==''||(typeof res.data!=='undefined'&&res.data.comments.posts.length>0)) {
									var commentsRes=res.data.comments,
										valHolder,
										cvals=commentsRes.countedvalues,
										props=['value', 'communication', 'recommend', 'material'],
										prop,
										revhtml='',
										post,
										i,
										overall=0;
									// console.log(res);
									// console.log(commentsRes);
									// console.log(cvals);

									if (typeof commentsRes.total!=='undefined') {
										th.find('.js-total').text(commentsRes.total);
									}

									for (i = 0; i < props.length; i++) {
										prop='hb_'+props[i];
										// console.log(prop);
										if (typeof cvals[prop]!=='undefined') {
											overall+=cvals[prop];
											valHolder=th.find('.js-'+props[i]);
											valHolder.find('.circle').get(0).dataset.cerclevalue=cvals[prop];
											valHolder.find('.circle_digit').text(Math.round(cvals[prop] * 100) / 100);
										}
									}

									// if (typeof cvals.hb_overall!=='undefined') {
										// th.find('.js-overall .raiting_digit').text(Math.round(cvals.hb_overall * 100) / 100);
										// th.find('.js-overall .star_holder').get(0).dataset.raiting=cvals.hb_overall;
									// }
									overall=Math.round(overall / props.length * 10) / 10
									th.find('.js-overall .raiting_digit').text(overall);
									th.find('.js-overall .star_holder').get(0).dataset.raiting=overall;

									for (i = 0; i < commentsRes.posts.length; i++) {
										post=commentsRes.posts[i];
										revhtml+=String.format(commentsRes.tpl, post.pmeta.thumb, post.post_date, post.pmeta.hb_name, post.post_content);
									}

									th.find('.js-revhtml').empty().append(revhtml);

									setTimeout(function(){
										if(circle.length && !circle.hasClass('loaded')){
											circle.addClass('loaded')
											circle.each(function(index, el) {
												$(this).circleProgress({
													startAngle: Math.PI / 2,
													thickness: 1,
													value: (Number(this.dataset.cerclevalue)/5),
													size:118,
													emptyFill: "rgba(0, 0, 0, 0)",
													fill: {color: '#FF6400'}
												});
											}).on('circle-animation-progress', function(event, progress, stepValue) {
												$(this).find('.circle_digit').text((stepValue*5).toFixed(1));
											});
										}
										if (th.find('.star_holder')) {
											th.find('.star_holder').mystar({click:false});
										}
									}, 500);
								} else {
									// th.fadeOut('300', function() {
									//
									// });
									th.find('.review_container').each(function(index, el) {
										if (!$(this).hasClass('js-noposts')) {
											$(this).fadeOut('300', function() {

											});
										} else {
											$(this).fadeIn('300', function() {

											});
										}
									});
								}
							},
							function( result ) {
								// console.log('2');
								console.log(JSON.parse(result));
							}
						);
					});
				}

			},
			params;
		$('.js-reviews').each(function(index, el) {
			params={target:$(this), courseId:(typeof document.body.dataset!=='undefined' && typeof document.body.dataset.courseId!=='undefined')?document.body.dataset.courseId:(($(this).data('courseid')!=='')?$(this).data('courseid'):null)};
			loadscripts([{id: 'circleProgress-js', src:'//cdn.baxtep.com/new/lib/circle-progress/circle-progress.js', cb:reviewsInit, cbparams:params}]);
		});

	}

	if ($('.js-gif').length) {
		apngTest().done(function(){
			$('.js-gif').each(function(index, el) {
				var th=$(this);
				switch (true) {
					case (APNG===true):
						var img=(th.data('apng')!=='')?th.data('apng'):th.data('gif');
						th.attr('src', img);
						break;
					default:
						th.attr('src', th.data('gif'));
				}
			});
		})
	}

	if ($('.js-calendar').length) {
		var removeUnusedBtns=function(){
			document.querySelectorAll('.js-calendar').forEach(function(elem){
				var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
				var elemObserver = new MutationObserver(function(mutations) {
					var btns=elem.querySelector('.js-addeventatc_dropdown')
					btns.parentNode.removeChild(btns)

					elemObserver.disconnect()
				});

				elemObserver.observe(elem, {
						attributes : true, attributeFilter : ['id'], childList: false, subtree: false, characterData: false
				});

			})

		}
		loadscripts([{id: 'addEvent-js', src:'//addevent.com/libs/atc/1.6.1/atc.min.js', cb: removeUnusedBtns}]);
	}

	// if (typeof document.body.dataset!=='undefined' && typeof document.body.dataset.courseId!=='undefined') {
	// 	// if (document.body.dataset.courseId=='235117') {
	// 	// 	customCouponApply({coupon:'coupon=HALF'});
	// 	// }
	//
	// 	if (document.body.dataset.courseId=='249839') {
	// 		customCouponApply({coupon:'coupon=GRACE120', percent: false, couponAddText: 'Скидка до '});
	// 	}
	// 	if (document.body.dataset.courseId=='258612') {
	// 		customCouponApply({coupon:'coupon=OPTIMGRACE180', percent: false, couponAddText: 'Скидка до '});
	// 	}
	// 	if (document.body.dataset.courseId=='258623') {
	// 		customCouponApply({coupon:'coupon=OPTIMGRACE139', percent: false, couponAddText: 'Скидка до '});
	// 	}
	// 	if (document.body.dataset.courseId=='260469') {
	// 		customCouponApply({coupon:'coupon=GRACE180', percent: false, couponAddText: 'Скидка до '});
	// 	}
	// 	if (document.body.dataset.courseId=='261010') {
	// 		customCouponApply({coupon:'coupon=GRACE139', percent: false, couponAddText: 'Скидка до '});
	// 	}
		setTimeout(function(){
			// if (couponapply!==true) {
			// 	buybtnClick();
			// } else {
			// 	buybtnClick({target:$('.buybtn').not('.couponapply')});
			// }
			buybtnClick({target: $('.js-buybtn')});
		}, 1000);
	// }
});
// $.prototype.mystar = function(uoption){
// 	console.log(this);
// 	if(this.length<1) {return false;}
//
// 	$(this).each(function(index, el) {
// 		option={
// 			raiting:Number(this.dataset.raiting) || 0,
// 			click:false
// 		}
// 		$.extend(option,uoption);
// 		$(this).each(function(index, el) {
// 			var r = 100/(5/option.raiting),
// 			t=$(el),
// 			hider = $(t).find('.star_hider');
// 			hider.width(r+'%');
// 			var timer;
// 			if(option.click){
// 				t.addClass('clicked');
// 				t.click(function(e) {
// 					/* Act on the event */
// 					var x = e.offsetX==undefined?e.layerX:e.offsetX;
// 					var nr = (x*100)/t.width();
// 					nr = nr>100?100:nr;
// 					r = nr.toFixed(2);
// 					hider.width(nr+'%');
// 					if($(this).find('.star-popup')[0]){
// 						popup = $(this).find('.star-popup');
// 					}
// 					else{
// 						$(this).append('<div class="star-popup"></div>').animate({opacity:1},500);
// 						popup = $(this).find('.star-popup');
// 					}
// 					popup.stop(true,false);
// 					popup.text((x/100*5).toFixed(1));
// 					if(timer) { clearTimeout(timer);}
// 					timer =setTimeout((function(){
// 						popup.animate({
// 							opacity: 0},300,
// 							function() {
//
// 							popup.remove();
// 						});
// 					}), 1000);
// 					//popup = '<div class="star-popup">'+(x/100*5).toFixed(1)+'</div>';
// 				});
// 			}
// 		});
// 	});
//
// };


$.prototype.isrich = function(arg,t){
	/*if(t){
		console.log(arg+'>='+$(this).offset().top,arg>=$(this).offset().top)
	}*/
	if(!this[0] || !arg){
		return false;
	}
	if(arg>=$(this).offset().top ){
		return true;
	}
	return false;
}

$.prototype.dadd = function(atr){
	atr =atr || 'load'
	$(this).each(function(index, el) {
		$(this).css({
			'background-image': 'url("'+this.dataset.dadd+'")',
		});
		$(this).addClass('load');
	});
}


// product added to cart
var buybtnClick=function(params){
	var target=(typeof params!=='undefined'&&typeof params.target!=='undefined')?params.target:$('.buybtn');
	// $(document).on('click', '.buybtn', function(event) {
	// 	event.preventDefault();
	// 	var loc = 'https://sso.teachable.com/secure/127625/checkout/confirmation?product_id=' + this.dataset.prodid + '&course_id=' + this.dataset.courseid,
	// 		data = [];
	// 	// data['prodid']=this.dataset.prodid;
	// 	// data['courseid']=this.dataset.courseid;
	// 	if (code !== '' && typeof code !== 'undefined') {
	// 		loc += '&coupon_code=' + code;
	// 		// data['code']=code;
	// 	}
	// 	// setCookie('proddata', data,{expires:3600,path:'/'})
	// 	var th = $(this),
	// 		q = 1,
	// 		pr = parseInt(th.data('price')) / 100,
	// 		name = $('body').data('course-name'),
	// 		args = {
	// 			product_id: $('body').data('course-id'),
	// 			sku: th.data('prodid'),
	// 			name: name,
	// 			brand: 'Heartbeat education',
	// 			variant: th.parent().find('.package_h2').text(),
	// 			price: pr,
	// 			quantity: 1,
	// 			coupon: code,
	// 			currency: 'USD',
	// 			value: pr * q,
	// 			url: document.location.href,
	// 			image_url: $('body').find('.topimg_bg').attr('src')
	// 		};
	// 	if (code !== '') {
	// 		args.value -= args.value * disc / 100;
	// 	}
	// 	// console.log(args);
	// 	analytics.track('Product Added', args);
	// 	window.location.href = loc;
	// });
	// console.log(params);
	// $(document).on('click', '.buybtn', function(event) {
	$(target).each(function(index){
		$(this).on('click', function(event) {
			event.preventDefault();
			var loc = 'https://sso.teachable.com/secure/'+this.dataset.schoolid+'/checkout/confirmation?product_id=' + this.dataset.prodid + '&course_id=' + this.dataset.courseid,
			data = [],
			prodCoupon=(typeof this.dataset.coupon!=='undefined')?this.dataset.coupon:null;

			// data['prodid']=this.dataset.prodid;
			// data['courseid']=this.dataset.courseid;
			// var couponstr=(typeof params=='object')?params.coupon:null;
			if (loc.indexOf('coupon_code')==-1 && code !== '' && typeof code !== 'undefined') {
				loc += '&coupon_code=' + code;
			}
			if (loc.indexOf('coupon_code')==-1&&prodCoupon!==''&&prodCoupon!==null) {
				loc += '&coupon_code=' + prodCoupon;
			}
			if (loc.indexOf('coupon_code')==-1&&typeof params=='object'&&typeof params.coupon!=='undefined') {
				loc += '&coupon_code=' + params.coupon;
			}
			// setCookie('proddata', data,{expires:3600,path:'/'})

			if (typeof analytics!=='undefined') {
				var th = $(this),
					q = 1,
					pr = parseInt(th.data('price')) / 100,
					name = $('body').data('course-name'),
					args = {
						product_id: $('body').data('course-id'),
						sku: th.data('prodid'),
						name: name,
						brand: 'Heartbeat education',
						variant: th.parent().find('.package_h2').text(),
						price: pr,
						quantity: 1,
						coupon: code,
						currency: 'USD',
						value: pr * q,
						url: document.location.href,
						image_url: $('body').find('.topimg_bg').attr('src')
					};
				args.value -= args.value * disc / 100;
				analytics.track('Product Added', args);
			}

			window.location.href = loc;
		});
	})
};
// buybtnClick();

// $(document).on('error', '.js-wind_form', function(event) {
// 	console.log(event);
// });

var submittedVals={};
html_forms.on('submit', function(formElement) {
	var successSvg=$.parseHTML('<div class="successAnimationWrapper js-successAnimation"><svg class="successAnimation" xmlns="http://www.w3.org/2000/svg" width="70" height="70" viewBox="0 0 70 70"><path class="successAnimationResult" fill="#D8D8D8" d="M35,60 C21.1928813,60 10,48.8071187 10,35 C10,21.1928813 21.1928813,10 35,10 C48.8071187,10 60,21.1928813 60,35 C60,48.8071187 48.8071187,60 35,60 Z M23.6332378,33.2260427 L22.3667622,34.7739573 L34.1433655,44.40936 L47.776114,27.6305926 L46.223886,26.3694074 L33.8566345,41.59064 L23.6332378,33.2260427 Z"/><circle class="successAnimationCircle" cx="35" cy="35" r="24" stroke="#979797" stroke-width="2" stroke-linecap="round" fill="transparent"/><polyline class="successAnimationCheck" stroke="#979797" stroke-width="2" points="23 34 34 43 47 27" fill="transparent"/></svg><h4>Регистрация прошла успешно!</h4></div>')[0]
	for (var i = 0; i < formElement.length; i++) {
		var form=formElement[i];

		form.querySelector('button').disabled=true

		if (!form.querySelectorAll('.js-preloader').length) {
			var formpreloader=clonePreloader()

			formpreloader.appendChild(successSvg)

			form.appendChild(formpreloader)
		}
		$(form.querySelectorAll('.js-preloader')).fadeIn()

		if (form.querySelectorAll('.error').length) {
			var fields=form.querySelectorAll('.error');
			for (var x = 0; x < fields.length; x++) {
				fields[x].classList.remove('error');
			}
		}

		var inputs=form.querySelectorAll('input'),
			names=['userName', 'phone', 'email', 'phonedata'];
		inputs.forEach(function(item, i, arr){
			if (names.indexOf(item.name) > -1) {
				submittedVals[item.name]=item.value;
			}
		})
	}
});


html_forms.on('success', function(formElement) {
	for (var i = 0; i < formElement.length; i++) {
		var form=formElement[i],
			preloader=form.querySelector('.js-preloader')


		if (preloader!==undefined) {
			$(preloader).find('.preloader-circleHolder').fadeOut('500', function() {
				var successAnim=this.parentNode.querySelector('.js-successAnimation')
				successAnim.classList.add('show')
				successAnim.querySelector('.successAnimation').classList.add('animated')
			});
		}
	}

	if (typeof analytics!=='undefined') {
		if (submittedVals.phonedata!=='') {
			submittedVals.phone=submittedVals.phonedata;
		}
		delete submittedVals.phonedata;

		var trackObj={Email: submittedVals.email, Phone: submittedVals.phone};
		switch (true) {
			case typeof submittedVals.email!=='undefined':
				trackObj.Email=submittedVals.email;
			case typeof submittedVals.phone!=='undefined':
				trackObj.Phone=submittedVals.phone;
		}
		analytics.track('Вебинар - Регистрация на событие', trackObj, {traits: trackObj});
	}
});

html_forms.on('error', function(formElement) {
	setTimeout(function(){
		for (var i = 0; i < formElement.length; i++) {
			var form=formElement[i],
				preloader=form.querySelector('.js-preloader');
			if (form.querySelectorAll('.hf-message-warning').length) {
				var fields=form.querySelectorAll('.hf-message-warning li');
				for (var x = 0; x < fields.length; x++) {
					form.querySelector('[name="'+fields[x].dataset.field+'"]').classList.add('error');
				}
			}

			if (preloader!==undefined) {
				$(preloader).fadeOut('500');
			}

			form.querySelector('button').disabled=false
		}
	}, 300);
});


// $(document).on('submit', '.form-invite', function(event){
// 	event.preventDefault();
// 	$(this).find('[type=text]').css({'border': '#f66 1px solid'});
// 	if(!$(this).find('.js-form-errors').attr('style')){
// 		// console.log($(this).find('.js-form-errors').attr('style'));
// 		// console.log($(this).find('.js-form-errors'));
// 		$(this).find('.js-form-errors').slideDown(300);
// 	}
// });




// post data to zapier
// console.log(typeof cid);
// if(typeof cid !== 'undefined'){
// 	console.log(typeof $('#fedora-data'));
// 	console.log($('#fedora-data'));
// 	console.log($('#fedora-data').data('report-card'));
// 	var el=$('#fedora-data'),
// 		// repCard=JSON.parse(el.dataset.reportCard.replace('&quot;', '"')),
// 		repCard=JSON.parse(el.data('report-card')),
// 		curcource=repCard[cid],
// 		courseData={
// 		 courseId: cid,
// 		 userData: curcource
// 		}
// 	;
// 	console.log(el);
// 	console.log(repCard);
// 	$.post({
// 		dataType: 'json',
// 		data: repCard,
// 		url: 'https://hooks.zapier.com/hooks/catch/2331074/i9ps3u/'
// 	})
// }

// var couponapply=false;
// var customCouponApply=function(params){
// 	if (typeof params !=='object') {
// 		console.log('Wrong params!');
// 		return;
// 	}
// 	couponapply=true;
// 	if (typeof params.coupon!=='undefined') {
// 		$.get(couponCheckerPath().toString(), params.coupon).then(getData).then(function(){
// 			// console.log(ddata);
// 			if($('.package_el').length){
// 				var thtext=strpt=fullstr=prcWDisc='', discpr='', prid;
// 				if (ddata.discounted_products.length==1) {
// 					$('.package_el').find('.package_price').each(function(){
// 						thtext=strpt=fullstr=prcWDisc='';
// 						if (typeof ddata.discounted_products[0].product_id!=='undefined') {
// 							prid='.buybtn[data-prodid="'+ddata.discounted_products[0].product_id+'"]';
// 							// console.log(prid);
// 							// console.log($(this).closest('.package_el').find(prid));
// 							if ($(this).closest('.package_el').find(prid).length) {
// 								// prcWDisc=parseInt(this.innerHTML.replace(/[^\w\s]/gi, ''))-(parseInt(this.innerHTML.replace(/[^\w\s]/gi, ''))*parseInt(disc)/100);
// 								// fullstr+='$'+prcWDisc.toFixed(2);
// 								fullstr+=ddata.discounted_products[0].formatted_final_price;
// 								strpt='<br><span class="package_yellow"><del>'+$(this).text()+'</del>, скидка '+disc+'%</span>';
// 								fullstr+=strpt;
// 								$(this).html(fullstr);
// 								var btn=$(this).closest('.package_el').find(prid);
// 								btn.off('click');
// 								btn.addClass('couponapply');
// 								var args={coupon:params.coupon.replace('coupon=', ''), target:btn};
// 								buybtnClick(args);
// 							}
// 						}
// 					})
// 				} else {
// 					var ddataprodids=[];
// 					$(ddata.discounted_products).each(function(index) {
// 						ddataprodids.push(this.product_id);
// 					});
// 					// console.log(ddataprodids);
// 					$('.package_el').find('.package_price').each(function(index){
// 						var ind=index,
// 							thisprodid=parseInt($(this).closest('.package_el').find('.buybtn').data('prodid'));
// 						// console.log(thisprodid);
// 						// console.log('ind pre-fix: ',ind);
// 						ind=(ddataprodids.indexOf(thisprodid)!==ind)?ddataprodids.indexOf(thisprodid):ind;
// 						// console.log('ind fixed: ',ind);
// 						thtext=strpt=fullstr=prcWDisc='';
// 						// if (typeof ddata.discounted_products[ind].product_id!=='undefined') {
// 						if (typeof ddata.discounted_products[ind]!=='undefined'&&typeof ddata.discounted_products[ind].product_id!=='undefined') {
// 							prid='.buybtn[data-prodid="'+ddata.discounted_products[ind].product_id+'"]';
// 							// console.log(prid);
// 							// console.log($(this).closest('.package_el').find(prid));
// 							if ($(this).closest('.package_el').find(prid).length) {
// 								// console.log(ddata.discounted_products[ind]);
// 								discpr=ddata.discounted_products[ind];
// 								fullstr+=discpr.formatted_final_price;
// 								strpt='<br><span class="package_yellow"><del>'+$(this).text()+'</del>, скидка '+discpr.discount_percent+'%</span>';
// 								fullstr+=strpt;
// 								$(this).html(fullstr);
//
// 								var btn=$(this).closest('.package_el').find(prid);
// 								btn.off('click');
// 								btn.addClass('couponapply');
// 								var args={coupon:params.coupon.replace('coupon=', ''), target:btn};
// 								buybtnClick(args);
// 							}
// 						}
//
// 						// if (typeof ddata.discounted_products[index]!=='undefined') {
// 						// 	// console.log(ddata.discounted_products[index]);
// 						// 	discpr=ddata.discounted_products[index];
// 						// 	fullstr+=discpr.formatted_final_price;
// 						// 	strpt='<br><span class="package_yellow"><del>'+$(this).text()+'</del>, скидка '+discpr.discount_percent+'%</span>';
// 						// 	fullstr+=strpt;
// 						// 	$(this).html(fullstr);
// 						// }
// 					})
// 				}
// 			}
// 			var bannerEl=$('.coupon_banner'),
// 				addText=(typeof params.couponAddText!=='undefined')?params.couponAddText:'';
// 			if (bannerEl.length) {
// 				if (typeof params.percent=='undefined'||params.percent) {
// 					bannerEl.find('span.coupon-percent').text(ddata.discount_percent+'%');
// 				} else {
// 					var filterDisc=ddata.discounted_products[0].formatted_discount.replace('$', '')+' долл.';
// 					bannerEl.find('span.coupon-percent').text(addText+filterDisc);
// 				}
// 				bannerEl.find('.coupon-invalid-text').remove();
// 				bannerEl.css('display', 'block');
// 				bannerEl.find('.hidden').css('display', 'block').removeClass('hidden');
// 			}
// 		})
// 		// product added to cart
// 		// $('.buybtn').off('click');
//
// 		// $(document).on('click', '.buybtn', function(event) {
// 		// 	event.preventDefault();
// 		// 	var loc = 'https://sso.teachable.com/secure/127625/checkout/confirmation?product_id=' + this.dataset.prodid + '&course_id=' + this.dataset.courseid,
// 		// 		data = [];
// 		// 	// data['prodid']=this.dataset.prodid;
// 		// 	// data['courseid']=this.dataset.courseid;
// 		// 	var couponstr=params.coupon.replace('coupon=', '');
// 		// 	loc += '&coupon_code=' + couponstr;
// 		// 	// setCookie('proddata', data,{expires:3600,path:'/'})
// 		// 	var th = $(this),
// 		// 		q = 1,
// 		// 		pr = parseInt(th.data('price')) / 100,
// 		// 		name = $('body').data('course-name'),
// 		// 		args = {
// 		// 			product_id: $('body').data('course-id'),
// 		// 			sku: th.data('prodid'),
// 		// 			name: name,
// 		// 			brand: 'Heartbeat education',
// 		// 			variant: th.parent().find('.package_h2').text(),
// 		// 			price: pr,
// 		// 			quantity: 1,
// 		// 			coupon: code,
// 		// 			currency: 'USD',
// 		// 			value: pr * q,
// 		// 			url: document.location.href,
// 		// 			image_url: $('body').find('.topimg_bg').attr('src')
// 		// 		};
// 		// 	args.value -= args.value * disc / 100;
// 		// 	// console.log(args);
// 		// 	analytics.track('Product Added', args);
// 		// 	window.location.href = loc;
// 		// });
//
// 	}
// };


// $(document).on('submit', '.js-form', function(event){
// 	event.preventDefault();
// 	var form=$(this),
// 		data=[];
// 	form.find('input,textarea').each(function(ind){
// 		data[$(this).attr('name')]=$(this).val();
// 	})
// 	// var res=sendData($(data).serialize(), {url:form.attr('action'), type:'POST'});
// 	// console.log(data);
// 	var res=sendData(data, {url:form.attr('action'), type:'POST'});
// 	$.when( res ).then(
// 		function( result ) {
// 			// console.log('1');
// 			console.log(result);
// 		},
// 		function( result ) {
// 			// console.log('2');
// 			console.log(result);
// 		}
// 	);
// });
// $('.js-customreg').on('submit', function(event){
// 	event.preventDefault();
// 	var rdata=$('.js-customreg').serialize();
// 	sendData(rdata, {url:'https://hooks.zapier.com/hooks/catch/2331074/8bqtae/', type:'GET'});
// 	setTimeout(function(){
// 		window.location.href='https://heartbeat.education/p/5012';
// 	})
// });

// $(document).on('click', '.js-registerBtn', function(event){
// 	if (typeof fedoraData!=='undefined') {
// 		if ( typeof fedoraData.dataset.email!=='undefined') {
// 			event.preventDefault();
// 			window.location.href+='/p/authors';
// 		}
// 	}
// });

// if ($('.js-curriculum').length) {
// 	$('.js-curriculum').each(function(index){
// 		$(this).find('.collapse_item').first().addClass('active');
// 	})
// }

// $(window).load(function(){
// 	setTimeout(function(){
// 		if ($('.js-mcSubmit').length) {
// 			$(document).on('click', '.js-mcSubmit', function(event){
// 				var form=$(this).closest('form');
// 				if (typeof MC!=='undefined'&&form.find('.mcwidget-checkbox').length) {
// 					var widg_id=form.find('.mcwidget-checkbox').data('widget-id'),
// 						widget=MC.getWidget(widg_id);
// 					// console.log(widg_id);
// 					// console.log("MC: ",MC);
// 			        // console.log("MC.getWidget(): ", widget);
// 					if (widget.checked&&widget.ref===null) {
// 						widget.submit();
// 					}
// 				}
// 			});
// 		}
// 	}, 1000)
// })
