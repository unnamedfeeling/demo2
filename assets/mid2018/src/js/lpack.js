;(function(factory) {
	'use strict';
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	} else if (typeof exports !== 'undefined') {
		module.exports = factory(require('jquery'));
	} else {
		factory(jQuery);
	}

}(function($) {
	'use strict';
	var Lpack = window.Lpack || {};
	var gheight = 0;

	Lpack = (function() {

		var instanceUid = 0;

		function Lpack(element, settings) {
			var _ = this,
			defsettings={
				synch:false
			};
			_.def = {
				tel : $(element), 
				head : $(element).find('.lpack_head'),
				reflactopr :$(element).find('.lpack_head-reflactor'),
				settings: $.extend(defsettings, settings)
			}
			
			_.init(true);
		}
		return	Lpack;
	}());

	Lpack.prototype.init = function(){

		var _ = this;
		_.lpackcount();
		$(window).on('scroll', $.proxy(_.lpackscrl, _) );
		$(window).on('resize', $.proxy(_.lpackres, _) );
		_.def.tel.addClass('lpack_innit');
		/*
			if(_.def.settings.synch && $('.lpack_el')[$('.lpack_el').length-1] == _.def.tel[0]){
				$('.lpack_el').lpack('lpackres');
			}
			// Закоментировал, так как тут появляется ошибка, по непонятной причине, вроде на функциональность не влияет.
			//custom.js:357 Uncaught TypeError: Cannot read property 'lpackres' of undefined
			*/
		}

		Lpack.prototype.lpackscrl = function(event) {

			var  _ = this,
			top = (document.documentElement.scrollTop || document.body.scrollTop)+$('header').innerHeight();
			debugger;
			if(top >= _.def.offsettop ){
				var ttop=top- _.def.offsettop;
				_.def.head.css({
					top: ttop+'px',
				});
			}
			if(top <= _.def.offsettop ){
				_.def.head.css({
					top: '0px',
				});
			}
			if(top >( _.def.bottom )){
				_.def.head.css({
					top:  (_.def.telheight-_.def.headheight )+'px',
				});
			}
		};
		Lpack.prototype.lpackres = function(rec) {

			var _ = this;
			clearTimeout(_.windowDelay);
			_.windowDelay = window.setTimeout(function() {
				_.lpackcount();
			}, 60);
		}
		Lpack.prototype.lpackcount = function(rec) {

			var _ = this;

			console.log(_.def.settings.synch);
			if(_.def.settings.synch){
				$('.lpack_el').height('auto');
				$('.lpack_el').height($('.lpack_el').rmax());
				_.def.max = $('.lpack_el').rmax();
			} else {_.def.max = _.def.tel.innerHeight();}
			_.def.headheight = _.def.head.innerHeight();
			_.def.reflactopr.height(_.def.headheight);
			_.def.telheight=  _.def.max;
			_.def.offsettop = _.def.tel.offset().top;
			_.def.bottom = _.def.offsettop + _.def.telheight-_.def.headheight;
			_.lpackscrl();
		/*console.log(
			_.def.headheight,
			_.def.telheight,
			_.def.offsettop,
			_.def.bottom,
			)*/
		}
		Lpack.prototype.pack = function(){

			var _ = this;
			return _;
		}
		$.fn.lpack = function() {
			var _ = this,
			opt = arguments[0],
			args = Array.prototype.slice.call(arguments, 1),
			l = _.length,
			i,
			ret;
			for (i = 0; i < l; i++) {
				if (typeof opt == 'object' || typeof opt == 'undefined')
					_[i].lpack = new Lpack(_[i], opt);
				else
					ret = _[i].lpack[opt].apply(_[i].lpack, args);
				if (typeof ret != 'undefined') return ret;
			}
			return _;
		};

	}));
