<?php
namespace Heartweb;

use Heartweb\Session;
use Heartweb\DripIntegration;
/**
 *
 */
class Webinar{
  private $apiKey;
  private $apiUrl;
  private $webinarData;
  private $webid;
  private $platform;
  private $sessions=[];
  private $msgPath=__DIR__.'/../../../messages/';
  private $p='heartweb_';

  private $time;

  function __construct($params){
    $p=$this->p;
    $this->time=time();

    $this->pid=$params['postId'];
    $this->postmeta=$this->getPostmeta($this->pid);

    $this->platform=$this->postmeta[$this->p.'webinarPlatform'][0];
    $this->setApiKey();

    $this->webid=$this->setWebId();
    $this->webinarsOutputCount=(!empty($this->postmeta[$this->p.'webinarsOutputCount'][0]))?$this->postmeta[$this->p.'webinarsOutputCount'][0]:2;
    $this->apiUrl=$this->getApiUrl();

    $this->funcs=$this->getFunctions();

    $this->dripApiKey=(!empty($params[$p.'dripApiKey']))?$params[$p.'dripApiKey']:$this->apiKey;
    $this->dripAccountId=(!empty($params[$p.'dripAccountId']))?$params[$p.'dripAccountId']:$this->accountId;

    // $this->sessions=$this->getSessionData();
  }

  function __get($name){
    if (isset($this->{$name})) {
      return $this->{$name};
    }
  }

  private function setApiKey(){
    $key=(!empty($this->postmeta[$this->p.'apikey-'.$this->platform][0]))?$this->postmeta[$this->p.'apikey-'.$this->platform][0]:'';
    $this->apiKey=(!empty($key))?$key:(($this->platform=='bigmarker')?'key1':'key2');
  }

  private function getApiUrl(){
    $platform=$this->platform;
    $url='';
    switch ($platform) {
      case 'webinarjam':
        $url='https://webinarjam.genndi.com/api/';
        break;

      default:
        $url='https://www.bigmarker.com/api/v1/';
        break;
    }

    return $url;
  }

  public function setWebId(){
    $p=$this->p;
    $webid='';
    if ($this->platform=='bigmarker') {
			$sessLength=$this->postmeta[$p.'regLimitSessions'][0];
			for ($i=0; $i < $sessLength; $i++) {
				$webid.=sprintf(
					'%s%s',
					($i!==0)?',':'',
					$this->postmeta[$p.'regLimitSessions_'.$i.'_session'][0]
				);
			}
		} else {
			$webid=$this->postmeta[$p.'webid-'.$this->platform][0];
		}

    return $webid;
  }

  public function getWebId(){
    return $this->webid;
  }

  public function getPostmeta($pid, $return=true){
    $pid=(!empty($pid))?$pid:$this->pid;
    $this->postmeta=get_post_meta($pid, '', false);

    if ($return) {
      return $this->postmeta;
    }
  }

  public function getSessionData(){
  	$result=[];
    $p=$this->p;
    $meta=$this->postmeta;

  	switch (true) {
  		case (!empty(get_field($p.'regLimitSessions', $this->pid))):
  			$data=get_field($p.'regLimitSessions', $this->pid);
  			$result=[];
  			foreach ($data as $key => $val) {
  				$p=$val['newxtwebinar'];
  				if (!empty($p)) {
  					$val['nextwebinarLink']=get_permalink($p);
  					$val['nextwebinarName']=get_the_title($p);
  					$val['nextwebinarId']=$p;
  					$val['nextwebinarSessionId']=$data[$key+1]['session'];
  				}
  				unset($val['newxtwebinar']);

          $sessionArgs=[
            'id' => $val['session'],
            'apiUrl' => $this->apiUrl,
            'apiKey' => $this->apiKey,
          ];

          $session=new Session($sessionArgs);
          $val['regCount']=$session->getRegistrationCount();
          $val['sessionData']=$session->getSessionData(['func'=>$this->funcs['conference']]);

          if (!empty($val['sessionData']->start_time)) {
            $start=strtotime($val['sessionData']->start_time);

            if ($start>$this->time) {
              $result[]=$val;
            }
          }
  			}

        if (count($result)>1) {
          usort($result, function( $a, $b ) {
             return strtotime($a['sessionData']->start_time) - strtotime($b['sessionData']->start_time);
          });

          $result = array_slice($result, 0, $this->webinarsOutputCount);
        }

  			break;

  		default:
  			$result=0;
  			break;
  	}

  	return $result;
  }

  public function register($params){
    $p=$this->p;

    $sessionArgs=[
      'id' => $params['webid'],
      'apiUrl' => $this->apiUrl,
      'apiKey' => $this->apiKey,
      'func' => $this->funcs['register'],
    ];
    $session=new Session($sessionArgs);
    $registration=$session->registerUser($params);
    $this->bmid=$registration['bmid'];

    if ($registration['success']) {
      $params['smsConf']=get_field($p.'sms-config', $params['postId']);
      $params['webinarName']=(!empty($params['webinarName']))?$params['webinarName']:get_the_title($this->pid);
      $params['curTime']=$this->time;
      $params['bmid']=$this->bmid;

      $sms=new Sms($params);
      $sendResult=$sms->send();

      $this->writeMessage($this->bmid);

      if (!empty($this->postmeta[$p.'dripActive'][0])) {
        $sessionData=$session->getSessionData(['func'=>$this->funcs['conference']]);
        $dripArgs=[
          'accountId' => $this->postmeta[$p.'dripAccountId'][0]
        ];
        $sessionUnixTime=strtotime($sessionData->start_time);
        $dripIntegration=new DripIntegration($dripArgs);

        $eventArgs=[
          'email'=>$params['email'],
          'action'=>'Registered for webinar - '.$params['webinarName'].'('.$params['webid'].')',
          'properties'=> [
            'sessionId' => $params['webid'],
            'sessionUnixtime' => $sessionUnixTime,
            'sessionDate' => date('d.m.Y', $sessionUnixTime),
            'sessionTime' => date('H:i', $sessionUnixTime),
            'sessionRoomLink' => $sessionData->conference_address,
          ]
        ];
        $dripIntegration->drip->record_event($eventArgs);
      }
    }

    // return ['status'=> $registration, 'sms'=>$sms, 'smsResult'=>$sendResult, 'eventArgs'=>$eventArgs, 'sessionData'=>$sessionData];
    return ['status'=> $registration];
  }

  public function checkRegistrationData($params){
    $result=[
  		'check'=>true
  	];

  	if (!empty($params['reqFields'])) {
  		$req=explode(',', $params['reqFields']);
  		$t='';
  		$n=[];
  		$trans=[
  			'name' => 'имя',
  			'userName' => 'имя',
  			'email' => 'почта',
  			'date' => 'дата',
  			'phone' => 'телефон',
  			'time' => 'время'
  		];
  		foreach ($req as $val) {
  			if (empty($params[$val])||($val=='time'&&$params[$val]=='Сперва выберите дату')||($val=='date'&&$params[$val]=='Выберите дату')) {
  				$t.='<li data-field="'.$val.'">'.strtr($val, $trans).'</li>';
  				$n[]=$val;
  			}
  		}
  		if(!empty($t)){
  			$result['check'] = false;

  			$result['names'] =$n;
  			$result['data'] = '<p>Мы не можем обработать Вашу регистрацию. Вам необходимо указать:</p> <ul>' . $t . '</ul>';
  		}
  	}

  	return $result;
  }

  private function writeMessage($phone){
    $fsms = fopen($this->msgPath.'site-'.$phone.'-'.date('D-m-Y--H-i-s').'.txt', 'w');
    fwrite($fsms, json_encode($this));
    fclose($fsms);
  }

  private function getFunctions(){
    $funcs=[];
    switch ($this->platform) {
      case 'webinarjam':
        $funcs=[
          'conference'=>'conference/'
        ];
        break;

      default:
        $funcs=[
          'conference'=>'conferences/',
          'register'=>'conferences/register',
        ];
        break;
    }

    return $funcs;
  }
}
