<?php
namespace Heartweb;

use WP_Http;
use WP_Error;
/**
 *
 */
class Sms
{
  private $smsPath=__DIR__.'/../../../sms/';
  private $msgPath=__DIR__.'/../../../messages/';

  function __construct($params){
    $this->params=$params;
    $this->userPhone=$this->getPhones($this->params);
    $this->smsRep=[
      '{bmid}'=>$params['bmid'],
      '{userName}'=>$params['userName'],
      '{userEmail}'=>$params['email'],
      '{userPhone}'=>$this->userPhone,
      '{webinarName}'=>(!empty($params['webinarName']))?$params['webinarName']:((!empty($post->post_title))?$post->post_title:null),
      '{webinarDate}'=>(!empty($params['curTime']))?date('d.m.Y H:i', $params['curTime']):null,
    ];

    $this->smsData=$this->setSmsData();

    $this->alreadySent=(file_exists($this->smsPath.$this->userPhone.'.txt'))?true:false;
    $this->timeDiff=($this->alreadySent)?(int)$params['curTime']-(int)file_get_contents($this->smsPath.$this->userPhone.'.txt'):false;
  }

  public function send(){
    $checkData=$this->checkSmsData($this->smsData);
    switch (true) {
      case (is_array($checkData)&&is_wp_error( $checkData[0] )):
      case (!is_bool($this->timeDiff)&&$this->timeDiff<=10800):
        return false;
        break;
    }

    $args=[
      'headers' => [
        'Authorization' => 'Basic ' . base64_encode( "" ),
      ],
      'body' => $this->smsData,
      'blocking' => true,
      'method'=>'POST',
    ];

    $request=$this->request('', $args);

    if (!is_wp_error($request)) {
      $this->writeSms();
      $this->writeMessage($request);
    } else {
      $this->writeError($request);
    }

    return [
      'args' => $args,
      'smsData' => $this->smsData,
      'request' => $request,
      'timediff' => $this->timeDiff
    ];
  }

  private function writeError($data){
    $fsms = fopen($this->msgPath.'error-'.$this->params['bmid'].'-'.date('d-m-Y--H-i-s').'.txt', 'w');
    fwrite($fsms, json_encode($data));
    fclose($fsms);
  }

  private function writeMessage($data){
    $fsms = fopen($this->msgPath.'responce-bm-'.$this->params['bmid'].'-'.date('d-m-Y--H-i-s').'.txt', 'w');
    fwrite($fsms, json_encode($data));
    fclose($fsms);
  }

  private function writeSms(){
    $fsms = fopen($this->smsPath.$this->userPhone.'.txt', 'w');
    fwrite($fsms, $this->params['curTime']);
    fclose($fsms);
  }

  private function request($url, $args){
    $http=new WP_Http();

    return $http->request($url, $args);
  }

  private function checkSmsData($data){
    $check=true;

    foreach ($data as $key => $val) {
      if (empty($val)) {
        if (!is_array($check)) {
          $check=[];
        }
        $check[]=new WP_Error('dataError', 'Empty data for SMS: '.$key);
      }
    }

    return $check;
  }

  private function getPhones($args){
    $phones='';

    switch (true) {
      case (!empty($args['phonedata'])):
        $phones=$args['phonedata'];
        break;
      case (!empty($args['phone'])):
        $phones=$args['phone'];
        break;

      default:
        $phones=null;
        break;
    }

  	if (empty($args['phonedata'])&&!empty($phones)) {
    	$countryData=(!empty($args['countryData']))?json_decode($args['countryData']):null;
  		$phone=(mb_strpos( $args['phone'], '0' )===0&&$countryData->iso2=='ua')?mb_substr($args, 1):$args;
  		$phones=(!empty($countryData))?$countryData->dialCode.$phone:'+380';
  	}

    return $phones;
  }

  private function getSmsText($args, $fulltext=null){
    if (!empty($fulltext)) {
      return $fulltext;
    }

    $text='';

    if (!empty($args['smsConf'])) {
      $smsConf=$args['smsConf'];

      if (count($smsConf)>0){
        foreach ($smsConf as $sms) {
          if (empty($sms['sms_time'])||$args['curTime']<$sms['sms_time']) {
            $text=(!empty($sms['sms_text']))?$sms['sms_text']:'';
            break;
          }
        }
      }

      $text=strtr($text, $this->smsRep);
    }

    return $text;
  }

  private function setSmsData(){
    $smsData=[
      'sms_phones'=>$this->userPhone,
      'type'=>'groupSms',
      'key'=>'salepartnersleadadssms',
      'sms_text'=>$this->getSmsText($this->params),
    ];

    return $smsData;
  }
}
