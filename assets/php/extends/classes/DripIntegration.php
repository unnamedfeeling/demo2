<?php
namespace Heartweb;

use Drip;
/**
 *
 */
class DripIntegration
{
  private $apiKey='';
  private $accountId='';

  function __construct($params)
  {
    $this->apiKey=(!empty($params['apiKey']))?$params['apiKey']:$this->apiKey;
    $this->accountId=(!empty($params['accountId']))?$params['accountId']:$this->accountId;

    $this->drip=new Drip\Client($this->apiKey, $this->accountId);
  }
}
