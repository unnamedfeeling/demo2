<?php
namespace Heartweb;

use WP_Http;
use Heartweb\Sms;
use Heartweb\User;
use Heartweb\DripIntegration;
/**
 *
 */
class Session{
  private $sessionId;
  private $sessionData;

  function __get($name){
    if (isset($this->{$name})) {
      return $this->{$name};
    }
  }

  function __construct($params){
    $this->sessionId=$params['id'];
    $this->apiUrl=$params['apiUrl'];
    $this->apiKey=$params['apiKey'];

    $this->rCountOpt='regcount_'.$this->sessionId;
    if (!empty($params['func'])) {
      $this->func=$params['func'];
    }
  }

  public function getSessionData($reqArgs){
    $result='';
    $id=$this->sessionId;
    $regTransientData=get_transient( 'webinar_'.$id );
    if (!$regTransientData) {
      $args=[
        'headers' => [
          "API-KEY" => $this->apiKey,
          "cache-control" => "no-cache"
        ],
        'blocking' => true,
        'method'=>'GET',
        'decompress'=>false
      ];
      $data=$this->request($this->apiUrl.$reqArgs['func'].$id, $args);

      if (!is_wp_error( $data )) {
        $result=$data['body'];
        set_transient( 'webinar_'.$id, $data['body'], DAY_IN_SECONDS );
      } else {
        $result=$data;
      }
    } else {
      $result=$regTransientData;
    }

    return json_decode($result);
  }

  private function request($url, $args){
    $http=new WP_Http();

    return $http->request($url, $args);
  }

  public function registerUser($args){
    $result=['success'=>false];

    $userObj=new User($args);

    $user=$userObj->getUser();

    if (is_wp_error($user)) {
      $user=$userObj->createUser();
    } else {
      $userObj->updateUserSessionregistrationData();
    }

    $data='';

    try {
      $query=[
        'id'=> $this->sessionId,
        'email' => $args['email'],
        'first_name' => $args['userName'],
        'last_name' => '',
      ];
      $query=http_build_query($query);
      $args=[
        'headers' => [
          "API-KEY" => $this->apiKey,
          "cache-control" => "no-cache"
        ],
        'blocking' => true,
        'method'=>'POST',
        'decompress'=>false
      ];
      $data=$this->request($this->apiUrl.$this->func.'?'.$query, $args);

      if (!empty($data['body'])) {
        $body=json_decode( $data['body'] );
        if ($body->conference_url) {
          $re = '/\?bmid=([\S]{12})/m';
          $str= $body->conference_url;
          preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);

          $bmid=(!empty($matches[0][1]))?$matches[0][1]:'';

          if (!empty($bmid)) {
            $userObj->setCustField('heartweb_bmid', $bmid);
            $result['bmid']=$bmid;
          }
          $this->registrationTick();
          $result['success']=true;
        } else {
          $result['msg']=$body->error;
        }
      }
    } catch (\Exception $e) {
      $data=$e->getMessage();
      $result['msg']=$data;
    }

    return $result;
  }

  public function getRegistrationCount(){
    $regcount=get_option('regcount_'.$this->sessionId);

    if (!$regcount) {
      update_option( $this->rCountOpt, 0, false );
      $regcount=0;
    }

    return (int)$regcount;
  }

  private function registrationTick(){
    $regcount=(int)get_option($this->rCountOpt, 0)+1;

    update_option( $this->rCountOpt, $regcount, false );
  }
}
