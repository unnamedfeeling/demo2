<?php
namespace Heartweb;

use WP_User_Query;
use WP_Error;
/**
 *
 */
class User{
  private $userId;
  private $userData;

  function __construct($args){
    $this->email=$args['email'];
    $this->args=$args;

    switch (true) {
      case (!empty($args['phonedata'])):
        $phone=$args['phonedata'];
        break;
      case (!empty($args['phone'])):
        $phone=$args['phone'];
        break;

      default:
        $phone='';
        break;
    }
    $this->phone=$phone;
  }

  public function getUser(){
    $args=[
      'search'=>$this->email,
      'search_columns'=>['user_email'],
    ];
    $findUser=new WP_User_Query( $args );
    $foundUsers=$findUser->get_results();

    $user=(count($foundUsers)>0)?$foundUsers[0]:new WP_Error();

    if (!is_wp_error( $user )) {
      $this->userId=$user->ID;
      $this->userData=$this->getUserData($this->userId);
    }

    return $user;
  }

  public function createUser(){
    $userdata = array(
      'user_login'    =>  $this->email,
      'user_email'    =>  $this->email,
      'user_pass'     =>  'wRTU9T153GA0CxJTpXnP',
      'first_name'    =>  $this->email,
      'description'   =>  'This user first registered on '.date('d/m/Y H:i'),
      'role' => ''
    );

    $user = wp_insert_user( $userdata );
    $this->userId=$user;

    $this->updateUserSessionregistrationData();

    return $this->getUser();
  }

  public function getUserData($id){
    return get_user_meta( $id, '', false );
  }

  public function returnUserData(){
    return $this->userData;
  }

  public function updateUserSessionregistrationData(){
    if (!empty($this->phone)) {
      update_user_meta( $this->userId, 'heartweb_userPhone', $this->phone );
    }

    $rowArgs=[
      'webname'=> $this->args['webinarName'],
      'session'=> $this->args['webid'],
    ];
    add_row('heartweb_registeredOn', $rowArgs, "user_{$this->userId}");
  }

  public function setCustField($CFname, $data, $overwrite=true){
    if (!empty($CFname)&&!empty($data)) {
      $meta=get_user_meta( $this->userId, $CFname, true );
      if (!$meta||($meta&&$overwrite)) {
        update_user_meta( $this->userId, $CFname, $data );
      }
    }
  }
}
