<?php
use Heartweb\Webinar;

function heartweb_sendSms($params){
	global $options;
	// $params=[
	// 	'phone' => String $phone,
	// 	'text' => String $text,
	// ];
	$t=time();

	$responce=[];

	// send sms
	if (!empty($params['phone'])&&!empty($params['text'])) {
		try {
			$smsPath=__DIR__.'/../sms/';
			$msgPath=__DIR__.'/../messages/';
			// Подключаемся к серверу
			$client = new SoapClient('http://turbosms.in.ua/api/wsdl.html');
			// Данные авторизации
			$auth = [
				'login' => 'E2uAUiB',
				'password' => 'FyfWyQjFeWjFDT4rKgBQCUBybws2T4'
			];
			// Авторизируемся на сервере
			$result = $client->Auth($auth);
			// Результат авторизации
			$responce['turboSMS']['auth']= $result->AuthResult . PHP_EOL;
			// Получаем количество доступных кредитов
			$responce['turboSMS']['balance'] = $client->GetCreditBalance();
			$text = $params['text'];
			$smsPhone=$params['phone'];
			$sms = [
				'sender' => 'Heartbeat',
				'destination' => $smsPhone,
				'text' => $text
			];
			$timediff=(file_exists($smsPath.$smsPhone.'.txt'))?time()-file_get_contents(__DIR__.'/../sms/'.$smsPhone.'.txt'):false;
			if (!file_exists($smsPath.$smsPhone.'.txt')||$timediff>10800) {
				$type='wj';
				$ttl=$params['webinarName'];
				// $tsms=json_decode( $responce['turboSMS'] );
				$responce['turboSMS']['sendResult'] = $client->SendSMS($sms);

				// Запрашиваем статус конкретного сообщения по ID
				// $sms = ['MessageId' => 'c9482a41-27d1-44f8-bd5c-d34104ca5ba9'];
				// $status = $client->GetMessageStatus($sms);
				// echo $status->GetMessageStatusResult . PHP_EOL;

				$fsms = fopen($msgPath.'responce-bm-'.$smsPhone.'.txt', 'w');
				fwrite($fsms, json_encode($responce['turboSMS']));
				fclose($fsms);

				try {
					$tsms=var_export($responce['turboSMS'], true);
					if (!empty($responce['turboSMS']['sendResult'])&&$responce['turboSMS']['sendResult']->SendSMSResult->ResultArray[0]==='Сообщения успешно отправлены') {
						$fsms = fopen($smsPath.$smsPhone.'.txt', 'w');
						fwrite($fsms, time());
						fclose($fsms);
					} else {
						$fsms = fopen($smsPath.$smsPhone.'.txt', 'w');
						fwrite($fsms, json_encode($responce['turboSMS']['sendResult']));
						fclose($fsms);
					}
				} catch (Exception $e) {
					$tsms=var_export($e, true);
					$fsms = fopen($smsPath.'Error-'.$smsPhone.'.txt', 'w');
					fwrite($fsms, $e);
					fclose($fsms);
				}
			}
		} catch(Exception $e) {
			$tsms=var_export($e, true);
			$responce['turboSMS']['error'] = 'Ошибка: ' . $e->getMessage() . PHP_EOL;
			$fsms = fopen($smsPath.'Error-'.time().'.txt', 'w');
			fwrite($fsms, $responce['turboSMS']['error']);
			fclose($fsms);
		}
	}



	// write logdata
	// $responce['result']='OK';
	$responce['date']=date('d-m-Y--H-i-s', time());
	// $responce['curtime']=time();
	$responce['filename']=$msgPath.'site-'.$responce['date'].'.txt';
	$logdata=json_encode($responce);
	$fp = fopen($responce['filename'], 'w');
	fwrite($fp, $logdata);
	fclose($fp);

	return $responce;
}


// site api routes - for registration, validation, form submissions, etc.
function heartweb_webinarData(WP_REST_Request $request){
	global $options;
	$responce=[];
	$p=$options['prfx'];
	$params = $request->get_params();
	$webinar=new Webinar($params);
	$pmeta=$webinar->postmeta;
	// $limitData=heartweb_getLimitData($pmeta, $params['postId']);

	$curTime=$webinar->time;


	$apiUrl='';
	// $func=(!empty($params['func']))?$params['func']:null;
	// $responce['func']=$func;
	$webid=(!empty($params['webid']))?$params['webid']:null;
	$responce['webid']=$webid;
	switch ($params['webinarType']) {
		case 'bigmarker':
			$limitData=$webinar->getSessionData();
			$responce['sessions']=$limitData;

			break;

		default:
			// webinarjam
			$key=(!empty($key))?$key:'e8e0b1d2cf663a10d945027a5b322568b0bc3ee38513cb7f6d3866e84d1216ed';
			$apiUrl='https://webinarjam.genndi.com/api/';

			if (!empty($func)||!empty($webid)) {
				$opts = [
					CURLOPT_URL			   => $apiUrl.$func,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POST		   => true,
				];

				switch ($func) {
					case 'webinar':

						$ch = curl_init();
						curl_setopt_array($ch, $opts);
						curl_setopt($ch, CURLOPT_POSTFIELDS, 'api_key='.$key.'&webinar_id='.$params['webid']);
						$data=curl_exec($ch);
						curl_close($ch);
						$responce['getWebinar']=json_decode($data);

						$scheds=(!empty($responce['getWebinar']->webinar))?$responce['getWebinar']->webinar->schedules:[];
						$responce['prevsched']=(!empty($scheds)&&count($scheds)>1)?strtotime ($scheds[count($scheds)-2]->date):null;
						break;

					default:
						$opts = array(
							CURLOPT_URL			   => $apiUrl."webinars",
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_POST		   => true,
						);
						$ch = curl_init();
						curl_setopt_array($ch, $opts);
						curl_setopt($ch, CURLOPT_POSTFIELDS, 'api_key='.$key);
						$data=curl_exec($ch);
						curl_close($ch);
						$responce['getWebinars']=$data;
						break;
				}
				break;
			}

	}

	return wp_send_json_success($responce);
}


function heartweb_registerForWebinar($submission){
	if (empty($options)) {
		global $options;
		$options=generic_globals();
	}

	$p=$options['prfx'];
	$tpl=$options['tpld'];
	$params=$submission->data;
	$responce=[];

	$webinar=new Webinar($params);
	$sessionRegistration=$webinar->register($params);

	$responce=[
		// 'webinar'=> $webinar,
		'session'=> $sessionRegistration,
	];

	return wp_send_json($responce);


	$apiUrl='';
	$post=(!empty($params['postId']))?get_post($params['postId']):null;
	$form=(!empty($submission->form_id))?hf_get_form($submission->form_id):null;
	$pmeta=(!empty($params['postId']))?get_post_meta($params['postId'], '', false):null;
	$func='register';
	$webid=(!empty($params['webid']))?$params['webid']:((!empty($pmeta[$p.'webid-'.$pmeta[$p.'webinarPlatform'][0]][0]))?$pmeta[$p.'webid-'.$pmeta[$p.'webinarPlatform'][0]][0]:null);
	// die(json_encode($pmeta));
	$type=(!isset($params['webinarType']))?$pmeta[$p.'webinarPlatform'][0]:$params['webinarType'];
	$params['webinarType']=$type;

	$curTime=time();
	$smsText='';
	$smsConf=get_field($p.'sms-config', $params['postId']);
	// die(var_dump(get_field($p.'sms-config', $params['postId'])));
	if (!empty($smsConf)) {
		switch (true) {
			case (count($smsConf)>0):
				foreach ($smsConf as $sms) {
					if (empty($sms['sms_time'])||$curTime<$sms['sms_time']) {
						$smsText=(!empty($sms['sms_text']))?$sms['sms_text']:'';
					}
				}
				break;
			// case (count($smsConf)==1):
			// 	$smsText=(!empty($pmeta[$p.'sms-config'][0][0]['sms_text']))?$pmeta[$p.'sms-config'][0][0]['sms_text']:'';
			// 	break;

			default:
				$smsText='';
				break;
		}
	}

	switch (true) {
		case (!empty($params['phonedata'])):
			$phone=$params['phonedata'];
			break;
		case (!empty($params['phone'])):
			$phone=$params['phone'];
			break;

		default:
			$phone=null;
			break;
	}


	$countryData=(!empty($params['countryData']))?json_decode($params['countryData']):null;
	if (empty($params['phonedata'])&&!empty($phone)) {
		$phone=(mb_strpos( $params['phone'], '0' )===0&&$countryData->iso2=='ua')?mb_substr($phone, 1):$phone;
		$phone=(!empty($countryData))?$countryData->dialCode.$phone:'+380';
	}

	$name=(!empty($params['userName']))?$params['userName']:null;
	$email=(!empty($params['email']))?$params['email']:null;

	$smsRep=[
		'{userName}'=>$name,
		'{userEmail}'=>$email,
		'{userPhone}'=>$phone,
		'{webinarName}'=>(!empty($params['webinarName']))?$params['webinarName']:((!empty($post->post_title))?$post->post_title:null),
		'{webinarDate}'=>(!empty($params['time']))?$params['time']:null,
	];

	$userConf=['email'=>$email, 'name'=>$name];
	if (!empty($phone)) {
		$userConf['phone']=$phone;
	}

	switch ($params['webinarType']) {
		case 'bigmarker':
			$key=(!empty($pmeta[$p.'apikey-'.$pmeta[$p.'webinarPlatform'][0]][0]))?$pmeta[$p.'apikey-'.$pmeta[$p.'webinarPlatform'][0]][0]:'7b7eb89e7d7c49f74850';
			$apiUrl='https://www.bigmarker.com/api/v1/';

			if (!empty($func)||!empty($webid)) {

				$trans=[
					'webinar'=>'conferences/'.$params['webid'],
					'register'=>'conferences/register',
				];
				$func=strtr($func, $trans);

				$opts = [
					CURLOPT_URL			   => $apiUrl.$func,
					CURLOPT_RETURNTRANSFER => true,
					// CURLOPT_POST		   => true,
					CURLOPT_HTTPHEADER => [
						"API-KEY: ".$key,
						"cache-control: no-cache"
					]
				];

				switch ($func) {
					case 'conferences/'.$webid:
						$ch = curl_init();
						$opts[CURLOPT_CUSTOMREQUEST]='GET';
						curl_setopt_array($ch, $opts);
						$data=curl_exec($ch);
						curl_close($ch);
						// $responce['getWebinarUrl']=$opts;
						$responce['getWebinar']=json_decode($data);
						break;
					case 'conferences/register':
						$check=heartweb_checkRegistrationData($params);
						if ($check['check']==false) {
							$responce['error']=[
								'type'=>'validation',
								'data'=>$check['data'],
								'names'=>$check['names'],
							];
						} else {
							try {
								$newConfArr=[];
								if (strpos($webid, ',')>1) {
									$confOpts=$opts;
									$confOpts[CURLOPT_URL]=$apiUrl.'conferences/';
									$arr=explode(',', $webid);

									$ch = curl_init();
									$opts[CURLOPT_CUSTOMREQUEST]='GET';
									curl_setopt_array($ch, $confOpts);
									$conferences=json_decode(curl_exec($ch));
									curl_close($ch);

									$newConfArr=array_filter($conferences->conferences, function($el) use ($arr){
										if (in_array($el->id, $arr)) {
											$confTime=strtotime($el->start_time);
											return $confTime>time();
										}
									});
									$newConfArr=array_values($newConfArr);
									// print_r($newConfArr);
								}

								$ch = curl_init();
								$opts[CURLOPT_CUSTOMREQUEST]='POST';
								curl_setopt_array($ch, $opts);
								curl_setopt($ch, CURLOPT_POSTFIELDS, 'id='.((!empty($newConfArr[0]))?$newConfArr[0]->id:$webid).'&email='.$params['email'].'&first_name='.$params['userName'].'&last_name=');
								$data=curl_exec($ch);
								curl_close($ch);
								$responce['register']=$data;

								if (!empty($responce['register'])) {
									$responce['ticker']=heartweb_tickRegistrationCounter($params['postId'], $webid);

									$user=heartweb_createUser($userConf);
									$registeredUsers=(!empty($pmeta['__heartweb_registeredUsers'][0]))?maybe_unserialize( $pmeta['__heartweb_registeredUsers'][0] ):[$webid=>[]];

									if (!is_array($registeredUsers)) {
										$registeredUsers=[$webid=>[]];
									}
									if (empty($user['error'])) {
										$regUserMeta=[
											'user'=>$user['userid'],
											'loginUrl'=>json_decode($data)->conference_url
										];
										if (!empty($phone)) {
											$regUserMeta['phone']=$phone;
										}
										$registeredUsers[$webid][]=$regUserMeta;
										update_post_meta( $params['postId'], '__heartweb_registeredUsers', $registeredUsers );
									}

									if (!empty($smsText)&&!empty($phone)) {
										$smsText=strtr($smsText, $smsRep);
										$responce['sms']=heartweb_sendSms(['phone'=>$phone, 'text'=>$smsText, 'time'=>$curTime, 'type'=>$params['webinarType']]);
									}
								}

							} catch (\Exception $e) {
								$responce['error']=[
									'type'=>'fatal',
									'data'=>$e
								];
							}
						}

						break;
				}
			}

			break;

		default:
			// webinarjam
			$key=(!empty($pmeta[$p.'apikey-'.$pmeta[$p.'webinarPlatform'][0]][0]))?$pmeta[$p.'apikey-'.$pmeta[$p.'webinarPlatform'][0]][0]:'e8e0b1d2cf663a10d945027a5b322568b0bc3ee38513cb7f6d3866e84d1216ed';
			$apiUrl='https://webinarjam.genndi.com/api/';

			if (!empty($func)||!empty($webid)) {
				$opts = [
					CURLOPT_URL			   => $apiUrl.$func,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_POST		   => true,
				];

				$params['time']=(empty($params['time']))?0:$params['time'];
				$str=[
					'api_key'=>$key,
					'webinar_id'=>$webid,
					'first_name'=>$name,
					'email'=>$email,
					'schedule'=>0,
					'ip_address'=>$submission->ip_address
				];

				switch (true) {
					case (!empty($params['phonedata'])&&!empty($params['countryData'])):
						// $cd=json_decode( $params['countryData'] );
						// $str['phone_country_code']=$cd->dialCode;
						$str['phone']=str_replace('+', '', $phone);
						break;

					default:
						// $str['phone_country_code']='';
						$str['phone']=$phone;
						break;
				}
				// $str['phone']=$phone;
				// $responce['str']=$str;

				$str=http_build_query($str);
				$opts[CURLOPT_POSTFIELDS]=$str;

				try {
					$ch = curl_init();
					curl_setopt_array($ch, $opts);
					$data=curl_exec($ch);
					curl_close($ch);
					$responce['register']=json_decode($data);

					// die(json_encode( $responce ));

					if (!empty($responce['register'])) {
						$user=heartweb_createUser($userConf);
						$webinarTime=(string)strtotime($responce['register']->user->date);
						$registeredUsers=(!empty($pmeta['_heartweb_registeredUsers'][0]))?maybe_unserialize( $pmeta['_heartweb_registeredUsers'][0] ):null;
						$registeredUsers=(!is_array($registeredUsers))?[$webinarTime=>[]]:$registeredUsers;
						if (empty($user['error'])&&!empty($user['userid'])) {
							$registeredUsers[$webinarTime][]=$user['userid'];
							update_post_meta( $params['postId'], '_heartweb_registeredUsers', $registeredUsers );
						}

						// var_dump($phone);
						// var_dump($smsText);

						if (!empty($smsText)&&!empty($phone)) {
							$smsText=strtr($smsText, $smsRep);
							$responce['sms']=heartweb_sendSms(['phone'=>$phone, 'text'=>$smsText, 'time'=>$curTime, 'type'=>$params['webinarType']]);
						}
					}
				} catch (\Exception $e) {
					$responce['error']=$e;
				}
			}

	}

	// $responce['submission']=$submission;
	// $responce['form']=(!empty($form))?$form:null;
	// $responce['form_meta']=get_post_meta($form->ID, '', false);
	// $responce['hf_settings']=get_post_meta($form->ID, '', false);
	$responce['redirect_url']=(!empty($form))?$form->settings['redirect_url']:null;

	// write logdata
	// $responce['result']='OK';
	$date=date('d-m-Y--H-i-s', $curTime);
	// $responce['curtime']=time();
	$filename=__DIR__.'/../messages/site-'.$date.'.txt';
	$logdata=json_encode($responce);
	$fp = fopen($filename, 'w');
	fwrite($fp, $logdata);
	fclose($fp);

	return wp_send_json($responce);
}
