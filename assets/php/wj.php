<?php
require './vendor/autoload.php';
require 'config.php';
// Using Medoo namespace
use Medoo\Medoo;
$key='305f56f0eaa1d5214a02940fc3e4b7f63f9fc24a94b1cb716e0299e5271fa5ba';
if (!empty($_POST)) {
	if (!empty($_POST['reqFields'])) {
		$req=explode(',', $_POST['reqFields']);
		$t='';
		$n=[];
		foreach ($req as $val) {
			if (empty($_POST[$val])||($val=='time'&&$_POST[$val]=='Сперва выберите дату')||($val=='date'&&$_POST[$val]=='Выберите дату')) {
				$t.='<li>'.$val.'</li>';
				$n[]=$val;
			}
		}
		if(!empty($t)){
			$json['error'] = 1;
			$json['status'] = 'error';
			$trans=array(
				'name' => 'имя',
				'email' => 'почта',
				'date' => 'дата',
				'phone' => 'телефон',
				'time' => 'время'
			);
			$t=strtr($t, $trans);
			$json['names']=$n;
			$json['msg'] = '<p>Мы не можем обработать Вашу регистрацию. Вам необходимо указать:</p> <ul>' . $t . '</ul>';
			header('Content-type: application/json');
			exit(json_encode($json));
		}
	}

	$responce['status']='ok';
	$params=$_POST;
	$responce['params']=$params;
	// die(json_encode( $params['f'] ));
	$url="https://webinarjam.genndi.com/api/".$params['f'];
	// Initialize
	$database = new Medoo($dbconf);
	switch ($params['f']) {
		case 'webinar':
			$opts = array(
				CURLOPT_URL			   => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST		   => true,
			);
			$ch = curl_init();
			curl_setopt_array($ch, $opts);
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'api_key='.$key.'&webinar_id='.$params['w']);
			$data=curl_exec($ch);
			curl_close($ch);
			$responce['getWebinar']=json_decode($data);

			$scheds=$responce['getWebinar']->webinar->schedules;
			$responce['prevsched']=strtotime ($scheds[count($scheds)-2]->date);
			$responce['registrations']=$database->count('sent_sms',[
				'time[>]'=>$responce['prevsched'],
				'status'=>'succ',
				'wtitle'=>$responce['getWebinar']->webinar->name
			]);
			break;

		case 'register':
			$tbl='sent_sms';
			$ind=($database->max($tbl, 'id')>0) ? $database->max($tbl, 'id')+1 : 0;

			$params['time']=($params['time']=='first')?0:$params['time'];
			// $str='api_key='.$key.'&webinar_id='.$params['w'].'&first_name='.$params['name'].'&email='.$params['email'].'&schedule='.$params['time'];
			// $str.=(!empty($params['phone']))?'&phone='.$params['phone']:null;
			$str=array(
				'api_key'=>$key,
				'webinar_id'=>$params['w'],
				'first_name'=>$params['name'],
				'email'=>$params['email'],
				'schedule'=>$params['time']
			);

			// $phonestr=(!empty($params['phone'])&&mb_strlen($params['phone'])>9)?preg_replace("/[^A-Za-z0-9+]/", '', $params['phone']):$params['phone'];
			// if (!empty($phonestr)&&strlen($phonestr)>9) {
			// 	$str['phone_country_code']='380';
			// 	$str['phone']=(!empty($params['phone'])&&mb_strlen( $params['phone'])>9)?mb_substr($params['phone'], 4):$params['phone'];
			// }


			// if (!empty($params['phone'])) {
			// 	$countryData=json_decode($params['countryData']);
			// 	// $str['phone_country_code']=$countryData->dialCode;
			// 	$valphone=(mb_strpos( $params['phone'], '0' )===0)?mb_substr($params['phone'], 1):$params['phone'];
			// 	$str['phone']=($countryData->iso2=='ua')?$valphone:$params['phone'];
			// 	$str['phone']=$countryData->dialCode.$str['phone'];
			// }


			$phone=(!empty($_POST['phonedata']))?$_POST['phonedata']:$_POST['phone'];
			$countryData=json_decode($params['countryData']);
			// if (strpos( $phone, '+' )===false) {
			if (empty($_POST['phonedata'])) {
				$phone=(mb_strpos( $params['phone'], '0' )===0&&$countryData->iso2=='ua')?mb_substr($phone, 1):$phone;
				$phone=(!empty($countryData))?$countryData->dialCode.$phone:'+380';
			}
			$str['phone']=$phone;

			$str=http_build_query($str);
			$opts = array(
				CURLOPT_URL			   => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST		   => true,
				CURLOPT_POSTFIELDS	   => $str,
			);
			// $responce['opts']=$opts;

			// send sms
			if (!empty($params['phone'])) {
				try {
					// Подключаемся к серверу
					$client = new SoapClient('http://turbosms.in.ua/api/wsdl.html');
					// Данные авторизации
					$auth = [
						'login' => 'E2uAUiB',
						'password' => 'FyfWyQjFeWjFDT4rKgBQCUBybws2T4'
					];
					// Авторизируемся на сервере
					$result = $client->Auth($auth);
					// Результат авторизации
					$responce['turboSMS']['auth']= $result->AuthResult . PHP_EOL;
					// Получаем количество доступных кредитов
					$responce['turboSMS']['balance'] = $client->GetCreditBalance();
					// Текст сообщения ОБЯЗАТЕЛЬНО отправлять в кодировке UTF-8
					// $text = iconv('windows-1251', 'utf-8', 'Благодарим за регистрацию на вебинар! Ожидайте данные о времени и ссылку на вебинар на email.');
					$text = 'Благодарим за регистрацию на вебинар! Ожидайте данные о времени и ссылку на вебинар на email.';
					// $text = 'Мастер-класс “3 способа убрать страх видеокамеры и сцены” начнётся в 19:00, ссылка на трансляцию: http://bit.ly/2BIxgTJ . Если Вы не успеваете, вы можете посмотреть с телефона.';
					// Отправляем сообщение на один номер.
					// Подпись отправителя может содержать английские буквы и цифры. Максимальная длина - 11 символов.
					// Номер указывается в полном формате, включая плюс и код страны
					// $smsPhone='+'.$countryData->dialCode.(($countryData->iso2=='ua')?mb_substr($params['phone'], 1):$params['phone']);
					// $smsPhone=(!empty($_POST['phonedata']))?$_POST['phonedata']:'+'.$countryData->dialCode.(($countryData->iso2=='ua')?mb_substr($params['phone'], 1):$params['phone']);
					$smsPhone=$phone;
					$sms = [
						'sender' => 'Hearbeat',
						'destination' => $smsPhone,
						'text' => $text
					];
					$timediff=(file_exists(__DIR__.'/../../sms/'.$smsPhone.'.txt'))?time()-file_get_contents(__DIR__.'/../../sms/'.$smsPhone.'.txt'):false;
					if (!file_exists(__DIR__.'/../../sms/'.$smsPhone.'.txt')||$timediff>10800) {
						$type='wj';
						$ttl=$params['webinarName'];
						// $tsms=json_decode( $responce['turboSMS'] );
						$responce['turboSMS']['sendResult'] = $client->SendSMS($sms);

						// Запрашиваем статус конкретного сообщения по ID
						// $sms = ['MessageId' => 'c9482a41-27d1-44f8-bd5c-d34104ca5ba9'];
						// $status = $client->GetMessageStatus($sms);
						// echo $status->GetMessageStatusResult . PHP_EOL;

						$fsms = fopen(__DIR__.'/../../sms/responce-wj-'.$smsPhone.'.txt', 'w');
						fwrite($fsms, json_encode($responce['turboSMS']));
						fclose($fsms);

						try {
							$tsms=var_export($responce['turboSMS'], true);
							if (!empty($responce['turboSMS']['sendResult'])&&$responce['turboSMS']['sendResult']->SendSMSResult->ResultArray[0]==='Сообщения успешно отправлены') {
								$database->insert($tbl, [
									'id'=>$ind,
									'type'=>$type,
									'name'=>$params['name'],
									'phone'=>$smsPhone,
									'email'=>$params['email'],
									'status'=>'succ',
									'time'=>time(),
									'result'=>'Success! Context: '.$tsms,
									'wtitle'=>$ttl,
								]);
								$fsms = fopen(__DIR__.'/../../sms/'.$smsPhone.'.txt', 'w');
								fwrite($fsms, time());
								fclose($fsms);
							} else {
								$database->insert($tbl, [
									'id'=>$ind,
									'type'=>$type,
									'name'=>$params['name'],
									'phone'=>$smsPhone,
									'email'=>$params['email'],
									'status'=>'err',
									'time'=>time(),
									'result'=>'Error! Context: '.$tsms,
									'wtitle'=>$ttl,
								]);
								$fsms = fopen(__DIR__.'/../../sms/'.$smsPhone.'.txt', 'w');
								fwrite($fsms, json_encode($responce['turboSMS']['sendResult']));
								fclose($fsms);
							}
						} catch (Exception $e) {
							$tsms=var_export($e, true);
							$database->insert($tbl, [
								'id'=>$ind,
								'type'=>$type,
								'name'=>$params['name'],
								'phone'=>$smsPhone,
								'email'=>$params['email'],
								'status'=>'succ',
								'time'=>time(),
								'result'=>'Error! Context: '.$tsms,
								'wtitle'=>$ttl,
							]);
							$fsms = fopen(__DIR__.'/../../sms/Error-'.$smsPhone.'.txt', 'w');
							fwrite($fsms, $e);
							fclose($fsms);
						}
					}
				} catch(Exception $e) {
					$tsms=var_export($e, true);
					$responce['turboSMS']['error'] = 'Ошибка: ' . $e->getMessage() . PHP_EOL;
					$database->insert($tbl, [
						'id'=>$ind,
						'type'=>$type,
						'phone'=>$smsPhone,
						'name'=>$params['name'],
						'email'=>$params['email'],
						'time'=>time(),
						'result'=>'Error! Context: '.$tsms,
						'wtitle'=>$ttl,
						'status'=>'err'
					]);
					$fsms = fopen(__DIR__.'/../../sms/Error-'.time().'.txt', 'w');
					fwrite($fsms, $responce['turboSMS']['error']);
					fclose($fsms);
				}
			}

			$ch = curl_init();
			curl_setopt_array($ch, $opts);
			// curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
			$data=curl_exec($ch);
			curl_close($ch);
			$responce['register']=$data;

			// write logdata
			// $responce['result']='OK';
			$responce['date']=date('d-m-Y--H-i-s', time());
			// $responce['curtime']=time();
			$responce['filename']=__DIR__.'/../../messages/site-'.$responce['date'].'.txt';
			$logdata=json_encode($responce);
			$fp = fopen($responce['filename'], 'w');
			fwrite($fp, $logdata);
			fclose($fp);

			// setcookie('successReg', json_encode($responce), time()+60*60*100, '/', '.heartbeat.education');
			break;

		default:
			$opts = array(
				CURLOPT_URL			   => "https://webinarjam.genndi.com/api/webinars",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_POST		   => true,
			);
			$ch = curl_init();
			curl_setopt_array($ch, $opts);
			curl_setopt($ch, CURLOPT_POSTFIELDS, 'api_key='.$key);
			$data=curl_exec($ch);
			curl_close($ch);
			$responce['getWebinars']=$data;
			break;
	}
	// if (empty($responce['getWebinars'])) {
	// 	$opts = array(
	// 		CURLOPT_URL			   => "https://webinarjam.genndi.com/api/webinars",
	// 		CURLOPT_RETURNTRANSFER => true,
	// 		CURLOPT_POST		   => true,
	// 	);
	// 	$ch = curl_init();
	// 	curl_setopt_array($ch, $opts);
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, 'api_key='.$key);
	// 	$data=curl_exec($ch);
	// 	curl_close($ch);
	// 	$responce['getWebinars']=$data;
	// }
	if ($params['f']=='register'&&empty($responce['getWebinar'])) {
		$opts = array(
			CURLOPT_URL			   => "https://webinarjam.genndi.com/api/webinar",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST		   => true,
		);
		$ch = curl_init();
		curl_setopt_array($ch, $opts);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'api_key='.$key.'&webinar_id='.$params['w']);
		$data=curl_exec($ch);
		curl_close($ch);
		$responce['getWebinar']=$data;
	}

	unset($responce['params']);
	unset($responce['opts']);
	unset($responce['filename']);
	unset($responce['date']);
	unset($responce['turboSMS']);
	die(json_encode($responce));
}
