<?php
register_post_type('author', // Register Custom Post Type
	array(
	'labels' => array(
		'name' => __('Authors', 'heartweb'), // Rename these to suit
		'singular_name' => __('Author', 'heartweb'),
		'add_new' => __('Add', 'heartweb'),
		'add_new_item' => __('Add element', 'heartweb'),
		'edit' => __('Edit', 'heartweb'),
		'edit_item' => __('Edit element', 'heartweb'),
		'new_item' => __('New element', 'heartweb'),
		'view' => __('Review', 'heartweb'),
		'view_item' => __('Review element', 'heartweb'),
		'search_items' => __('Search', 'heartweb'),
		'not_found' => __('Nothing found', 'heartweb'),
		'not_found_in_trash' => __('Nothing found in trash', 'heartweb')
	),
	'public' => true,
	'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
	'has_archive' => false,
	'supports' => array(
		'title',
		'editor',
		'thumbnail'
	), // Go to Dashboard Custom HTML5 Blank post for supports
	'can_export' => true, // Allows export in Tools > Export
	'menu_icon'		   => 'dashicons-welcome-learn-more',
	'menu_position'	   => 5,
	'publicly_queryable'  => false,
	// 'taxonomies' => array(
	//	 // 'post_tag',
	//	 // 'category'
	// ) // Add Category and Post Tags support
));
register_post_type('reviews', // Register Custom Post Type
	array(
	'labels' => array(
		'name' => __('Reviews', 'heartweb'), // Rename these to suit
		'singular_name' => __('Review', 'heartweb'),
		'add_new' => __('Add', 'heartweb'),
		'add_new_item' => __('Add element', 'heartweb'),
		'edit' => __('Edit', 'heartweb'),
		'edit_item' => __('Edit element', 'heartweb'),
		'new_item' => __('New element', 'heartweb'),
		'view' => __('Review', 'heartweb'),
		'view_item' => __('Review element', 'heartweb'),
		'search_items' => __('Search', 'heartweb'),
		'not_found' => __('Nothing found', 'heartweb'),
		'not_found_in_trash' => __('Nothing found in trash', 'heartweb')
	),
	'public' => true,
	'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
	'has_archive' => false,
	'supports' => array(
		'title',
		'editor',
		'thumbnail'
	), // Go to Dashboard Custom HTML5 Blank post for supports
	'can_export' => true, // Allows export in Tools > Export
	'menu_icon'		   => 'dashicons-welcome-learn-more',
	'menu_position'	   => 5,
	'publicly_queryable'  => false,
	// 'taxonomies' => array(
	//	 // 'post_tag',
	//	 // 'category'
	// ) // Add Category and Post Tags support
));
register_post_type('pricing', // Register Custom Post Type
	array(
	'labels' => array(
		'name' => __('Pricing blocks', 'heartweb'), // Rename these to suit
		'singular_name' => __('Pricing block', 'heartweb'),
		'add_new' => __('Add', 'heartweb'),
		'add_new_item' => __('Add element', 'heartweb'),
		'edit' => __('Edit', 'heartweb'),
		'edit_item' => __('Edit element', 'heartweb'),
		'new_item' => __('New element', 'heartweb'),
		'view' => __('Review', 'heartweb'),
		'view_item' => __('Review element', 'heartweb'),
		'search_items' => __('Search', 'heartweb'),
		'not_found' => __('Nothing found', 'heartweb'),
		'not_found_in_trash' => __('Nothing found in trash', 'heartweb')
	),
	'public' => true,
	'hierarchical' => false, // Allows your posts to behave like Hierarchy Pages
	'has_archive' => false,
	'supports' => array(
		'title',
		'editor'
	), // Go to Dashboard Custom HTML5 Blank post for supports
	'can_export' => true, // Allows export in Tools > Export
	'menu_icon'		   => 'dashicons-products',
	'menu_position'	   => 5,
	'publicly_queryable'  => false,
	// 'taxonomies' => array(
	//	 // 'post_tag',
	//	 // 'category'
	// ) // Add Category and Post Tags support
));
