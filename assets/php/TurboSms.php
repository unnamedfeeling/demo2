<?php

/**
 * Copyright (C) 2016 webmak@ukr.net.
 * 
 * The MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * 
 * @author    webmak <webmak@ukr.net>
 * @copyright 2016 webmak@ukr.net.
 * @license   <https://opensource.org/licenses/MIT>.
 * @link http://webmak.com.ua
 * @link https://github.com/webmak
 */
class TurboSms
{

    protected $soap_client;
    protected $success_auth = false;
    protected $sms_time_limit = 10800; //3*60*60 три часа

    public function __construct()
    {
        $auth = array(
            'login' => 'enigmator',
            'password' => 'mppDhl45644sdfsdfLhh778sdfsdf'
        );
        $this->soap_client = new SoapClient('http://turbosms.in.ua/api/wsdl.html');
        $result = $this->soap_client->Auth($auth);
        $this->success_auth = ($result->AuthResult === 'Вы успешно авторизировались');
    }

    public function sendSms($phone_number, $msg)
    {
        if (!$this->success_auth) {
            return false;
        }
        $sms = array(
            'sender' => 'Enigma',
            'destination' => $phone_number,
            'text' => $msg
        );
        $result = $this->soap_client->SendSMS($sms);
        $answer = $result->SendSMSResult->ResultArray[0];
        if (!is_array($result->SendSMSResult->ResultArray)) {
            $answer = $result->SendSMSResult->ResultArray;
        }
        if ($answer === 'Сообщения успешно отправлены') {
            return true;
        }
        return false;
    }

    public function addHash($string)
    {
        $data = array(
            'hash' => Tools::encrypt($string),
            'date_upd' => date("Y-m-d H:i:s")
        );
        return Db::getInstance()->insert('turbosms', $data, false, false, Db::ON_DUPLICATE_KEY);
    }

    public function getHashFromDb($string)
    {
        return Db::getInstance()->getRow('SELECT * FROM ' . _DB_PREFIX_ . 'turbosms WHERE hash = \'' . pSQL(Tools::encrypt($string)) . '\'');
    }

}
