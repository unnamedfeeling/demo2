<?php

// Register HTML5 Blank Navigation
function register_generic_menu(){
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Главное меню', 'heartweb'), // Main Navigation
        'footer-menu1' => __('Меню футера 1', 'heartweb'), // Sidebar Navigation
        'footer-menu2' => __('Меню футера 2', 'heartweb'), // Sidebar Navigation
        // 'extra-menu' => __('Extra Menu', 'heartweb') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// HTML5 Blank navigation
function generic_nav(){
    wp_nav_menu(
    array(
        'theme_location'  => 'header-menu',
        'menu'            => '',
        'container'       => 'div',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<ul class="headmidmenu">%3$s</ul>',
        'depth'           => 0,
        'walker'          => new Nav_Menu_Walker
        )
    );
}

// Menu walker
class Nav_Menu_Walker extends Walker_Nav_Menu{
	/**
	 * Start the element output.
	 *
	 * @param  string $output Passed by reference. Used to append additional content.
	 * @param  object $item   Menu item data object.
	 * @param  int $depth     Depth of menu item. May be used for padding.
	 * @param  array $args    Additional strings.
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
		// print_r($item);

		$class=($depth>=1) ? 'headmenubot_item' : 'headmidmenu_item';
		$class.=($depth==0&&$item->current&&!in_array('headmidmenu_item-logo', $item->classes)) ? ' headmidmenu_link-active':'';
		// $class.=($depth>=1 ? 'header__bottom-sub-menu-link ' : 'header__bottom-menu-link ');
		$class.=($depth>=1&&$item->current ? ' headmidmenu_link-active ':'');
		$class.=' d-'.$depth;
		$addclass=' ';
		foreach ($item->classes as $key => $val) {
			$addclass.=' '.$val;
		}
		$class.=$addclass;
		$output.= '<li class="'.$class.'">';
		$jsbtn=(in_array('headmidmenu_item-logo', $item->classes)) ? ' headmidmenu_link-logo' : '';
		// $jsbtn='';
		// print_r($item->classes);
		$attributes  = ($depth>=1) ? ' class="headmenubot_link"' : ' class="headmidmenu_link'.$jsbtn.'"';
		if( !empty ( $item->attr_title ) && $item->attr_title !== $item->title ){
			// Avoid redundant titles
			$attributes .= ' title="' . esc_attr( $item->attr_title ) .'"';
		} else {
			$title       = apply_filters( 'the_title', $item->title, $item->ID );
			$attributes .= ' title="' . $title .'"';
		}
		! empty ( $item->url )
			and $attributes .= ' href="' . esc_attr( $item->url ) .'"';
		$attributes  = trim( $attributes );
		$title       = apply_filters( 'the_title', $item->title, $item->ID );
		$item_output = "$args->before<a $attributes>$args->link_before$title</a>"
						. "$args->link_after$args->after";
		// Since $output is called by reference we don't need to return anything.
		$output .= apply_filters('walker_nav_menu_start_el',   $item_output,   $item,   $depth,   $args);
	}
	/**
	 * @see Walker::start_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ){
		if($depth>=0){
			$output .= '<ul class="headmenubot">';
		} else {
			$output .= '<ul class="headmidmenu">';
		}
	}
	/**
	 * @see Walker::end_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ){
		// $output .= '</ul>';
		// if($depth>=0){
			$output .= '</ul>';
		// } else {
		// 	$output .= '</li>';
		// }
	}
	/**
	 * @see Walker::end_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ){
		$output .= '</li>';
	}
}

function generic_footer1_nav(){
    wp_nav_menu(
    array(
        'theme_location'  => 'footer-menu1',
        'menu'            => '',
        'container'       => 'nav',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<p class="footer_text">Компания</p><ul class="footer_list">%3$s</ul>',
        'depth'           => 0,
		'walker'          => new Nav_Footer1_Menu_Walker
        )
    );
}

// Menu walker
class Nav_Footer1_Menu_Walker extends Walker_Nav_Menu{
	/**
	 * Start the element output.
	 *
	 * @param  string $output Passed by reference. Used to append additional content.
	 * @param  object $item   Menu item data object.
	 * @param  int $depth     Depth of menu item. May be used for padding.
	 * @param  array $args    Additional strings.
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
		// print_r($item);
		// $jsbtn=(in_array('menu-item-has-children', $item->classes)) ? ' js-footer-submenu' : '';
		$class='footer_item';
		$class.=($depth==0&&$item->current) ? ' active':'';
		$class.=($depth>=1 ? 'footer-main__link ':'');
		$class.=($depth>=1&&$item->current ? 'active ':'');
		$class.=' d-'.$depth;
		$output.= '<li class="'.$class.'">';
		$attributes  = ($depth>=1) ? '' : ' class="footer_link"';
		// $attributes  = '';
		if( !empty ( $item->attr_title ) && $item->attr_title !== $item->title ){
			// Avoid redundant titles
			$attributes .= ' title="' . esc_attr( $item->attr_title ) .'"';
		} else {
			$title       = apply_filters( 'the_title', $item->title, $item->ID );
			$attributes .= ' title="' . $title .'"';
		}
		! empty ( $item->url )
			and $attributes .= ' href="' . esc_attr( $item->url ) .'"';
		$attributes  = trim( $attributes );
		$title       = apply_filters( 'the_title', $item->title, $item->ID );
		$item_output = "$args->before<a $attributes>$args->link_before$title</a>"
						. "$args->link_after$args->after";
		// Since $output is called by reference we don't need to return anything.
		$output .= apply_filters('walker_nav_menu_start_el',   $item_output,   $item,   $depth,   $args);
	}
	/**
	 * @see Walker::start_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ){
		if($depth>=0){
			$output .= '<ul class="footer-main-sublist">';
		} else {
			$output .= '<ul class="footer-main-list clearfix">';
		}
	}
	/**
	 * @see Walker::end_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ){
		$output .= '</ul>';
	}
	/**
	 * @see Walker::end_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ){
		$output .= '</li>';
	}
}

function generic_footer2_nav(){
    wp_nav_menu(
    array(
        'theme_location'  => 'footer-menu2',
        'menu'            => '',
        'container'       => 'div',
        'container_class' => 'menu-{menu slug}-container',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'items_wrap'      => '<p class="footer_text">Информация</p><ul class="footer_list">%3$s</ul>',
        'depth'           => 0,
		'walker'          => new Nav_Footer2_Menu_Walker
        )
    );
}

// Menu walker
class Nav_Footer2_Menu_Walker extends Walker_Nav_Menu{
	/**
	 * Start the element output.
	 *
	 * @param  string $output Passed by reference. Used to append additional content.
	 * @param  object $item   Menu item data object.
	 * @param  int $depth     Depth of menu item. May be used for padding.
	 * @param  array $args    Additional strings.
	 * @return void
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
		// print_r($item);
		// $jsbtn=(in_array('menu-item-has-children', $item->classes)) ? ' js-footer-submenu' : '';
		$class='footer_item';
		$class.=($depth==0&&$item->current) ? ' active':'';
		$class.=($depth>=1 ? 'footer-main__link ':'');
		$class.=($depth>=1&&$item->current ? 'active ':'');
		$class.=' d-'.$depth;
		$output.= '<li class="'.$class.'">';
		$attributes  = ($depth>=1) ? '' : ' class="footer_link"';
		// $attributes  = '';
		if( !empty ( $item->attr_title ) && $item->attr_title !== $item->title ){
			// Avoid redundant titles
			$attributes .= ' title="' . esc_attr( $item->attr_title ) .'"';
		} else {
			$title       = apply_filters( 'the_title', $item->title, $item->ID );
			$attributes .= ' title="' . $title .'"';
		}
		! empty ( $item->url )
			and $attributes .= ' href="' . esc_attr( $item->url ) .'"';
		$attributes  = trim( $attributes );
		$title       = apply_filters( 'the_title', $item->title, $item->ID );
		$item_output = "$args->before<a $attributes>$args->link_before$title</a>"
						. "$args->link_after$args->after";
		// Since $output is called by reference we don't need to return anything.
		$output .= apply_filters('walker_nav_menu_start_el',   $item_output,   $item,   $depth,   $args);
	}
	/**
	 * @see Walker::start_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ){
		if($depth>=0){
			$output .= '<ul class="footer-main-sublist">';
		} else {
			$output .= '<ul class="footer-main-list clearfix">';
		}
	}
	/**
	 * @see Walker::end_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ){
		$output .= '</ul>';
	}
	/**
	 * @see Walker::end_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return void
	 */
	function end_el( &$output, $item, $depth = 0, $args = array() ){
		$output .= '</li>';
	}
}