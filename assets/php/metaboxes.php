<?php
function generic_metaboxes() {
	// Start with an underscore to hide fields from custom fields list
	global $options;
	$p = $options['prfx'];


	/**
	 * Initiate the metabox
	 */
	// if(isset($_GET['post'])){
	// 	$post_id = $_GET['post'];
	// } elseif(isset($_POST['post_ID'])){
	// 	$post_id = $_POST['post_ID'];
	// } else {
	// 	$post_id='';
	// }
	// $mpost=get_post($post_id);
	// print_r($post_id);
	//$post_id = get_the_ID() ;
	// $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
	// $pt=get_post_type($post_id);
	// $pt=get_current_screen()->post_type;
	// check for a template type
	// $cmb_webinar = new_cmb2_box( array(
	// 	'id'			=> 'modules_metabox',
	// 	'title'		 => __( 'Условия вывода блоков', 'cmb2' ),
	// 	'object_types'  => array( 'page', ), // Post type
	// 	'show_on'	   => array( 'key' => 'page-template', 'value' => 'tpl-webinar.php' ),
	// 	'context'	   => 'normal',
	// 	'priority'	  => 'high',
	// 	'show_names'	=> true, // Show field names on the left
	// 	// 'cmb_styles' => false, // false to disable the CMB stylesheet
	// 	'closed'	 => true, // Keep the metabox closed by default
	// ) );
	// $cmb_webinar->add_field( array(
	// 	'name' => __( 'Спрятать блок "Главный слайдер"?', 'heartwebinars' ),
	// 	'id'   => $p.'mod_slider',
	// 	'type' => 'checkbox'
	// ) );

	// $cmb_webinar = new_cmb2_box( array(
	// 	'id'            => 'webinar_metabox',
	// 	'title'         => __( 'Данные для этой страницы', 'cmb2' ),
	// 	'object_types'  => array( 'page', ), // Post type
	// 	'show_on'       => array( 'key' => 'page-template', 'value' => ['tpl-webinar.php','tpl-webinarThanks.php'] ),
	// 	'context'       => 'normal',
	// 	'priority'      => 'high',
	// 	'show_names'    => true, // Show field names on the left
	// 	// 'cmb_styles' => false, // false to disable the CMB stylesheet
	// 	// 'closed'     => true, // Keep the metabox closed by default
	// ) );
	// $cmb_webinar->add_field( array(
	// 	'name' => __( 'Использовать широкий блок для контента?', 'heartweb' ),
	// 	'id'   => $p.'widecont',
	// 	'type' => 'checkbox'
	// ) );
	// $cmb_webinar->add_field( array(
	// 	'name' => __( 'ID вебинара в webinarjam', 'heartweb' ),
	// 	'desc' => 'Обязательно! Необходимо заполнить, иначе регистрации работать не будут.',
	// 	'id'   => $p.'webid',
	// 	'type' => 'text'
	// ) );
	// $cmb_webinar->add_field( array(
	// 	'name' => __( 'Выберите автора для данного вебинара', 'heartweb' ),
	// 	'id'   => $p.'webauthor',
	// 	'show_option_none' => true,
	// 	'type' => 'select',
	// 	'options_cb' => 'generic_authors_dropdown_cb'
	// ) );
	// $cmb_webinar->add_field( array(
	// 	'name'      	=> __( 'Attached reviews', 'heartweb' ),
	// 	'id'        	=> $p.'attached_reviews',
	// 	'type'      	=> 'post_search_ajax',
	// 	'desc'			=> __( '(Start typing post title)', 'heartweb' ),
	// 	// Optional :
	// 	'limit'      	=> 10, 		// Limit selection to X items only (default 1)
	// 	'sortable' 	 	=> true, 	// Allow selected items to be sortable (default false)
	// 	'query_args'	=> array(
	// 		'post_type'			=> array( 'reviews' ),
	// 		'post_status'		=> array( 'publish' ),
	// 		'posts_per_page'	=> -1
	// 	)
	// ) );
	// $cmb_webinar->add_field(array(
	// 	'name' => __('Особый JS', 'heartweb'),
	// 	'desc' => __('Пиксели, метрики и другие важные js-вещи. Будет выводиться где надо и как надо.<br><b>Теги <code>&lt;script&gt; и &lt;/script&gt;</code> опускаем - сюда пишем ТОЛЬКО ЧИСТЫЙ JS</b>', 'heartweb'),
	// 	'id' => $p.'custom_js',
	// 	'default' => ' ',
	// 	'type' => 'textarea',
	// ));
	// $cmb_webinar->add_field(array(
	// 	'name' => __('Особый JS, который нужно выводить с тегом "script"', ' generic'),
	// 	'desc' => __('Пиксели, метрики и другие важные js-вещи. Будет выводиться где надо и как надо.<br><b>Теги <code>&lt;script&gt; и &lt;/script&gt;</code> указываем тут - это нужно для тех случаев, когда нужно задать код, который подключает сторонний файл напрямую</b>. НУЖНО ДОБАВЛЯТЬ АТРИБУТ <b>async="async"</b> К ТАКОМУ КОДУ ОБЯЗАТЕЛЬНО', ' generic'),
	// 	'id' => $p.'custom_js_tag',
	// 	'default' => '',
	// 	'type' => 'textarea',
	// 	// 'sanitization_cb' => 'unfilter_all',
	// 	// 'escape_cb' => 'unescape_all'
	// ));
	// $cmb_webinar->add_field(array(
	// 	'name' => __('Особый noscript', 'heartweb'),
	// 	'desc' => __('Если к коду идет дополнение в тегах noscript.<br><b>Теги <code>&lt;noscript&gt; и &lt;/noscript&gt;</code> опускаем</b>', 'heartweb'),
	// 	'id' => $p.'custom_noscript',
	// 	'default' => ' ',
	// 	'type' => 'textarea',
	// ));
	//
	$cmb_author = new_cmb2_box( array(
		'id'            => 'author_metabox',
		'title'         => __( 'Данные для этой страницы', 'cmb2' ),
		'object_types'  => array( 'author', ), // Post type
		// 'show_on'       => array( 'key' => 'page-template', 'value' => 'tpl-webinar.php' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	$cmb_author->add_field( array(
		'name' => __( 'Регалии автора', 'heartweb' ),
		'id'   => $p.'authorInfo',
		'type' => 'text'
	) );
	$cmb_author->add_field( array(
		'name' => __( 'Позиция автора', 'heartweb' ),
		'id'   => $p.'authorPos',
		'type' => 'text'
	) );
	$cmb_author->add_field( array(
		'name' => __( 'Видеообращение автора (тип видео)', 'heartweb' ),
		'id'   => $p.'authorVidType',
		'type' => 'select',
		'show_option_none' => false,
		'default'          => 'youtube',
		'options'          => [
			'youtube' => __( 'Youtube', 'cmb2' ),
			'wistia'  => __( 'Wistia', 'cmb2' ),
		],
	) );
	$cmb_author->add_field( [
		'name' => __( 'Видеообращение автора', 'heartweb' ),
		'id'   => $p.'authorVidId',
		'type' => 'text'
	] );
	$cmb_author->add_field( array(
		'name' => __( 'Дополнительные умения (регалии) автора', 'heartweb' ),
		'id'   => $p.'authorEls',
		'type' => 'text',
		'repeatable' => true
	) );

	$cmb_reviews = new_cmb2_box( array(
		'id'            => 'reviews_metabox',
		'title'         => __( 'Данные для этой страницы', 'cmb2' ),
		'object_types'  => array( 'reviews', ), // Post type
		// 'show_on'       => array( 'key' => 'page-template', 'value' => 'tpl-webinar.php' ),
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	$cmb_reviews->add_field( array(
		'name' => __( 'Video type', 'heartweb' ),
		'id'   => $p.'vidType',
		'type' => 'select',
		'show_option_none' => false,
		'default'          => 'youtube',
		'options'          => [
			'youtube' => __( 'Youtube', 'cmb2' ),
			'wistia'  => __( 'Wistia', 'cmb2' ),
		],
	) );
	$cmb_reviews->add_field( array(
		'name' => __( 'Video ID', 'heartweb' ),
		'id'   => $p.'ytvid',
		'type' => 'text'
	) );
	$cmb_reviews->add_field( array(
		'name' => __( 'Reviewer name', 'heartweb' ),
		'id'   => $p.'rname',
		'type' => 'text'
	) );
	$cmb_reviews->add_field( array(
		'name' => __( 'Reviewer position', 'heartweb' ),
		'id'   => $p.'rpos',
		'type' => 'text'
	) );
}


//
// function generic_header_fields( $fields ) {
// 	global $options;
// 	$p = $options['prfx'];
// 	$new_fields = array(
// 		array(
// 			'name'    => esc_html__( 'Block Title', 'heartwebinars' ),
// 			'id'      => $p.'header_title',
// 			'type'    => 'wysiwyg',
// 			'options' => array( 'textarea_rows' => 5, ),
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block background', 'heartwebinars' ),
// 			'id'      => $p.'header_bg',
// 			'type' => 'file',
// 			'query_args' => array(
// 				'type' => array('image/jpeg')
// 			)
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block Background parameters', 'heartwebinars' ),
// 			'id'      => $p.'header_bgparams',
// 			'type'    => 'text'
// 		),
// 	);
// 	return array_merge( $fields, $new_fields );
// }
//
// function generic_video_fields( $fields ) {
// 	global $options;
// 	$p = $options['prfx'];
// 	$new_fields = array(
// 		array(
// 			'name'    => esc_html__( 'Block Title', 'heartwebinars' ),
// 			'id'      => $p.'video_blockTtl',
// 			'type'    => 'text',
// 		),
// 		array(
// 			'name'    => esc_html__( 'Wistia video ID', 'heartwebinars' ),
// 			'id'      => $p.'video_vidid',
// 			'type'    => 'text',
// 		),
// 		array(
// 			'name'    => esc_html__( 'Video background', 'heartwebinars' ),
// 			'id'      => $p.'video_vidimg',
// 			'type' => 'file',
// 			'query_args' => array(
// 				'type' => array('image/jpeg')
// 			)
// 		),
// 		array(
// 			'name'    => esc_html__( 'Author name', 'heartwebinars' ),
// 			'id'      => $p.'video_authorName',
// 			'type'    => 'text',
// 		),
// 		array(
// 			'name'    => esc_html__( 'Author avatar', 'heartwebinars' ),
// 			'id'      => $p.'video_authorImg',
// 			'type' => 'file',
// 			'query_args' => array(
// 				'type' => array('image/jpeg')
// 			)
// 		),
// 		array(
// 			'name'    => esc_html__( 'Author information', 'heartwebinars' ),
// 			'id'      => $p.'video_authorInfo',
// 			'type'    => 'text',
// 		),
// 		array(
// 			'name'    => esc_html__( 'Author postition', 'heartwebinars' ),
// 			'id'      => $p.'video_authorPosition',
// 			'type'    => 'text',
// 		),
// 	);
// 	return array_merge( $fields, $new_fields );
// }
//
// function generic_block_cont_fields( $fields ) {
// 	global $options;
// 	$p = $options['prfx'];
// 	$new_fields = array(
// 		array(
// 			'name'    => esc_html__( 'Block Title', 'heartwebinars' ),
// 			'id'      => $p.'block-content_blockTtl',
// 			'type'    => 'text',
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block Content', 'heartwebinars' ),
// 			'id'      => $p.'block-content_blockCont',
// 			'type'    => 'wysiwyg'
// 		),
// 	);
// 	return array_merge( $fields, $new_fields );
// }
//
// function generic_block_cont_wide_fields( $fields ) {
// 	global $options;
// 	$p = $options['prfx'];
// 	$new_fields = array(
// 		array(
// 			'name'    => esc_html__( 'Block Title', 'heartwebinars' ),
// 			'id'      => $p.'block-content-wide_blockTtl',
// 			'type'    => 'text',
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block Content', 'heartwebinars' ),
// 			'id'      => $p.'block-content-wide_blockCont',
// 			'type'    => 'wysiwyg'
// 		),
// 	);
// 	return array_merge( $fields, $new_fields );
// }
//
// function generic_block_cont_fullwidth_fields( $fields ) {
// 	global $options;
// 	$p = $options['prfx'];
// 	$new_fields = array(
// 		array(
// 			'name'    => esc_html__( 'Block Title', 'heartwebinars' ),
// 			'id'      => $p.'block-content-fullwidth_blockTtl',
// 			'type'    => 'text',
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block Content', 'heartwebinars' ),
// 			'id'      => $p.'block-content-fullwidth_blockCont',
// 			'type'    => 'wysiwyg'
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block Background', 'heartwebinars' ),
// 			'id'      => $p.'block-content-fullwidth_bg',
// 			'type' => 'file',
// 			'query_args' => array(
// 				'type' => array('image/jpeg')
// 			)
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block Background parameters', 'heartwebinars' ),
// 			'id'      => $p.'block-content-fullwidth_bgparams',
// 			'type'    => 'text'
// 		),
// 	);
// 	return array_merge( $fields, $new_fields );
// }
//
// function generic_block_list_fields( $fields ) {
// 	global $options;
// 	$p = $options['prfx'];
// 	$new_fields = array(
// 		array(
// 			'name'    => esc_html__( 'Block Title', 'heartwebinars' ),
// 			'id'      => $p.'block-content-list_blockTtl',
// 			'type'    => 'text',
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block Content', 'heartwebinars' ),
// 			'id'      => $p.'block-content-list_blockCont',
// 			'type'    => 'text',
// 			'repeatable'    => true,
// 		),
// 	);
// 	return array_merge( $fields, $new_fields );
// }
//
// function generic_parallax_fields( $fields ) {
// 	global $options;
// 	$p = $options['prfx'];
// 	$new_fields = array(
// 		array(
// 			'name'    => esc_html__( 'Block Title', 'heartwebinars' ),
// 			'id'      => $p.'parallax_blockTtl',
// 			'type'    => 'wysiwyg',
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block Content', 'heartwebinars' ),
// 			'id'      => $p.'parallax_blockCont',
// 			'type'    => 'wysiwyg',
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block Background', 'heartwebinars' ),
// 			'id'      => $p.'parallax_blockBg',
// 			'type' => 'file',
// 			'query_args' => array(
// 				'type' => array('image/jpeg')
// 			)
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block Background parameters', 'heartwebinars' ),
// 			'id'      => $p.'parallax_blockBgparams',
// 			'type'    => 'text'
// 		),
// 	);
// 	return array_merge( $fields, $new_fields );
// }
//
//
// function generic_specialoffer_fields( $fields ) {
// 	global $options;
// 	$p = $options['prfx'];
// 	$new_fields = array(
// 		array(
// 			'name'    => esc_html__( 'Block Title', 'heartwebinars' ),
// 			'id'      => $p.'specialoffer_title',
// 			'type'    => 'wysiwyg',
// 			'options' => array( 'textarea_rows' => 5, ),
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block background', 'heartwebinars' ),
// 			'id'      => $p.'specialoffer_bg',
// 			'type' => 'file',
// 			'query_args' => array(
// 				'type' => array('image/jpeg')
// 			)
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block Background parameters', 'heartwebinars' ),
// 			'id'      => $p.'specialoffer_bgparams',
// 			'type'    => 'text'
// 		),
// 		array(
// 			'name'    => esc_html__( 'Wistia video ID', 'heartwebinars' ),
// 			'id'      => $p.'specialoffer_vidid',
// 			'type'    => 'text',
// 		),
// 		array(
// 			'name'    => esc_html__( 'Video background', 'heartwebinars' ),
// 			'id'      => $p.'specialoffer_vidimg',
// 			'type' => 'file',
// 			'query_args' => array(
// 				'type' => array('image/jpeg')
// 			)
// 		),
// 		array(
// 			'name'    => esc_html__( 'Package Data', 'heartwebinars' ),
// 			'id'      => $p.'specialoffer_pkgData',
// 			'type'    => 'wysiwyg',
// 		),
// 		array(
// 			'name'    => esc_html__( 'Package purchase link', 'heartwebinars' ),
// 			'id'      => $p.'specialoffer_purchLnk',
// 			'type'    => 'text',
// 		),
//
// 	);
// 	return array_merge( $fields, $new_fields );
// }
//
// function generic_author_fullwidth_fields( $fields ) {
// 	global $options;
// 	$p = $options['prfx'];
// 	$new_fields = array(
// 		array(
// 			'name'    => esc_html__( 'Block background', 'heartwebinars' ),
// 			'id'      => $p.'author-fullwidth_bg',
// 			'type' => 'file',
// 			'query_args' => array(
// 				'type' => array('image/jpeg')
// 			)
// 		),
// 		array(
// 			'name'    => esc_html__( 'Block background params', 'heartwebinars' ),
// 			'id'      => $p.'author-fullwidth_bgparams',
// 			'type'    => 'text',
// 		),
// 	);
// 	return array_merge( $fields, $new_fields );
// }
//
// function generic_reviews_fields( $fields ) {
// 	global $options;
// 	$p = $options['prfx'];
// 	$new_fields = array(
// 		// array(
// 		// 	'name'    => esc_html__( 'Attached reviews', 'heartwebinars' ),
// 		// 	'id'      => $p.'reviews_attached',
// 		// 	'type'      	=> 'post_search_ajax',
// 		// 	'desc'			=> __( '(Start typing post title)', 'cmb2' ),
// 		// 	// Optional :
// 		// 	'limit'      	=> 10, 		// Limit selection to X items only (default 1)
// 		// 	'sortable' 	 	=> true, 	// Allow selected items to be sortable (default false)
// 		// 	'query_args'	=> array(
// 		// 		'post_type'			=> array( 'reviews' ),
// 		// 		'post_status'		=> array( 'publish' ),
// 		// 		'posts_per_page'	=> -1
// 		// 	)
// 		// ),
// 		array(
// 			'name'    => esc_html__( 'Block Content', 'heartwebinars' ),
// 			'id'      => $p.'reviews_blockCont',
// 			'type'    => 'wysiwyg',
// 		),
// 	);
// 	return array_merge( $fields, $new_fields );
// }
