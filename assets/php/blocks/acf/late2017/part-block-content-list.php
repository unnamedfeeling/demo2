<?php global $options, $pmeta, $tpl, $p;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section">
	<div class="container">
		<article class="win">
			<?=(!empty($blockTtl))?'<p class="win_h2">'.$blockTtl.'</p>':null?>
			<?php
			if (!empty($blockCont)) {
				$cont='';
				foreach ($blockCont as $val) {
					$cont.=sprintf('<li><p class="win_p">%s</p></li>', $val['element']);
				}
				printf('<ul>%s</ul>', $cont);
			}
			?>
		</article>
	</div>
</section>
