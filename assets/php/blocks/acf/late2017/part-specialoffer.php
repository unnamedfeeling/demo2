<?php global $options, $pmeta, $tpl, $p;
$video['img']=(!empty($video['img']))?$video['img']:$tpl.'/assets/late2017/img/wistia-img.jpg';

switch (true) {
	case (!empty($val[$p.'_bg'])):
		$bg='background:url('.((!empty($val[$p.'_bg']))?$val[$p.'_bg']:$tpl.'/assets/late2017/img/thanks/fon-thn1.jpg');
		$bg.=') '.((!empty($val[$p.'_bgparams']))?$val[$p.'_bgparams']:'center / cover');
		$pkgData=(!empty($val[$p.'_pkgData']))?$val[$p.'_pkgData']:null;
		break;
	case (!empty($val[$p.$val['template_group'].'_bg'])):
		$bg='background:url('.((!empty($val[$p.$val['template_group'].'_bg']))?$val[$p.$val['template_group'].'_bg']:$tpl.'/assets/late2017/img/thanks/fon-thn1.jpg');
		$bg.=') '.((!empty($val[$p.$val['template_group'].'_bgparams']))?$val[$p.$val['template_group'].'_bgparams']:'center / cover');
		$pkgData=(!empty($val[$p.$val['template_group'].'_pkgData']))?$val[$p.$val['template_group'].'_pkgData']:null;
		break;

	default:
		$bg='background:url(//via.placeholder.com/1920x922.png)';
		break;
}

if (!empty($author)) {
	$authorName=$author->post_title;
} else {
	$authorName=(!empty($val[$p.$val['template_group'].'_authorName']))?$val[$p.$val['template_group'].'_authorName']:null;
}
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="topimg fluid thanks newel js-topimg <?=$val['template_group']?>-section" style="<?=$bg?>">
	<div class="topimg_cont">
		<div class="container container-big">
			<?=apply_filters('the_content', $blockTtl)?>
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<div class="video thanks notp">
						<div class="wistia-placeholder js-wistia-placeholder">
							<img src="<?=$video['img']?>" class="img-responsive" alt="<?=$authorName?>">
							<div class="playbtn"></div>
						</div>
						<script>
						window._wq = window._wq || [];
						_wq.push({
						  id: "<?=$video['id']?>",
						  options: {
							videoFoam: true,
							autoPlay: true
						  }
						});
						</script>
						<div class="wistia_responsive_padding js-wistia-video" style="display:none;padding:56.25% 0 0 0;position:relative;">
							<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_chsqjjcjvi" style="height:100%;width:100%">&nbsp;</div></div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-4">
					<div class="package_slide notm">
						<article class="package_el notm fluid">
							<?=$pkgData?>
							<?php if (!empty($val[$p.$val['template_group'].'_purchLnk'])): ?>
								<button class="package_btn js-gotohref" data-href="<?=$val[$p.$val['template_group'].'_purchLnk']?>"><?=(!empty($val[$p.$val['template_group'].'_purchLnkTxt']))?$val[$p.$val['template_group'].'_purchLnkTxt']:'Оплатить участие!'?></button>
							<?php endif; ?>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
