<?php global $options, $pmeta, $tpl, $p;
if (!empty($author)):

	switch (true) {
		case (!empty($val[$p.'_bg'])):
			$bg='background:url('.((!empty($val[$p.'_bg']))?$val[$p.'_bg']:$tpl.'/assets/late2017/img/thanks/fon-s5.jpg');
			$bg.=') '.((!empty($val[$p.'_bgparams']))?$val[$p.'_bgparams']:'center / cover fixed');
			break;
		case (!empty($val[$p.$val['template_group'].'_bg'])):
			$bg='background:url('.((!empty($val[$p.$val['template_group'].'_bg']))?$val[$p.$val['template_group'].'_bg']:$tpl.'/assets/late2017/img/thanks/fon-s5.jpg');
			$bg.=') '.((!empty($val[$p.$val['template_group'].'_bgparams']))?$val[$p.$val['template_group'].'_bgparams']:'center / cover fixed');
			break;

		default:
			$bg='//via.placeholder.com/1920x1024.png';
			break;
	}

	$authorName=$author->post_title;
	$authorImg=(has_post_thumbnail( $author->ID ))?get_the_post_thumbnail_url($author->ID, 'authorImg'):$tpl.'/assets/late2017/img/avart/gladir2.jpg';
	$authorInfo=(!empty($ameta[$p.'authorInfo'][0]))?'<p class="sideauthor_info">'.$ameta[$p.'authorInfo'][0].'</p>':null;
	$authorPos=(!empty($ameta[$p.'authorPos'][0]))?'<p class="author_prof">'.$ameta[$p.'authorPos'][0].'</p>':null;
	$authorEls=(!empty($ameta[$p.'authorEls'][0]))?maybe_unserialize($ameta[$p.'authorEls'][0]):null;
	?>
<section id="<?=$val['template_group'].'-'.$i?>" class="topimg fluid thanks js-topimg <?=$val['template_group']?>-section" style="<?=$bg?>">
	<div class="topimg_cont">
		<div class="container container-big">
			<div class="row">
				<div class="col-xs-12">
					<p class="topb_h4 bold decor">Видео тренинг проводит лично</p>
					<br><br>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-6 col-md-offset-3">
					<div class="win win-gor win-top authorell">
						<div class="sideauthor flex flex-cnt">
							<div class="sideauthor_avat-holder">
								<img src="<?=$authorImg?>" alt="<?=$authorName?>" class="sideauthor_avat">
							</div>
							<div class="sideauthor_about">
								<p class="sideauthor_name"><?=$authorName?></p>
								<p class="sideauthor_info"><?=$authorInfo?></p>
								<hr>
								<p class="sideauthor_pos"><?=$authorPos?></p>
							</div>
						</div>
						<?php if (!empty($authorEls)): ?>
							<div class="row">
								<div class="col-xs-12">
									<ul class="nolist nolp">
										<?php
										foreach ($authorEls as $el):
											if (!empty($el)) {
												printf('<li class="flex flex-lft"><span class="listel"><i class="icon-Feature-Personal"></i></span><p class="win_p tal">%s</p></li>', $el);
											}
										endforeach; ?>
									</ul>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
