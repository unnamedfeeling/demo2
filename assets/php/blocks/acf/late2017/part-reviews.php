<?php global $options, $pmeta, $tpl, $p;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="reviews <?=$val['template_group']?>-section">
	<div class="container-fluid">
		<article class="win">
			<div class="row">
				<div class="col-xs-12">
					<p class="win_h2 decor">
						<i class="icon-user-1 thcol"></i><span>Отзывы</span>
					</p>
				</div>
			</div>
			<?php if (!empty($pmeta[$p.'attached_reviews'][0])): ?>
			<div class="row">
				<?php
				$reviews=maybe_unserialize( $pmeta[$p.'attached_reviews'][0] );
				$args=[
					'post_type'=>'reviews',
					'posts_per_page'=>-1,
					'orderby'=>'post__in',
					'post__in'=>$reviews
				];
				$rews=get_posts( $args );
				foreach ($rews as $rew) {
					$rmeta=get_post_meta( $rew->ID, '', false );
					?>
				<div class="col-xs-12 col-sm-6 el">
					<?php if (!empty($rmeta[$p.'ytvid'][0])): ?>
					<div class="video notp">
						<div class="youtube-player" data-id="<?=$rmeta[$p.'ytvid'][0]?>"></div>
					</div>
					<?php endif; ?>
					<div class="elcont">
						<div class="sideauthor_about">
							<?=(!empty($rmeta[$p.'rname'][0]))?'<p class="sideauthor_name colb">'.$rmeta[$p.'rname'][0].'</p>':null?>
							<?=(!empty($rmeta[$p.'rpos'][0]))?'<p class="sideauthor_info">'.$rmeta[$p.'rpos'][0].'</p>':null?>
							<hr>
						</div>
						<div class="quote">
							<?=(!empty($rew->post_content))?apply_filters( 'the_content', $rew->post_content ):null?>
						</div>
					</div>
				</div>
				<?php }
				unset($reveiws, $args, $rews, $rmeta);
				?>
			</div>
			<?php endif; ?>
			<?=(!empty($blockCont))?'<div class="row"><div class="cont">'.apply_filters('the_content', $blockCont).'</div></div>':null?>
		</article>
	</div>
</section>
