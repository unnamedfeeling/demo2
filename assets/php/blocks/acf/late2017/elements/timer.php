<div class="msclock msclock-mrg js-timer"  data-dend="<?=$endTime?>">
	<p class="msclock_p">
		До начала осталось
	</p>
	<hr class="msclock_hr">
	<div class="msclock_holder">
		<span class="msclock_digit days false "></span>
		<div class="msclock_label">дней</div>
	</div>
	<div class="msclock_holder">
		<span class=" msclock_digit hours false "></span>
		<div class="msclock_label">часов</div>
	</div>
	<div class="msclock_holder">
		<span class=" msclock_digit minutes false "></span>
		<div class="msclock_label">минут</div>
	</div>
</div>
