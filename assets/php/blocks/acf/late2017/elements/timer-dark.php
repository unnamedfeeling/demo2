<div class="msclock msclock-dark js-timer" data-dend="<?=$endTime?>">
	<p class="msclock_p">До начала осталось</p>
	<div class="clockdiv_holder">
		<span class="clockdiv_digit days"></span>
		<div class="clockdiv_label">Дней</div>
	</div>
	<div class="clockdiv_holder">
		<span class=" clockdiv_digit hours"></span>
		<div class="clockdiv_label">Часов</div>
	</div>
	<div class="clockdiv_holder">
		<span class=" clockdiv_digit minutes"></span>
		<div class="clockdiv_label">Минут</div>
	</div>
	<div class="clockdiv_holder">
		<span class=" clockdiv_digit seconds"></span>
		<div class="clockdiv_label">Секунд</div>
	</div>
</div>
