<?php global $options, $pmeta, $tpl, $p;

switch (true) {
	case (!empty($val[$p.'_bg'])):
		$bg='background:url('.((!empty($val[$p.'_bg']))?$val[$p.'_bg']:$tpl.'/assets/late2017/img/thanks/fon-s7.jpg');
		$bg.=') '.((!empty($val[$p.'_bgparams']))?$val[$p.'_bgparams']:'center / cover');
		break;
	case (!empty($val[$p.$val['template_group'].'_bg'])):
		$bg='background:url('.((!empty($val[$p.$val['template_group'].'_bg']))?$val[$p.$val['template_group'].'_bg']:$tpl.'/assets/late2017/img/thanks/fon-s7.jpg');
		$bg.=') '.((!empty($val[$p.$val['template_group'].'_bgparams']))?$val[$p.$val['template_group'].'_bgparams']:'center / cover');
		break;

	default:
		$bg='background:url(//via.placeholder.com/1920x1024.png)';
		break;
}
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="stages fluid <?=$val['template_group']?>-section" style="<?=$bg?>">
	<div class="topimg_cont">
		<div class="container container-big">
			<?='<p class="win_h2" style="color:#fff">'.$blockTtl.'</p>'?>
			<?=apply_filters('the_content', $blockCont)?>
		</div>
	</div>
</section>
