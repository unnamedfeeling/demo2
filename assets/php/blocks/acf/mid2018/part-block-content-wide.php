<?php global $options, $pmeta, $tpl, $p;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section">
	<div class="container-fluid">
		<article class="win">
			<?=(!empty($blockTtl))?'<p class="win_h2">'.$blockTtl.'</p>':null?>
			<?=(!empty($blockCont))?'<div class="cont">'.apply_filters('the_content', $blockCont).'</div>':null?>
		</article>
	</div>
</section>
