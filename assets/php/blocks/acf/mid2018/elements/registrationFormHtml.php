<?=(!empty($formTtl))?sprintf('<p class="h1 wind_h1">%s</p>', $formTtl):null?>
<?php if (!empty($formDescr)): ?>
<div class="gwind">
	<img src="<?=$tpl?>/assets/mid2018/img/gwind_dec.png" alt="" class="gwind_decor">
	<div class="gwind_text">
		<?=apply_filters( 'the_content', $formDescr )?>
	</div>
</div>
<?php endif; ?>

<?=(!empty($form->post_name))?do_shortcode('[hf_form slug="'.$form->post_name.'" html_class="wind_form js-wind_form"]'):null?>
<?php if (!empty($pmeta[$p.'webinardate'][0])&&empty($hideTimer)): ?>
	<div class="wind_form">
		<?php include('timer-dark.php') ?>
	</div>
<?php endif; ?>
