<div class="clockdiv js-timer" data-dend="<?=$endTime?>">
	<div class="clockdiv_holder">
		<span class="digit msclock_digit clockdiv_digit days">00</span>
		<div class="clockdiv_label">Дней</div>
	</div>
	<div class="clockdiv_holder">
		<span class="digit msclock_digit clockdiv_digit hours">00</span>
		<div class="clockdiv_label">Часов</div>
	</div>
	<div class="clockdiv_holder">
		<span class="digit msclock_digit clockdiv_digit minutes">00</span>
		<div class="clockdiv_label">Минут</div>
	</div>
	<div class="clockdiv_holder">
		<span class="digit msclock_digit clockdiv_digit seconds">00</span>
		<div class="clockdiv_label">Секунд</div>
	</div>
</div>
