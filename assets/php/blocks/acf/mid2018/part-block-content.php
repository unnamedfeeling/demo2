<?php global $options, $pmeta, $tpl, $p;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section ccomf">
	<div class="container container-mw">
		<article class="win">
			<?=(!empty($blockTtl))?'<p class="ccomf_wh2">'.$blockTtl.'</p>':null?>
			<?=(!empty($blockCont))?apply_filters('the_content', $blockCont):null?>
		</article>
	</div>
</section>
