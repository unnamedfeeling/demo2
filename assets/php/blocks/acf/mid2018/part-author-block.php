<?php global $options, $pmeta, $tpl, $p;
if (!empty($author)):
	$authorName=$author->post_title;
	$authorImg=(has_post_thumbnail( $author->ID ))?get_the_post_thumbnail_url($author->ID, 'large'):'//via.placeholder.com/463x560.png';
	$authorPos=(!empty($ameta[$p.'authorInfo'][0]))?'<p class="author_prof">'.$ameta[$p.'authorInfo'][0].'</p>':null;
	$authorEls=(!empty($ameta[$p.'authorEls'][0]))?maybe_unserialize($ameta[$p.'authorEls'][0]):null;
	$video=[
		'id'=>(!empty($ameta[$p.'authorVidId'][0]))?$ameta[$p.'authorVidId'][0]:null,
		'type'=>(!empty($ameta[$p.'authorVidType'][0]))?$ameta[$p.'authorVidType'][0]:null,
		'modaltype'=>'vid'
	];
	switch ($video['type']) {
		case 'youtube':
			$video['url']='https://www.youtube.com/watch?v=';
			break;

		default:
			$video['url']='https://home.wistia.com/medias/';
			$video['modaltype']='wistia';
			break;
	}
	?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section ccomf idea">
	<div class="container">
		<p class="ccomf_h1">Автор</p >
		<div class="row">
			<div class="col-xs-12 col-md-10 col-md-offset-1">
				<div class="idea_img" data-dadd="<?=$authorImg?>">
					<?php if (!empty($video['id'])): ?>
					<div class="soffer_vcont-bottom">
						<div class="soffer_lable">
							<p>Видеообращение автора </p> <button class="vbtn vbtn-orange js-<?=$video['modaltype']?>Modal" href="<?=$video['url'].$video['id']?>"> <i class="icon-play"></i> </button>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<div class="idea_wind">
					<p class="idea_h1"><?=$authorName?></p>
					<div class="idea_text">
						<?=(!empty($authorPos))?sprintf('<p class="lght">%s</p>', $authorPos):null?>
						<?php //<p class="ccomf_space"></p> ?>
						<?php if (!empty($author->post_content)): ?>
							<div class="cont">
								<?=apply_filters( 'the_content', $author->post_content )?>
							</div>
						<?php elseif(!empty($authorEls)): ?>
							<p class="">
								<?php
								if (!empty($authorEls)):
									foreach ($authorEls as $el):
										if (!empty($el)) {
											echo $el.'<br>'.PHP_EOL;
										}
									endforeach;
								endif; ?>
							</p>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif;
