<?php global $options, $pmeta, $tpl, $p;?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section container block-registrationForm">
	<div class="row">
		<div class="col-xs-12 col-lg-10 col-lg-offset-1">
			<div class="wind">
				<?php include('elements/registrationFormHtml.php') ?>
			</div>
		</div>
	</div>
</section>
