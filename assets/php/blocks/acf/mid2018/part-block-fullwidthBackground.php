<?php global $options, $pmeta, $tpl, $p;
switch (true) {
	case (!empty($val[$p.'_blockTargetEl'])):
		$target=$val[$p.'_blockTargetEl'];
		break;
	case (!empty($val[$p.$val['template_group'].'_blockTargetEl'])):
		$target=$val[$p.$val['template_group'].'_blockTargetEl'];
		break;

	default:
		$target=null;
		break;
}
switch (true) {
	case (!empty($val[$p.'_blockImg'])):
		$img=wp_get_attachment_image_src( $val[$p.'_blockImg']['ID'], 'full', false )[0];
		break;
	case (!empty($val[$p.$val['template_group'].'_blockImg'])):
		$img=wp_get_attachment_image_src( $val[$p.$val['template_group'].'_blockImg']['ID'], 'full', false )[0];
		break;

	default:
		$img='//via.placeholder.com/1920x500.png';
		break;
}
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section imgcont"<?=$style?>>
	<div class="imgcont_img" data-dadd="<?=$img?>">
		<!--<img src="https://cdn.baxtep.com/new/img/poster/tvid_sample.jpg" class="sample " alt="">-->
	</div>
	<div class="imgcont_cont">
		<div class="imgcont_middler">
			<div class="container">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1">
					<p class="tvid_h1"><?=$blockTtl?></p>
					<div class="tvid_p"><?=$blockCont?></div>
					<?php if (!empty($target)): ?>
					<button class="btn tvid_btn js-ancor" data-target="<?=$target?>">Занять место</button>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
