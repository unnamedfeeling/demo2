<?php global $options, $pmeta, $tpl, $p;
switch (true) {
	case (!empty($val[$p.$val['template_group'].'_blockCont'])):
		$blockCont=maybe_unserialize($val[$p.$val['template_group'].'_blockCont']);
		break;
	case (!empty($val[$p.'_blockCont'])):
		$blockCont=maybe_unserialize($val[$p.'_blockCont']);
		break;

	default:
		$blockCont=null;
		break;
}
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section ccomf">
	<div class="container container-mw">
		<article class="win">
			<?=(!empty($blockTtl))?'<p class="ccomf_wh2">'.$blockTtl.'</p>':null?>
			<?php
			if (!empty($blockCont)) {
				$cont='';
				foreach ($blockCont as $listEl) {
					$cont.=sprintf('<li><p>%s</p></li>', $listEl['element']);
				}
				printf('<ul class="ccomf_list">%s</ul>', $cont);
			}
			?>
		</article>
	</div>
</section>
