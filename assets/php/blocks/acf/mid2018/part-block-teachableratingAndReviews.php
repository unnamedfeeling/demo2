<?php global $options, $pmeta, $tpl, $p; ?>
<div class="section grey js-reviews" data-courseId="<?=(!empty($pmeta[$p.'teachableCourseId'][0]))?$pmeta[$p.'teachableCourseId'][0]:421007?>">
	<p class="micon_h1 h1">
		Отзывы студентов
	</p>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1">
				<section class="review">
					<div class="review_container">
						<div class="win_head review_head text-center">
							<ul class="win_headlist text-center listinfo xs-xs-hide">
								<li class="listinfo_item">
									<p class="listinfo_p"> Оценок:  <span class="js-total">0</span></p>
								</li>
								<li class="listinfo_item">
									<p class="listinfo_p">Зарегистрированных студентов: <span class="js-totalStudents">0</span></p>
								</li>
							</ul>
						</div>
						<div class="raiting-holder">
							<div class="raiting tb-text-center js-overall">
								<p class="raiting_digit">
									4.6
								</p>
								<div class="star_holder" data-raiting="4.6">
									<ul class="star" >
										<li class="star_item">
											<p class="star_star">
												<i class="icon-Star"></i>
											</p>
										</li>
										<li class="star_item">
											<p class="star_star">
												<i class="icon-Star"></i>
											</p>
										</li>
										<li class="star_item">
											<p class="star_star">
												<i class="icon-Star"></i>
											</p>
										</li>
										<li class="star_item">
											<p class="star_star">
												<i class="icon-Star"></i>
											</p>
										</li>
										<li class="star_item">
											<p class="star_star">
												<i class="icon-Star"></i>
											</p>
										</li>
									</ul>
									<div class="star_hider">
										<ul class="star">
											<li class="star_item">
												<p class="star_star star_star-active">
													<i class="icon-Star"></i>
												</p>
											</li>
											<li class="star_item">
												<p class="star_star star_star-active">
													<i class="icon-Star"></i>
												</p>
											</li>
											<li class="star_item">
												<p class="star_star star_star-active">
													<i class="icon-Star"></i>
												</p>
											</li>
											<li class="star_item">
												<p class="star_star star_star-active">
													<i class="icon-Star"></i>
												</p>
											</li>
											<li class="star_item">
												<p class="star_star star_star-active">
													<i class="icon-Star"></i>
												</p>
											</li>
										</ul>
									</div>
								</div>
								<div class="xs-xs-show">
									<ul class="win_headlist listinfo">
										<li class="listinfo_item">
											<p class="listinfo_p"> Оценок:  <span class="js-total">347</span></p>
										</li>
										<li class="listinfo_item">
											<p class="listinfo_p">Зарегистрированных студентов: 1 671</p>
										</li>
									</ul>
								</div>
							</div>
							<div class="circle-holder js-recommend">
								<div class="circle_el">
									<div class="circle" data-cerclevalue="4.9">
										<div class="circle_decor" ></div>
										<div class="circle_holder">
											<div class="circle_text">
												<p class="circle_digit">4.9</p>
												<p class="circle_discr">Порекомендую</p>
											</div>
										</div>
									</div>
								</div>
								<div class="circle_el js-material">
									<div class="circle" data-cerclevalue="4.7">
										<div class="circle_decor" ></div>
										<div class="circle_holder">
											<div class="circle_text">
												<p class="circle_digit">4.7</p>
												<p class="circle_discr">Материал</p>
											</div>
										</div>
									</div>
								</div>
								<div class="circle_el js-communication">
									<div class="circle" data-cerclevalue="4.2">
										<div class="circle_decor" ></div>
										<div class="circle_holder">
											<div class="circle_text">
												<p class="circle_digit">4.2</p>
												<p class="circle_discr">Взаимодействие</p>
											</div>
										</div>
									</div>
								</div>
								<div class="circle_el js-value">
									<div class="circle" data-cerclevalue="3.5">
										<div class="circle_decor" ></div>
										<div class="circle_holder">
											<div class="circle_text">
												<p class="circle_digit">3.5</p>
												<p class="circle_discr">Ценность </p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="js-revhtml"></div>
						<div class="ta-center" style="display:none">
							<a class="author_link js-loadmore" href="#">+ Показать больше</a>
						</div>
					</div>
					<div class="review_container text-center js-noposts" style="display:none">
						<h3>Еще нет отзывов на этот курс</h3>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
