<?php global $options, $pmeta, $tpl, $p;
$url=esc_url( get_the_permalink(), null, 'display' );
$text=esc_html('Я собираюсь пойти на '.$post->post_title);
$title=esc_html( $post->post_title );
$blog_name = get_bloginfo('name');
$img_to_pin = has_post_thumbnail() ? wp_get_attachment_url( get_post_thumbnail_id() ) : "";
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section sect">
	<div class="container text-center">
		<p class="scl_p">
			Пригласить друзей
		</p>
		<ul class="scl">
			<li>
				<a href="https://twitter.com/intent/tweet?text=<?=$text?>&url=<?=$url?>&hashtags=CoolestWebinarEver" target="_blank"><i class="icon-tw"></i></a>
			</li>
			<li>
				<a href="https://www.facebook.com/sharer/sharer.php?u=<?=$url?>&t=<?=$title?>" target="_blank"><i class="icon-fb"></i></a>
			</li>
			<li>
				<a href="https://vk.com/share.php?url=<?=urlencode($url)?>&title=<?=$title?>" target="_blank"><i class="icon-vk"></i></a>
			</li>
			<li>
				<a href="https://connect.ok.ru/offer?url=<?=urlencode($url)?>&title=<?=$title?>" target="_blank"><i class="icon-odnoklassniki"></i></a>
			</li>
			<li>
				<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=$url?>&title=<?=$title?>&summary=<?=$text?>&source=<?=$blog_name?>" target="_blank"><i class="icon-linkedin"></i></a>
			</li>
			<li>
				<a href="//www.pinterest.com/pin/create/button/?url=<?=$url?>&amp;media=<?=$img_to_pin?>&amp;description=<?=$title?>" target="_blank"><i class="icon-pinterest-p"></i></a>
			</li>
			<li>
				<a href="https://telegram.me/share/url?url=<?=urlencode($url)?>" target="_blank"><i class="icon-telegram"></i></a>
			</li>
			<li>
				<a href="mailto:?subject=<?=$text?>&amp;body=<?=$url?>" target="_blank"><i class="icon-envelope"></i></a>
			</li>
		</ul>
	</div>
</section>
