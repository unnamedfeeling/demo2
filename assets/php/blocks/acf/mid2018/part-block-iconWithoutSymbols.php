<?php global $options, $pmeta, $tpl, $p;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section white"<?=$style?>>
	<p class="micon_h1"><?=$blockTtl?></p>
	<div class="container">
		<?php
		if (!empty($blockCont)&&count($blockCont)>0) {
			$icnCount=count($blockCont);
			$icnInd=1;
			$blockCont=array_chunk($blockCont, 3);
			ob_start();
			foreach ($blockCont as $row):
				echo '<div class="row">';
				foreach ($row as $el) {
					$icon='';
					switch (true) {
						case ($el['iconImg']['subtype']=='svg+xml'):
							$icon=file_get_contents($el['iconImg']['url']);
							break;

						case (!empty($el['iconImg'])):
							$icon=sprintf('<img src="%s" alt="">', $el['iconImg']['url']);
							break;

						default:
							$icon=sprintf('<i class="icon-%s"></i>', $el['icon']);
							break;
					}

					printf('<div class="col-xs-12 col-sm-7 col-sm-offset-2 col-md-offset-0 col-md-4 ">
						<div class="micon_el micon_el-mod">
							<div class="micon_img">
								%s
							</div>
							<div class="micon_text">
								<p>%s</p>
							</div>
						</div>
					</div>',
						$icon,
						$el['description']);

					$icnInd++;
				}
				echo '</div>';
			endforeach;
			?>
			<?=ob_get_clean()?>
			<?php
		}
		if (!empty($val[$p.$val['template_group'].'_blockIconsColor'])):
			printf('<style>#%1$s .micon_img svg path{fill:%2$s!important}#%1$s .micon_img i{color:%2$s!important}</style>', $val['template_group'].'-'.$i, $val[$p.$val['template_group'].'_blockIconsColor']);
		endif; ?>
	</div>
</section>
