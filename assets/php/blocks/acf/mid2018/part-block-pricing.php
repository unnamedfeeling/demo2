<?php global $options, $pmeta, $tpl, $p;
$priceblocks=(!empty($val[$p.$val['template_group'].'_pricingEls']))?$val[$p.$val['template_group'].'_pricingEls']:null;
$time=time();

switch (true) {
	case (!empty($val[$p.'_pricingEls'])):
		$priceblocks=$val[$p.'_pricingEls'];
		break;
	case (!empty($val[$p.$val['template_group'].'_pricingEls'])):
		$priceblocks=$val[$p.$val['template_group'].'_pricingEls'];
		break;

	default:
		$priceblocks=null;
		break;
}

if (count($priceblocks)>0): ?>
	<section id="<?=$val['template_group'].'-'.$i?> pricing-table" class="<?=$val['template_group']?>-section package">
		<div class="container">
			<div class="row">
				<div class="package_slider dfc">
					<?php foreach ($priceblocks as $elem):
						$pricemeta=get_post_meta( $elem->ID, '', false );
						$price=(!empty($pricemeta[$p.'priceDisc'][0]))?$pricemeta[$p.'priceDisc'][0]:$pricemeta[$p.'price'][0];
						$schoolid=(!empty($pricemeta[$p.'teachableSchoolId'][0]))?$pricemeta[$p.'teachableSchoolId'][0]:127625;
						$coupon=(!empty($pricemeta[$p.'coupon'][0]))?' data-coupon="'.$pricemeta[$p.'coupon'][0].'"':null;
						?>
					<div class="package_slide">
						<article class="package_el">
							<p class="package_n">пакет</p>
								<p class="package_h2"><?=$elem->post_title?></p>
								<?=apply_filters( 'the_content', $elem->post_content )?>
							<div class="package_price-holder">
								<p class="package_price">
									$<?=$price?>
									<?php if (!empty($pricemeta[$p.'priceDisc'][0])):
										$percent=floor(100-((int)$pricemeta[$p.'priceDisc'][0]/(int)$pricemeta[$p.'price'][0]*100));
										?>
									<br><span class="package_yellow"><del>$<?=$pricemeta[$p.'price'][0]?></del>, скидка <?=$percent?>%</span>
									<?php endif; ?>
								</p>
							</div>
							<?php if (!empty($pricemeta[$p.'priceDisc'][0])): ?>
							<p class="package_timelife">
								Данная цена доступна еще <br>
								<?=get_date_diff( $time, strtotime($pricemeta[$p.'promoTerm'][0]), 3 )?>!</p>
							<?php endif;

							printf(
								'<span data-prodid="%1$s" data-courseid="%2$s" data-schoolId="%3$s" data-price="%4$s"%5$s class="package_btn buybtn js-buybtn">Купить</span>',
								$pricemeta[$p.'teachableProductId'][0],
								$pricemeta[$p.'teachableCourseId'][0],
								$schoolid,
								$price,
								$coupon
							);
							?>
						</article>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
