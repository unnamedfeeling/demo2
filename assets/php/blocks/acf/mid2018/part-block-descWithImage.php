<?php global $options, $pmeta, $tpl, $p;
?>
<section  id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section idea">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-10 col-md-offset-1">
				<div class="idea_img" data-dadd="<?=$img?>">
					<?php if (!empty($video['id'])): ?>
					<div class="soffer_vcont-bottom">
						<div class="soffer_lable">
							<button class="vbtn vbtn-orange js-<?=$video['modaltype']?>Modal" href="<?=$video['url'].$video['id']?>"> <i class="icon-play"></i> </button>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<div class="idea_wind">
					<p class="idea_h1"><?=$blockTtl?></p>
					<div class="idea_text">
						<?php //<p class="ccomf_space"></p> ?>
						<?php if (!empty($blockCont)): ?>
							<div class="cont">
								<?=apply_filters( 'the_content', $blockCont )?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
