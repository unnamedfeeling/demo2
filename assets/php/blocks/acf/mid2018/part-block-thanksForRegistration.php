<?php global $options, $pmeta, $tpl, $p;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="ccomf cctop <?=$val['template_group']?>-section">
	<?=(!empty($blockTtl))?'<p class="cctop_h1">'.$blockTtl.'</p>':null?>
	<?=(!empty($blockCont))?'<div class="cctop_info">'.apply_filters('the_content', $blockCont).'</p>':null?>
</section>
