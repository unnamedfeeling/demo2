<?php global $options, $pmeta, $tpl, $p;

$fieldsArr=['_blockSofferStyle', '_blockRegform'];

foreach ($fieldsArr as $el) {
	switch (true) {
		case (!empty($val[$p.$val['template_group'].$el])):
			$fieldsArr[$el]=(strpos($el, 'Style')!==false)?' style="'.$val[$p.$val['template_group'].$el].'"':$val[$p.$val['template_group'].$el];
			break;
		case (!empty($val[$p.$el])):
			$fieldsArr[$el]=(strpos($el, 'Style')!==false)?' style="'.$val[$p.$el].'"':$val[$p.$el];
			break;

		default:
			$fieldsArr[$el]=null;
			break;
	}

}

// switch (true) {
// 	case (!empty($val[$p.$val['template_group'].'_blockRegform'])):
// 		$form=$val[$p.$val['template_group'].'_blockRegform'];
// 		break;
// 	case (!empty($val[$p.'_blockRegform'])):
// 		$form=$val[$p.'_blockRegform'];
// 		break;
//
// 	default:
// 		$form=null;
// 		break;
// }

$video['img']=(empty($video['img']))?'//via.placeholder.com/450x435.png':$video['img'];
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section section ccomf soffer"<?=$style?>>
	<div class="soffer_bg"<?=$fieldsArr['_blockSofferStyle']?>>
		<div class="container container-mw">
			<?=apply_filters('the_content', $blockTtl)?>
		</div>
	</div>
	<?php if (!empty($video['id'])||!empty($form)): ?>
		<div class="container container-mw">
			<?php if (!empty($video['img'])): ?>
				<div class="col-xs-12 col-sm-6">
					<div class="soffer_vcont">
						<img src="<?=$video['img']?>"  alt="">
						<?php if (!empty($video['id'])): ?>
						<div class="soffer_vcont-bottom">
							<div class="soffer_lable">
								<p>Видеообращение автора </p> <button class="vbtn js-<?=$video['modaltype']?>Modal" href="<?=$video['url'].$video['id']?>"> <i class="icon-play"></i> </button>
							</div>
						</div>
					<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if (!empty($fieldsArr['_blockRegform'])): ?>
				<div class="col-xs-12<?= (!empty($video['img']))?' col-sm-6':'' ?>">
					<div class="soffer_vcont">
						<div class="wind small">
							<?=(!empty($fieldsArr['_blockRegform']->post_name))?do_shortcode('[hf_form slug="'.$fieldsArr['_blockRegform']->post_name.'" html_class="wind_form js-wind_form"]'):null?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<?php if (!empty($blockCont)): ?>
		<div class="container container-mw">
			<div class="row">
				<div class="col-xs-12">
					<div class="soffer_p">
						<?=apply_filters('the_content', $blockCont)?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
