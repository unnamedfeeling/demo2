<?php global $options, $pmeta, $tpl, $p;
$fieldsArr=['_blockLink', '_btnText'];

foreach ($fieldsArr as $el) {
	switch (true) {
		case (!empty($val[$p.$val['template_group'].$el])):
			$fieldsArr[$el]=(strpos($el, 'Style')!==false)?' style="'.$val[$p.$val['template_group'].$el].'"':$val[$p.$val['template_group'].$el];
			break;
		case (!empty($val[$p.$el])):
			$fieldsArr[$el]=(strpos($el, 'Style')!==false)?' style="'.$val[$p.$el].'"':$val[$p.$el];
			break;

		default:
			$fieldsArr[$el]=null;
			break;
	}

}
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section section ccomf"<?=$style?>>
	<div class="container container-mw">
		<div class="row">
			<div class="soffer_bt dfc aic">
				<div class="col-xs-12 col-md-8">
					<p class="soffer_p soffer_p-org"><?=$blockTtl?></p>
					<p class="soffer_p soffer_p-lrg"><?=$blockCont?></p>
				</div>
				<?php if (!empty($fieldsArr['_blockLink'])): ?>
				<div class="col-xs-12 col-md-4 text-right">
					<?php
					switch ($val[$p.'_btnType']) {
						case 'firstForm':
							printf(
								'<button class="package_btn soffer_btn js-scrollToFirstForm">%s</button>',
								(!empty($fieldsArr['_btnText']))?$fieldsArr['_btnText']:'Занять место'
							);
							break;

						case 'modal':
							printf(
								'<button class="package_btn soffer_btn js-callModal" data-mfp-src="%s">%s</button>',
								(!empty($val[$p.'_blockTargetEl']))?$val[$p.'_blockTargetEl']:'#registerForm',
								(!empty($fieldsArr['_btnText']))?$fieldsArr['_btnText']:'Занять место'
							);
							break;

						default:
							printf(
								'<button class="package_btn soffer_btn js-gotohref" data-href="%s">%s</button>',
								$fieldsArr['_blockLink'],
								(!empty($fieldsArr['_btnText']))?$fieldsArr['_btnText']:'Занять место'
							);
							break;
					}
					?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
