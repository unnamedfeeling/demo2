<?php global $options, $pmeta, $tpl, $p;
$video['img']=(!empty($video['img']))?$video['img']:'//via.placeholder.com/960x540.png';
$video['id']=(!empty($video['id']))?$video['id']:'jvfhf0aaie';
$blockTtl=(!empty($blockTtl))?$blockTtl:'Видео';
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section ccomf">
	<div class="container container-mw">
		<article class="win">
			<?=(!empty($blockTtl))?'<p class="ccomf_wh2">'.$blockTtl.'</p>':null?>
				<?= do_shortcode(
					sprintf(
						'[hbvideo type="%s" id="%s" poster="%s"]',
						$video['type'],
						$video['id'],
						$video['img']
					)
				);
				?>
		</article>
	</div>
</section>
