<?php global $options, $pmeta, $tpl, $p;

switch (true) {
	case (!empty($val[$p.'_blockBg'])):
		$bg=$val[$p.$val['template_group'].'_blockBg']['url'];
		break;
	case (!empty($val[$p.$val['template_group'].'_blockBg'])):
		$bg=$val[$p.$val['template_group'].'_blockBg']['url'];
		break;

	default:
		$bg='//via.placeholder.com/1920x630.png';
		break;
}
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section tvid">
	<div class="tvid_poster tvid_poster-hei" data-dadd="<?=$bg?>">
		<img src="<?=$tpl?>/assets/mid2018/img/poster/tvid_sample.jpg" class="sample " alt="">
		<a href="<?=$video['url'].$video['id']?>" class="tvid_playicon-mob tvid_playicon js-<?=$video['modaltype']?>Modal">
			<img src="<?=$tpl?>/assets/new/img/icon/icon_play-2.png" alt="">
		</a>
	</div>
	<div class="tvid_cont">
		<div class="tvid_middler">
			<div class="container text-center">
				<div class="row">
					<div class="col-xs-12 col-md-8 col-md-offset-2">
						<p class="tvid_h1"><?=$blockTtl?></p>
					</div>
					<div class="col-xs-12 col-md-8 col-md-offset-2">
						<p class="tvid_p"><?=$blockCont?></p>
					</div>
				</div>
				<a href="<?=$video['url'].$video['id']?>" class="tvid_playicon js-<?=$video['modaltype']?>Modal">
					<img src="<?=$tpl?>/assets/mid2018/img/icon/icon_play-2.png" alt="">
				</a>
				<br>
				<a class="btn tvid_btn js-ancor" href="#pricing-table">Занять место</a>
			</div>
		</div>
		<div class="tvid_arw">
			<i class="icon-List-Open"></i>
		</div>
	</div>
</section>
