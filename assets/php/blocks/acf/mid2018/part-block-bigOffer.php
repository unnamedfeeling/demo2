<?php global $options, $pmeta, $tpl, $p;

switch (true) {
	case (!empty($val[$p.$val['template_group'].'_blockSofferStyle'])):
		$sofferStyle=' style="'.$val[$p.$val['template_group'].'_blockSofferStyle'].'"';
		break;
	case (!empty($val[$p.'_blockSofferStyle'])):
		$sofferStyle=' style="'.$val[$p.'_blockSofferStyle'].'"';
		break;

	default:
		$sofferStyle=null;
		break;
}

$video['img']=(empty($video['img']))?'//via.placeholder.com/960x475.png':$video['img'];
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section section ccomf soffer"<?=$style?>>
	<div class="soffer_bg"<?=$sofferStyle?>>
		<div class="container container-mw">
			<?=apply_filters('the_content', $blockTtl)?>
		</div>
	</div>
	<?php if (!empty($video['id'])): ?>
		<div class="container container-mw">
			<div class="soffer_vcont">
				<img src="<?=$video['img']?>"  alt="">
				<div class="soffer_vcont-bottom">
					<div class="soffer_lable">
						<p>Видеообращение автора </p> <button class="vbtn js-<?=$video['modaltype']?>Modal" href="<?=$video['url'].$video['id']?>"> <i class="icon-play"></i> </button>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if (!empty($blockCont)): ?>
		<div class="container container-mw">
			<div class="row">
				<div class="col-xs-12">
					<div class="soffer_p">
						<?=apply_filters('the_content', $blockCont)?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
