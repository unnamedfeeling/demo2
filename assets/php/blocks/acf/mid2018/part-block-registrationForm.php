<?php global $options, $pmeta, $tpl, $p;
switch (true) {
	case (!empty($val[$p.'_blockRegform'])):
		$form=$val[$p.'_blockRegform'];
		break;
	case (!empty($val[$p.$val['template_group'].'_blockRegform'])):
		$form=$val[$p.$val['template_group'].'_blockRegform'];
		break;

	default:
		$form=null;
		break;
}

$btnText=(!empty($val[$p.'_btnText']))?$val[$p.'_btnText']:null;
$formTtl=(!empty($val[$p.'_formTtl']))?$val[$p.'_formTtl']:null;
$formDescr=(!empty($val[$p.'_formDescr']))?$val[$p.'_formDescr']:null;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section <?=$val['template_group']?> container">
	<div class="row">
		<div class="col-xs-12 col-lg-10 col-lg-offset-1">
			<div class="wind">
				<?=(!empty($formTtl))?sprintf('<p class="h1 wind_h1">%s</p>', $formTtl):null?>
				<?php if (!empty($formDescr)): ?>
				<div class="gwind">
					<img src="<?=$tpl?>/assets/mid2018/img/gwind_dec.png" alt="" class="gwind_decor">
					<div class="gwind_text">
						<?=apply_filters( 'the_content', $formDescr )?>
					</div>
				</div>
				<?php endif; ?>
				<?=(!empty($form->post_name))?do_shortcode('[hf_form slug="'.$form->post_name.'" html_class="wind_form js-wind_form"]'):null?>
				<?php
				if (!empty($pmeta[$p.'webinardate'][0])):
					include('elements/timer-dark.php');
				endif; ?>
			</div>
		</div>
	</div>
</section>
