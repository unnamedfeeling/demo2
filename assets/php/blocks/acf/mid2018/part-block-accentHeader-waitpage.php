<?php global $options, $pmeta, $tpl, $p;
$blockWebinarType=(!empty($val[$p.$val['template_group'].'_blockWebinarType']))?$val[$p.$val['template_group'].'_blockWebinarType']:null;

switch (true) {
	case (!empty($val[$p.'_blockImg'])):
		$img=wp_get_attachment_image_src( $val[$p.'_blockImg']['ID'], 'full', false )[0];
		break;
	case (!empty($val[$p.$val['template_group'].'_blockImg'])):
		$img=wp_get_attachment_image_src( $val[$p.$val['template_group'].'_blockImg']['ID'], 'full', false )[0];
		break;

	default:
		$img='//via.placeholder.com/1440x756.png';
		break;
}

if (empty($author)&&!empty($pmeta[$p.'webauthor'][0])) {
	$author=get_post( $pmeta[$p.'webauthor'][0], OBJECT, 'raw' );
	$ameta=get_post_meta( $pmeta[$p.'webauthor'][0], '', false );
}

$blockOpts=(!empty($val[$p.$val['template_group'].'_blockOpts']))?$val[$p.$val['template_group'].'_blockOpts']:null;

if (count($blockOpts)>0) {
	$elnum=ceil(count($blockOpts)/2);
	$blockOpts=array_chunk($blockOpts, $elnum);
}

$btnText=(!empty($val[$p.'_btnText']))?$val[$p.'_btnText']:null;
$formTtl=(!empty($val[$p.'_formTtl']))?$val[$p.'_formTtl']:null;
$formDescr=(!empty($val[$p.'_formDescr']))?$val[$p.'_formDescr']:null;
$hideTimer=(!empty($val[$p.'_hideTimer']))?$val[$p.'_hideTimer']:null;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section mstop js-padtop">
	<div class="mstop_img" data-dadd="<?=$img?>">
		<div class="mstop_grad"></div>
		<img src="//via.placeholder.com/249x110.png" class="sample" alt="">
	</div>
	<div class="mstop_cont">
		<div class="container"  >
			<div class="row row-va-middle">
				<div class="col-xs-12 hidden-sm hidden-xs col-md-4 col-lg-3">
					<p class="mstop_p text-right"><?=$blockWebinarType?></p>
				</div>
				<div class="col-xs-12 hidden-sm hidden-xs col-md-6 col-lg-6"></div>
				<div class="col-xs-12 hidden-sm hidden-xs col-md-2 col-lg-3">
					<p class="mstop_p"><?=$author->post_title?></p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-2 col-md-offset-5 text-center">
					<?php if (!empty($video['id'])): ?>
					<button class="mstop_ibtn js-<?=$video['modaltype']?>Modal" href="<?=$video['url'].$video['id']?>">
						<img src="<?=$tpl?>/assets/mid2018/img/icon/icon_play-2.png" alt="">
					</button>
					<?php endif; ?>

				</div>
				<div class="col-xs-12 hidden-md hidden-lg text-center">
					<p class="mstop_p "><?=$blockWebinarType?></p>
				</div>
				<div class="col-xs-12 hidden-md hidden-lg text-center">
					<p class="mstop_p"><?=$author->post_title?></p>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-10 col-sm-offset-1">
					<p class="mstop_h1"><?=$post->post_title?></p>
				</div>
			</div>
			<div class="row text-center">
				<?=(!empty($endTime))?
					sprintf(
						'<p class="mstop_begin js-webinarDate">%s <span class="sep"></span> %s UTC(+2)</p>',
						strtr(date('d F, D', $endTime),
						$options['translate']),
						strtr(date('H:i', $endTime), $options['translate'])
					):'<p class="mstop_begin js-webinarDate"></p>'?>
				<button class="mstop_btn js-scrollToFirstForm"><?=(!empty($btnText))?$btnText:'Зарегистрироваться'?></button>
				<br>
				<?php include('elements/timer.php') ?>
			</div>
			<div class="row">
				<div class="col-xs-12  col-lg-10 col-lg-offset-1 text-center">
					<div class="mstop_text"><?=apply_filters( 'the_content', $blockCont )?></div>
				</div>
			</div>
			<div class="row ">
				<div class="col-xs-12">
					<p class="mstop_h2 text-center"><?=$blockTtl?></p>
				</div>
				<?php $i=0; foreach ($blockOpts as $col):
					if (count($col)>0): ?>
						<div class="col-xs-12 col-sm-6 col-lg-5<?=($i%2==0)?' col-lg-offset-1':null?>">
							<div class="mstop_list">
								<ul>
									<?php foreach ($col as $el):
										printf('<li>%s</li>', $el['text']);
									endforeach; ?>
								</ul>
							</div>
						</div>
					<?php $i++; endif;
				endforeach; ?>
			</div>
		</div>
	</div>
</section>
