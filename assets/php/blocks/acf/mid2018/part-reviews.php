<?php global $options, $pmeta, $tpl, $p;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="reviews <?=$val['template_group']?>-section crev ccomf">
	<div class="container container-mw">
		<p class="ccomf_h1">Отзывы</p>
		<?php if (!empty($pmeta[$p.'attached_reviews'][0])): ?>
		<div class="row">
			<?php
			$reviews=maybe_unserialize( $pmeta[$p.'attached_reviews'][0] );
			$args=[
				'post_type'=>'reviews',
				'posts_per_page'=>-1,
				'orderby'=>'post__in',
				'post__in'=>$reviews
			];
			$rews=get_posts( $args );
			$i=0;
			foreach ($rews as $rew) {
				$rmeta=get_post_meta( $rew->ID, '', false );
				$video=[
					'id'=>(!empty($rmeta[$p.'ytvid'][0]))?$rmeta[$p.'ytvid'][0]:null,
					'type'=>(!empty($rmeta[$p.'vidType'][0]))?$rmeta[$p.'vidType'][0]:null,
					'modaltype'=>'vid'
				];
				switch ($video['type']) {
					case 'youtube':
						$video['url']='https://www.youtube.com/watch?v=';
						break;

					default:
						$video['url']='https://home.wistia.com/medias/';
						$video['modaltype']='wistia';
						break;
				}
				?>
			<div class="col-xs-12 col-md-6 text-<?=($i%2==0)?'left':'right'?>">
				<div class="crev_el">
					<?php if (!empty($rmeta[$p.'ytvid'][0])):
						$img=(has_post_thumbnail( $rew->ID ))?get_the_post_thumbnail_url($rew->ID, 'large'):'//via.placeholder.com/430x247.png';
						?>
					<div class="crev_img">
						<img src="<?=$img?>" class="crev_photo" alt="<?=$rew->post_title?>">
						<div class="crev_btn-pos">
							<button class="vbtn vbtn-orange js-<?=$video['modaltype']?>Modal" href="<?=$video['url'].$video['id']?>"> <i class="icon-play"></i> </button>
						</div>
					</div>
					<?php endif; ?>
					<div class="crev_info">
						<?=(!empty($rmeta[$p.'rname'][0]))?'<p class="crev_name">'.$rmeta[$p.'rname'][0].'</p>':null?>
						<?=(!empty($rmeta[$p.'rpos'][0]))?'<p class="crev_pos">'.$rmeta[$p.'rpos'][0].'</p>':null?>
						<div class="crev_text">
							<?=(!empty($rew->post_content))?apply_filters( 'the_content', $rew->post_content ):null?>
						</div>
					</div>
				</div>
			</div>
			<?php $i++; }
			unset($reveiws, $args, $rews, $rmeta);
			?>
		</div>
		<?php endif; ?>
		<?=(!empty($blockCont))?'<div class="row"><div class="cont">'.apply_filters('the_content', $blockCont).'</div></div>':null?>
		<div class="row">
			<div class="col-xs-12">
				<hr class="ccomf_hr">
			</div>
		</div>
	</div>

</section>
