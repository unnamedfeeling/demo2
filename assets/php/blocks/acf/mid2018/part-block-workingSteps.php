<?php global $options, $pmeta, $tpl, $p;
switch (true) {
	case (!empty($val[$p.$val['template_group'].'_blockCont'])):
		$blockCont=maybe_unserialize($val[$p.$val['template_group'].'_blockCont']);
		break;
	case (!empty($val[$p.'_blockCont'])):
		$blockCont=maybe_unserialize($val[$p.'_blockCont']);
		break;

	default:
		$blockCont=null;
		break;
}
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section section ccomf"<?=$style?>>
	<div class="container container-mw">
		<div class="row">
			<?php
			$icnCount=count($blockCont);
			$icnInd=1;
			if ($icnCount>0):
				foreach ($blockCont as $el): ?>
					<div class="col-xs-12  col-md-offset-0 col-md-4">
						<div class="steps_el">
							<div class="steps_img">
								<?= ($icnInd<$icnCount)?'<div class="steps_arw"></div>':null ?>
								<i class="icon-<?=$el['icon']?>"></i>
							</div>
							<div class="steps_text">
								<p><?=$el['description']?></p>
							</div>
						</div>
					</div>
				<?php
					$icnInd++;
				endforeach;
			endif; ?>
			<div class="col-xs-12">
				<hr class="ccomf_hr">
			</div>
		</div>
	</div>
</section>
