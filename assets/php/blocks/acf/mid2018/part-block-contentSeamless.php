<?php global $options, $pmeta, $tpl, $p;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section white">
	<p class="micon_h1">
		<?=$blockTtl?>
	</p>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
				<p class="review_text text-center">
					<?=apply_filters( 'the_content', $blockCont )?>
				</p>
			</div>
		</div>
	</div>
</section>
