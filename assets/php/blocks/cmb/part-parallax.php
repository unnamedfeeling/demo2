<?php global $options, $pmeta, $tpl, $p;
$blockTtl=(!empty($val[$p.$val['template_group'].'_blockTtl']))?$val[$p.$val['template_group'].'_blockTtl']:null;
$blockCont=(!empty($val[$p.$val['template_group'].'_blockCont']))?$val[$p.$val['template_group'].'_blockCont']:null;
$bg='background:url('.((!empty($val[$p.$val['template_group'].'_blockBg']))?$val[$p.$val['template_group'].'_blockBg']:$tpl.'assets/img/bg/tbg.jpg');
$bg.=') '.((!empty($val[$p.$val['template_group'].'_blockBgparams']))?$val[$p.$val['template_group'].'_blockBgparams']:'center / cover fixed');
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="bwbg <?=$val['template_group']?>-section" style="<?=$bg?>">
	<div class="container">
		<div class="ta-center">
			<?=(!empty($blockTtl))?'<div class="h1">'.$blockTtl.'</div>':null?>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<?=(!empty($blockCont))?'<div class="bwbg_p">'.apply_filters('the_content', $blockCont).'</div>':null?>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-sm-offset-4">
				<button class="package_btn js-callModal" data-mfp-src="#registerForm" tabindex="0" onclick="if (typeof analytics  != 'undefined') {analytics.track('Вебинар - Кнопка по центру');}">Зарегистрироваться</button>
			</div>
		</div>
	</div>
</section>
