<?php global $options, $pmeta, $tpl, $p;
if (!empty($author)):
	$authorName=$author->post_title;
	$authorImg=(has_post_thumbnail( $author->ID ))?get_the_post_thumbnail_url($author->ID, 'authorImg'):$tpl.'/assets/late2017/img/avart/gladir2.jpg';
	$authorPos=(!empty($ameta[$p.'authorInfo'][0]))?'<p class="author_prof">'.$ameta[$p.'authorInfo'][0].'</p>':null;
	?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section">
	<div class="container">
		<article class="win">
			<p class="win_h2">АВТОР</p>
			<div class="row">
				<div class="author_el">
					<div class="author_ls">
						<div class="author_avat-holder">
							<img src="<?=$authorImg?>" alt="<?=$authorName?>" class="author_avat">
						</div>
						<div class="author_infoholder">
							<div class="xs-xs-show sm-show">
								<p class="author_name"><?=$authorName?></p>
								<?=$authorPos?>
							</div>
						</div>
					</div>
					<div class="author_rs js_hidder-parent">
						<div class="sm-hide xs-xs-hide">
							<p class="author_name"><?=$authorName?></p>
							<?=$authorPos?>
						</div>
						<?=(!empty($author->post_content))?apply_filters('the_content', $author->post_content):null?>
					</div>
				</div>
			</div>
		</article>
	</div>
</section>
<?php endif; ?>
