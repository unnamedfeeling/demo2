<?php global $options, $pmeta, $tpl, $p;
$blockTtl=(!empty($val[$p.$val['template_group'].'_blockTtl']))?$val[$p.$val['template_group'].'_blockTtl']:null;
$blockCont=(!empty($val[$p.$val['template_group'].'_blockCont']))?maybe_unserialize($val[$p.$val['template_group'].'_blockCont']):null;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section">
	<div class="container">
		<article class="win">
			<?=(!empty($blockTtl))?'<p class="win_h2">'.$blockTtl.'</p>':null?>
			<?php
			if (!empty($blockCont)) {
				echo '<ul>';
				foreach ($blockCont as $val) {
					printf('<li><p class="win_p">%s</p></li>', $val);
				}
				echo '</ul>';
			}
			?>
		</article>
	</div>
</section>
