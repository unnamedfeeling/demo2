<?php global $options, $pmeta, $tpl, $p;
?>
<section id="page-content" class="page-content-section">
	<div class="<?=($pmeta[$p.'widecont'][0]=='on')?'container-fluid':'container'?>">
		<article class="win">
			<?=apply_filters('the_content', $post->post_content)?>
		</article>
	</div>
</section>
