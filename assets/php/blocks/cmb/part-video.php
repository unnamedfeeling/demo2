<?php global $options, $pmeta, $tpl, $p;
$vidimg=(!empty($val[$p.$val['template_group'].'_vidimg']))?wp_get_attachment_image_src( $val[$p.$val['template_group'].'_vidimg_id'], 'webinarVid', false )[0]:$tpl.'/assets/late2017/img/wistia-img.jpg';
$vidid=(!empty($val[$p.$val['template_group'].'_vidid']))?$val[$p.$val['template_group'].'_vidid']:'jvfhf0aaie';
$blockTtl=(!empty($val[$p.$val['template_group'].'_blockTtl']))?$val[$p.$val['template_group'].'_blockTtl']:'Подробнее о МАСТЕР-КЛАССЕ';

if (!empty($author)) {
	$authorName=$author->post_title;
	$authorImg=(has_post_thumbnail( $author->ID ))?get_the_post_thumbnail_url($author->ID, 'authorImg'):$tpl.'/assets/late2017/img/avart/gladir2.jpg';
	$authorInfo=(!empty($ameta[$p.'authorInfo'][0]))?'<p class="sideauthor_info">'.$ameta[$p.'authorInfo'][0].'</p>':null;
	$authorPos=(!empty($ameta[$p.'authorInfo'][0]))?'<p class="sideauthor_pos">'.$ameta[$p.'authorInfo'][0].'</p>':null;
} else {
	$authorName=(!empty($val[$p.$val['template_group'].'_authorName']))?$val[$p.$val['template_group'].'_authorName']:null;
	$authorImg=(!empty($val[$p.$val['template_group'].'_authorImg']))?wp_get_attachment_image_src( $val[$p.$val['template_group'].'_authorImg'], 'authorImg', false )[0]:$tpl.'/assets/late2017/img/avart/gladir2.jpg';
	$authorInfo=(!empty($val[$p.$val['template_group'].'_authorInfo']))?'<p class="sideauthor_info">'.$val[$p.$val['template_group'].'_authorInfo'].'</p>':null;
	$authorPos=(!empty($val[$p.$val['template_group'].'_authorPosition']))?'<p class="sideauthor_pos">'.$val[$p.$val['template_group'].'_authorPosition'].'</p>':null;
}
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section">
	<div class="container container-big mtop">
		<div class="row row-drow">
			<div class="col-xs-12 col-md-8">
				<div class="video">
					<div class="wistia-placeholder js-wistia-placeholder">
						<img src="<?=$vidimg?>" class="img-responsive" alt="<?=$authorName?>">
						<div class="playbtn"></div>
					</div>
					<script>
					window._wq = window._wq || [];
					_wq.push({
					  id: "<?=$vidid?>",
					  options: {
						videoFoam: true,
						autoPlay: true
					  }
					});
					</script>
					<div class="wistia_responsive_padding js-wistia-video" style="display:none;padding:56.25% 0 0 0;position:relative;">
						<div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_<?=$vidid?>" style="height:100%;width:100%">&nbsp;</div></div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<div class="win win-gor win-top">
					<p class="win_h3"><?=$blockTtl?></p>
					<div class="sideauthor">
						<div class="sideauthor_avat-holder">
							<img src="<?=$authorImg?>" alt="man_140x140" class="sideauthor_avat">
						</div>
						<div class="sideauthor_about">
							<p class="sideauthor_name"><?=$authorName?></p>
							<?=$authorInfo?>
							<?=$authorPos?>
						</div>
					</div>
					<div class="calendar">
						<p class="side_h1">КАЛЕНДАРЬ</p>
						<ul class="calendar_list js-cal"></ul>
					</div>
					<div class="text-center">
						<button class="calendar_btn js-calendar-hidden"> показать больше </button>
					</div>
					<div class="side_h1">до начала осталось</div>
					<div class="clockdiv js-timer">
						<div class="clockdiv_holder">
							<span class="clockdiv_digit days"></span>
							<div class="clockdiv_label">Дней</div>
						</div>
						<div class="clockdiv_holder">
							<span class="clockdiv_digit hours"></span>
							<div class="clockdiv_label">Часов</div>
						</div>
						<div class="clockdiv_holder">
							<span class="clockdiv_digit minutes"></span>
							<div class="clockdiv_label">Минут</div>
						</div>
						<div class="clockdiv_holder">
							<span class="clockdiv_digit seconds"></span>
							<div class="clockdiv_label">Секунд</div>
						</div>
					</div>
					<div class="text-center js-registered"><p></p></div>
					<div class="text-center">
						<button class="package_btn js-callModal" data-mfp-src="#registerForm">Зарегистрироваться</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
