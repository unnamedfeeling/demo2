<?php global $options, $pmeta, $tpl, $p;
$bg='background:url('.((!empty($val[$p.$val['template_group'].'_bg']))?$val[$p.$val['template_group'].'_bg']:$tpl.'/assets/late2017/img/thanks/fon-s7.jpg');
$bg.=') '.((!empty($val[$p.$val['template_group'].'_bgparams']))?$val[$p.$val['template_group'].'_bgparams']:'center / cover');
$blockTtl=(!empty($val[$p.$val['template_group'].'_blockTtl']))?$val[$p.$val['template_group'].'_blockTtl']:null;
$blockCont=(!empty($val[$p.$val['template_group'].'_blockCont']))?$val[$p.$val['template_group'].'_blockCont']:null;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="stages fluid <?=$val['template_group']?>-section" style="<?=$bg?>">
	<div class="topimg_cont">
		<div class="container container-big">
			<?=(!empty($blockTtl))?'<p class="win_h2" style="color:#fff">'.$blockTtl.'</p>':null?>
			<?=(!empty($blockCont))?apply_filters('the_content', $blockCont):null?>
		</div>
	</div>
</section>
