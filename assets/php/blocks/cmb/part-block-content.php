<?php global $options, $pmeta, $tpl, $p;
$blockTtl=(!empty($val[$p.$val['template_group'].'_blockTtl']))?$val[$p.$val['template_group'].'_blockTtl']:null;
$blockCont=(!empty($val[$p.$val['template_group'].'_blockCont']))?$val[$p.$val['template_group'].'_blockCont']:null;
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="<?=$val['template_group']?>-section">
	<div class="container">
		<article class="win">
			<?=(!empty($blockTtl))?'<p class="win_h2">'.$blockTtl.'</p>':null?>
			<?=(!empty($blockCont))?'<div class="cont">'.apply_filters('the_content', $blockCont).'</div>':null?>
		</article>
	</div>
</section>
