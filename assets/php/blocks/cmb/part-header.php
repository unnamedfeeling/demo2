<?php global $options, $pmeta, $tpl, $p;
$bg='background:url('.((!empty($val[$p.$val['template_group'].'_bg']))?$val[$p.$val['template_group'].'_bg']:$tpl.'/assets/late2017/img/bg/gladyr_orig_bg_m.jpg');
$bg.=') '.((!empty($val[$p.$val['template_group'].'_bgparams']))?$val[$p.$val['template_group'].'_bgparams']:'center / cover');
?>
<section id="<?=$val['template_group'].'-'.$i?>" class="topimg topimg-pdbot" style="<?=$bg?>">
	<div class="topimg_cont">
		<div class="container container-big">
			<div class="row">
				<?php if (!empty($val[$p.$val['template_group'].'_title'])): ?>
					<?=$val[$p.$val['template_group'].'_title']?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
