<?php
require './vendor/autoload.php';
require 'config.php';
// Using Medoo namespace
use Medoo\Medoo;
// initial variables
$key='305f56f0eaa1d5214a02940fc3e4b7f63f9fc24a94b1cb716e0299e5271fa5ba';
$zapkey='ZTi1o0l5GDhtmqK1lZed9trnPghXprrWF3l';
if (!empty($_POST)&&$_POST['zapkey']==$zapkey&&!empty($_POST['webid'])) {
	if (empty($_POST['email'])) {
		$responce['result']='No email - no registration. Go out!';
		die(json_encode($responce));
	}
	// setup responce
	$responce['result']='OK';
	$responce['post']=$_POST;
	$responce['webid']=$responce['post']['webid'];
	$responce['date']=date('d-m-Y--H-i-s', time());
	$responce['time']=time();
	$responce['filename']=__DIR__.'/../../messages/zapier-manychat-'.$responce['date'].'.txt';

	// Initialize
	$database = new Medoo($dbconf);
	$tbl='sent_sms';
	$ind=($database->max($tbl, 'id')>0) ? $database->max($tbl, 'id')+1 : 0;
	$type='mc';

	// get webinar data
	$getWebinarOpts = array(
		CURLOPT_URL			   => "https://webinarjam.genndi.com/api/webinar",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_POST		   => true,
		CURLOPT_POSTFIELDS	   => 'api_key='.$key.'&webinar_id='.$responce['webid'],
	);
	$ch = curl_init();
	curl_setopt_array($ch, $getWebinarOpts);
	$data=curl_exec($ch);
	curl_close($ch);
	$responce['getWebinar']=json_decode($data);

	// register to webinar
	$registerOpts = array(
		CURLOPT_URL			   => "https://webinarjam.genndi.com/api/register",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_POST		   => true,
	);
	// find first newer date in schedules
	foreach($responce['getWebinar']->webinar->schedules as $el) {
		if(strtotime($el->date)>time()) {
			$sched = $el->schedule;
			break;
		}
	}
	$sched=(!empty($sched))?$sched:0;
	$ch = curl_init();
	// $str='api_key='.$key.'&webinar_id='.$responce['webid'].'&first_name='.explode(' ', $responce['post']['full_name'])[0].'&email='.$responce['post']['email'].'&schedule='.$sched;
	// $str.=(!empty($responce['post']['phone']))?'&phone='.$responce['post']['phone']:null;
	$str=array(
		'api_key'=>$key,
		'webinar_id'=>$responce['webid'],
		'first_name'=>explode(' ', $responce['post']['full_name'])[0],
		'email'=>$responce['post']['email'],
		'schedule'=>$sched
	);
	if (!empty($responce['post']['phone'])) {
		$phonestr=(mb_strlen($responce['post']['phone'])>9)?preg_replace("/[^A-Za-z0-9+]/", '', $responce['post']['phone']):$responce['post']['phone'];
			$str['phone_country_code']='380';
		if (!empty($phonestr)&&strlen($phonestr)>9) {
			$str['phone']=mb_substr($responce['post']['phone'], strpos($responce['post']['phone'], '0')+1);
		} elseif(!empty($phonestr)&&strlen($phonestr)==9) {
			$str['phone']=$responce['post']['phone'];
			$responce['post']['phone']='380'.$responce['post']['phone'];
		} elseif(!empty($phonestr)) {
			$str['phone']=$responce['post']['phone'];
			$responce['post']['phone']='380'.$responce['post']['phone'];
		}
	}

	$str=http_build_query($str);
	curl_setopt_array($ch, $registerOpts);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
	// sleep(30);
	$data=curl_exec($ch);
	curl_close($ch);
	$responce['register']=$data;

	// send sms
	if (!empty($responce['post']['phone'])) {
		try {
			// Подключаемся к серверу
			$client = new SoapClient('http://turbosms.in.ua/api/wsdl.html');
			// Данные авторизации
			$auth = [
				'login' => 'E2uAUiB',
				'password' => 'FyfWyQjFeWjFDT4rKgBQCUBybws2T4'
			];
			// Авторизируемся на сервере
			$result = $client->Auth($auth);
			// Результат авторизации
			$responce['turboSMS']['auth']= $result->AuthResult . PHP_EOL;
			// Получаем количество доступных кредитов
			$responce['turboSMS']['balance'] = $client->GetCreditBalance();
			// Текст сообщения ОБЯЗАТЕЛЬНО отправлять в кодировке UTF-8
			// $text = iconv('windows-1251', 'utf-8', 'Благодарим за регистрацию на вебинар! Ожидайте данные о времени и ссылку на вебинар на email.');
			$text = 'Благодарим за регистрацию на вебинар! Ожидайте данные о времени и ссылку на вебинар на email.';
			// $text = 'Мастер-класс “3 способа убрать страх видеокамеры и сцены” начнётся в 19:00, ссылка на трансляцию: http://bit.ly/2BIxgTJ . Если Вы не успеваете, вы можете посмотреть с телефона.';
			// Отправляем сообщение на один номер.
			// Подпись отправителя может содержать английские буквы и цифры. Максимальная длина - 11 символов.
			// Номер указывается в полном формате, включая плюс и код страны
			$smsPhone=$responce['post']['phone'];
			$sms = [
				'sender' => 'Hearbeat',
				'destination' => $responce['post']['phone'],
				'text' => $text
			];
			$timediff=(file_exists(__DIR__.'/../../sms/'.$responce['post']['phone'].'.txt'))?$responce['time']-file_get_contents(__DIR__.'/../../sms/'.$responce['post']['phone'].'.txt'):false;
			if (!file_exists(__DIR__.'/../../sms/'.$responce['post']['phone'].'.txt')||$timediff>10800) {
				$responce['turboSMS']['sendResult'] = $client->SendSMS($sms);
				$ttl=$responce['getWebinar']->webinar->name;

				// Запрашиваем статус конкретного сообщения по ID
				// $sms = ['MessageId' => 'c9482a41-27d1-44f8-bd5c-d34104ca5ba9'];
				// $status = $client->GetMessageStatus($sms);
				// echo $status->GetMessageStatusResult . PHP_EOL;

				$fsms = fopen(__DIR__.'/../../sms/responce-mc-'.$responce['post']['phone'].'.txt', 'w');
				fwrite($fsms, json_encode($responce['turboSMS']));
				fclose($fsms);

				try {
					$tsms=var_export($responce['turboSMS'], true);
					if (!empty($responce['turboSMS']['sendResult'])&&$responce['turboSMS']['sendResult']->SendSMSResult->ResultArray[0]==='Сообщения успешно отправлены') {
						$database->insert($tbl, [
							'id'=>$ind,
							'type'=>$type,
							'name'=>$responce['post']['full_name'],
							'phone'=>$smsPhone,
							'email'=>$responce['post']['email'],
							'status'=>'succ',
							'time'=>time(),
							'result'=>'Success! Context: '.$tsms,
							'wtitle'=>$ttl,
						]);
						$fsms = fopen(__DIR__.'/../../sms/'.$responce['post']['phone'].'.txt', 'w');
						fwrite($fsms, time());
						fclose($fsms);
					} else {
						$database->insert($tbl, [
							'id'=>$ind,
							'type'=>$type,
							'name'=>$responce['post']['full_name'],
							'phone'=>$smsPhone,
							'email'=>$responce['post']['email'],
							'status'=>'err',
							'time'=>time(),
							'result'=>'Error! Context: '.$tsms,
							'wtitle'=>$ttl,
						]);
						$fsms = fopen(__DIR__.'/../../sms/'.$responce['post']['phone'].'.txt', 'w');
						fwrite($fsms, json_encode($responce['turboSMS']['sendResult']));
						fclose($fsms);
					}
				} catch (Exception $e) {
					$tsms=var_export($e, true);
					$database->insert($tbl, [
						'id'=>$ind,
						'type'=>$type,
						'name'=>$responce['post']['full_name'],
						'phone'=>$smsPhone,
						'email'=>$responce['post']['email'],
						'status'=>'err',
						'time'=>time(),
						'result'=>'Error! Context: '.$tsms,
						'wtitle'=>$ttl,
					]);
					$fsms = fopen(__DIR__.'/../../sms/Error-'.$responce['post']['phone'].'.txt', 'w');
					fwrite($fsms, $e);
					fclose($fsms);
				}
			}
		} catch(Exception $e) {
			$tsms=var_export($e, true);
			$database->insert($tbl, [
				'id'=>$ind,
				'type'=>$type,
				'name'=>$responce['post']['full_name'],
				'phone'=>$smsPhone,
				'email'=>$responce['post']['email'],
				'status'=>'err',
				'time'=>time(),
				'result'=>'Error! Context: '.$tsms,
				'wtitle'=>$ttl,
			]);
			$responce['turboSMS']['error'] = 'Ошибка: ' . $e->getMessage() . PHP_EOL;
			$fsms = fopen(__DIR__.'/../../sms/Error-'.$responce['time'].'.txt', 'w');
			fwrite($fsms, $responce['turboSMS']['error']);
			fclose($fsms);
		}
	}
	// write logdata
	$logdata=json_encode($responce);
	$fp = fopen($responce['filename'], 'w');
	fwrite($fp, $logdata);
	fclose($fp);
	die(json_encode($responce));
} else {
	$responce['result']='Go and fuck yourself!';
	die(json_encode( $responce ));
}
