<?php
function heartweb_videoSC($atts){
  $atts = shortcode_atts(
  array(
    'type' => 'youtube',
    'id' => '',
    'poster' => '//via.placeholder.com/945x597.png',
  ), $atts, 'hbvideo' );
  switch ($atts['type']) {
    case 'wistia':
      ob_start();
      ?>
      <div class="video-wrapper">
        <div class="wistia-placeholder js-wistia-placeholder">
          <img src="<?=$atts['poster']?>" class="img-responsive">
          <div class="playbtn"></div>
        </div>
        <script>
        window._wq = window._wq || [];
        _wq.push({
          id: "<?=$atts['id']?>",
          options: {
            videoFoam: true,
            autoPlay: true,
            qualityMin: 720
          }
        });
        </script>
        <div class="wistia_responsive_padding js-wistia-video" style="display:none;padding:63.25% 0 0 0;position:relative;">
          <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_<?=$atts['id']?>" style="height:100%;width:100%">&nbsp;</div></div>
        </div>
      </div>
      <?php
      return ob_get_clean();
      break;

    default:
      return sprintf(
        '<div class="video-wrapper"><div class="js-youtube-player" data-id="%s" data-poster="%s"></div></div>',
        $atts['id'],
        $atts['poster']
      );
      break;
  }
}

function heartweb_regbuttonSC($atts){
  $atts = shortcode_atts(
  array(
    'type' => 'regbutton',
    'text' => 'Зарегистрироваться',
  ), $atts, 'hbvideo' );

  $html='';
  switch ($atts['type']) {
    case 'modal':
      $html = sprintf(
        '<div class="text-center"><button class="mstop_btn %s" data-mfp-src="#registerForm">%s</button></div>',
        'js-callModal',
        $atts['text']
      );
      break;

    default:
      $html = sprintf(
        '<div class="text-center"><button class="mstop_btn %s">%s</button></div>',
        'js-scrollToFirstForm',
        $atts['text']
      );
      break;
  }

  return $html;
}


function heartweb_addEvent($atts) {
  global $pmeta, $post;
  // print_r($post);
  $atts = shortcode_atts([
    'datestart'=>(!empty($pmeta['heartweb_webinardate']))?date('d-M-Y H:i', strtotime($pmeta['heartweb_webinardate'][0])):null,
    'dateend'=>(!empty($pmeta['heartweb_webinardate']))?date('d-M-Y H:i', (int)strtotime($pmeta['heartweb_webinardate'][0])+7200):null,
    'allday'=>'false',
    'title'=>'',
    'description'=>'',
    'organizer'=>'',
    'organizeremail'=>'',
    'type'=>'inline-buttons',
  ],$atts);

  if (!empty($post->post_parent)&&empty($atts['title'])) {
    $parent=get_post($post->post_parent);
    $atts['title']=$parent->post_title;
  }

  if (!empty($atts['datestart'])) {
    return sprintf('
    <div title="%1$s" class="addeventatc js-calendar"%11$s>%2$s %3$s %4$s %5$s %7$s %8$s %9$s %6$s %10$s  </div>',
      __('Add to calendar', 'heartweb'),
      sprintf('<span class="start">%s</span>', $atts['datestart']),
      sprintf('<span class="end">%s</span>', $atts['dateend']),
      '<span class="timezone">Europe/Kiev</span>',
      (!empty($atts['title']))?sprintf('<span class="title">%s</span>', $atts['title']):'',
      sprintf('<span class="all_day_event">%s</span>', $atts['allday']),
      (!empty($atts['description']))?sprintf('<span class="description">%s</span>', $atts['description']):'',
      (!empty($atts['organizer']))?sprintf('<span class="organizer">%s</span>', $atts['organizer']):'',
      (!empty($atts['organizer_email']))?sprintf('<span class="organizer_email">%s</span>', $atts['organizer_email']):'',
      '<span class="addeventatc_dropdown js-addeventatc_dropdown" aria-hidden="false" aria-labelledby="addeventatc1" style="display: none;"><span class="ateappleical" id="appleical" role="menuitem" tabindex="0">Apple</span><span class="ategoogle" id="google" role="menuitem" tabindex="0">Google <em>(online)</em></span><span class="ateoutlook" id="outlook" role="menuitem" tabindex="0">Outlook</span><span class="ateoutlookcom" id="outlookcom" role="menuitem" tabindex="0">Outlook.com <em>(online)</em></span><span class="ateyahoo" id="yahoo" role="menuitem" tabindex="0">Yahoo <em>(online)</em></span><em class="copyx"><em class="brx"></em><em class="frs"><a href="https://www.addevent.com" title="" tabindex="-1" id="home">AddEvent.com</a></em></em></span>',
      ($atts['type']!=='none')?sprintf(' data-render="%s"', $atts['type']):''
    );
  }
}
add_shortcode('hb_addEvent','heartweb_addEvent');
