
(function( root, $, undefined ) {
	"use strict";


	$(document).on('submit', '.js-form', function(event){
		event.preventDefault();
		var date = new Date(),
			form=$(this),
			msg=new Object(),
			inputs=form.find('input, select');
		if (form.find('.form-group').hasClass('error')) {
			form.find('.form-group').removeClass('error');
			form.find('.js-errorbox-all').slideUp(300, function() {
				$(this).empty();
			})
		}
		// $(this).find('.js-time').val(date.getDate()+'.'+date.getMonth()+'.'+date.getFullYear());
		// console.log(inputs);
		inputs.each(function(){
			var th=$(this);
			msg[th.attr('name')]=th.val();
		})


		msg.webid=(typeof docData.webid!=='undefined')?docData.webid:'undefined';
		msg.type=(typeof docData.type!=='undefined')?docData.type:'';
		msg.func='register';
		// if (msg.time===0||msg.time==='0') {
		// 	msg.time='first';
		// }

		var res=sendData(msg, {url:form.attr('action'), type:'POST'});
		$.when( res ).then(
			function( result ) {
				result=JSON.parse(result);
				// console.log('1');
				// console.log(result);
				var info={};

				if (typeof result.data.error!=='undefined') {
					info.type='error';
					form.find('.js-errorbox-all').addClass('error').removeClass('success');
					switch (result.data.error.type) {
						case 'validation':
							info.string=result.data.error.data;
							for (var x = 0; x < result.data.error.names.length; x++) {
								$('[name='+result.data.error.names[x]+']').addClass('error');
							}
							break;
						default:
							info.string='<p>Что-то пошло не так и мы не смогли зарегистрировать вас! Попробуйте еще раз немного позже!</p>';
					}
				} else {
					form.find('.js-errorbox-all').addClass('success').removeClass('error');
					info.type='success';
					info.string='<p>Регистрация прошла успешно! Ожидайте SMS со ссылкой на вебинарную комнату на контактный номер телефона</p>'
				}
				form.find('.js-errorbox-all').removeClass('success error').addClass(info.type).html(info.string)
				form.find('.js-errorbox-all').slideDown(500);
			},
			function( result ) {
				result=JSON.parse(result);
				// console.log('2');
				console.log(result);
			}
		);
	});

	//prototype BEGIN
	$.prototype.loop_slick = function(argument) {
		$(this).each(function(index, el) {
			$(el).slick({
				nextArrow: '<button type="button" class="slick-next"><i class="icon-play"></i></button>',
				prevArrow: '<button type="button" class="slick-prev"><i class="icon-play"></i></button>',

			});


		});
	};

	var pageTimers=[]

	$.prototype.ptimer = function(params) {
		// body...
		if (!$(this)[0]) {
			return false
		}

		var trg=$(this),
			date=(params!==undefined&&params.date!==undefined)?params.date:''

		if (typeof params!=='undefined'&&params.action=='destroy') {
			pageTimers=pageTimers.filter(function(item, i, arr){
				if (item.elem[0]==trg[0]) {
					clearInterval(item.timerId)
				} else {
					return item
				}
			})
			return pageTimers
		}

		function getTimeRemaining(endtime) {
			// console.log(endtime);
			var t = Date.parse(endtime) - Date.parse(new Date());
			var seconds = Math.floor((t / 1000) % 60);
			var minutes = Math.floor((t / 1000 / 60) % 60);
			var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
			var days = Math.floor(t / (1000 * 60 * 60 * 24));
			// console.log('t:',t);
			return {
				'total': t,
				'days': days,
				'hours': hours,
				'minutes': minutes,
				'seconds': seconds
			};
		}

		function initializeClock(id, endtime) {
			var clock = id;
			var daysSpan = clock.find('.days')[0];
			var hoursSpan = clock.find('.hours')[0];
			var minutesSpan = clock.find('.minutes')[0];
			var secondsSpan = clock.find('.seconds')[0];
			// console.log(clock, daysSpan, hoursSpan, minutesSpan, secondsSpan);

			function updateClock() {
				// console.log('endtime:',endtime);
				var t = getTimeRemaining(endtime);

				if (typeof daysSpan!=='undefined') {
					daysSpan.innerHTML = format(t.days);
				}
				if (typeof hoursSpan!=='undefined') {
					hoursSpan.innerHTML = format(t.hours);
				}
				if (typeof minutesSpan!=='undefined') {
					minutesSpan.innerHTML = format(t.minutes);
				}
				if (typeof secondsSpan!=='undefined') {
					secondsSpan.innerHTML = format(t.seconds);
				}

				if (t.total <= 0) {
					clearInterval(timeinterval);
				}
			}

			function format(t) {
				return "<div>" + ('0' + t).slice(-2)[0] + "</div>" + "<div>" + ('0' + t).slice(-2)[1] + "</div>"
			}

			updateClock();
			var timeinterval = setInterval(updateClock, 1000);
			pageTimers.push({elem: id, timerId: timeinterval})
		}

		// var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
		// var s = '2011-06-21T14:27:28.593Z';
		// var s = (typeof date !== 'undefined') ? date[0]+'-'+date[1].replace(':', '-') : null,
		// 	a = s.split('-'),
		// 	d=new Date (a[0],a[1]-1,a[2],a[3],a[4] );

		date=(date=='')?parseInt(this.data('dend')):date;

		// console.log(date);

		// var deadline = (typeof date !== 'undefined') ? new Date(Date.parse(date*1000)) : new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
		var deadline = (typeof date !== 'undefined') ? new Date(date*1000) : new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
		// console.log('date:',Date.parse(date*1000));
		// console.log('d:',d);
		// console.log('deadline:',deadline);
		initializeClock(trg, deadline);
	};
	//
	//
	//
	// $.prototype.mystar = function(uoption) {
	// 	if (!this[0]) {
	// 		return false
	// 	}
	// 	option = {
	// 		raiting: Number(this[0].dataset.raiting) || 0,
	// 		click: false
	// 	}
	// 	$.extend(option, uoption);
	// 	$(this).each(function(index, el) {
	// 		var r = 100 / (5 / option.raiting),
	// 			t = $(el),
	// 			hider = $(t).find('.star_hider');
	// 		hider.width(r + '%');
	// 		var timer;
	// 		if (option.click) {
	// 			t.addClass('clicked');
	// 			t.click(function(e) {
	// 				/* Act on the event */
	// 				var x = e.offsetX == undefined ? e.layerX : e.offsetX;
	// 				var nr = (x * 100) / t.width();
	// 				nr = nr > 100 ? 100 : nr;
	// 				r = nr.toFixed(2);
	// 				hider.width(nr + '%');
	// 				if ($(this).find('.star-popup')[0]) {
	// 					popup = $(this).find('.star-popup');
	// 				} else {
	// 					$(this).append('<div class="star-popup"></div>').animate({
	// 						opacity: 1
	// 					}, 500);
	// 					popup = $(this).find('.star-popup');
	// 				}
	// 				popup.stop(true, false);
	// 				popup.text((x / 100 * 5).toFixed(1));
	// 				if (timer) {
	// 					clearTimeout(timer);
	// 				}
	// 				timer = setTimeout((function() {
	// 					popup.animate({
	// 						opacity: 0
	// 					}, 300, function() {
	// 						popup.remove();
	// 					});
	// 				}), 1000);
	// 			});
	// 		}
	// 	});
	// };

	// console.log(webinarType);
	// console.log(webID);
	// switch (webinarType) {
	// 	case 'bigmarker':
	//
	// 		break;
	// 	default:
	// 		// webinarjam | undefined
	//
	// }

	//prototype EOF
	/*
	 * Custom
	 */
	/*$(window).load(function(){

	});*/
	// window._wq = window._wq || [];
	// _wq.push({
	//   id: "nsn69b29b3",
	//   options: {
	// 	videoFoam: true,
	// 	autoplay: true
	//   }
	// });

	var viewport=vp(),
		res,
		curPage = window.location.href,
		weekday = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
		month = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
	// console.log(curPage.search('success')!==-1);
	// console.log(curPage.search('thanks')!==-1);
	// console.log(getCookie('successReg'));
	// console.log(curPage.replace(/(success|thanks)([\/]?)([\.html]{0,5})/, ''));
	// console.log(typeof getCookie('successReg')=='undefined'&&(curPage.search('success')!==-1||curPage.search('thanks')!==-1));

	if (typeof getCookie('successReg')=='undefined'&&(curPage.search('success')!==-1||curPage.search('thanks')!==-1)) {
		var redir=curPage.replace(/(success|thanks)([\/]?)([\.html]{0,5})/, '');
		// console.log(redir);
		window.location.href=redir;
	}
	// console.log(document.body.dataset.webinar);
	$.ajax({
		type: 'post',
		dataType: 'json',
		data: {func: 'webinar', webid: (typeof docData.webid!=='undefined')?docData.webid:'', postId:(typeof docData.pid!=='undefined')?docData.pid:'', webinarType: webinarType},
		// url: ajax_func.tpld+'/assets/php/wj.php',
		url: '/wp-json/heartweb/v1/webinarData',
		success: function(data){
			// console.log('success: ', data);
			// console.log('getWebinar: ', JSON.parse(data.getWebinar));
			res= data.data;
		},
		error: function(data){
			console.log('error: ', data);
		}
	}).done(function(){
		var html_li='',
			// html_opt_d='<option placeholder>Выберите дату</option>',
			html_opt_d='',
			html_opt_s=[],
			html_opt_so=[],
			ind=0,
			jsdate=$('.js-dateSel'),
			jstime=$('.js-timeDd'),
			timerInitialized=false,
			webName;
		// switch (webinarType) {
		// 	case 'webinarjam':
		// 		webName=res.getWebinar.webinar.name;
		// 		break;
		// 	case 'bigmarker':
		// 		webName=res.getWebinar.conference.title
		// 		break;
		// 	default:
		// 		webName=''
		// }
		// $('.js-webinarId').val(webID);
		// $('.js-webinarName').val(webName);
		if (!isNaN(res.registrations)) {
			var numreg=res.registrations;
			if (numreg<=43) {
				numreg=43;
			}
			$('.js-registered').find('p').text('Уже зарегистрированы '+numreg+' участников');
		}
		// $(JSON.parse(res.getWebinar).webinar.schedules).each(function(index) {
		$(res.getWebinar.webinar.schedules).each(function(index) {
			// if (this.webinar_id==document.body.dataset.webinar) {
				// $(this.schedules).each(function(index){
					var d=this.date.split(' '),
						schedId=this.schedule,
						date=new Date(Date.parse(d[0])),
						s = (typeof d !== 'undefined') ? d[0]+'-'+d[1].replace(':', '-') : null,
						a = s.split('-'),
						darr=new Date (a[0],a[1]-1,a[2],a[3],a[4] ).getTime();
					if (darr>new Date().getTime()) {
						if (timerInitialized!==true) {
							// console.log(date);
							$('.js-contdown').ptimer(d);
							timerInitialized=true;
						}
						var n = weekday[date.getDay()],
							m = month[date.getMonth()],
							s = d[1];
						// html_li+='<li class="calendar_item'+((ind>2)?' js-calendar-hidden':'')+'"><i class="icon-calendar-check-o"></i><p class="calendar_p ">'+n+', '+date.getDate()+' '+m+' '+date.getFullYear()+', '+d[1]+' (GMT +2)</p></li>';
						html_li+='<li class="calendar_item'+((ind>2)?' js-calendar-hidden':'')+'"><i class="icon-calendar-check-o"></i><p class="calendar_p ">'+n+', '+date.getDate()+' '+m+' '+date.getFullYear()+', '+d[1]+'</p></li>';
						// html_opt_d+='<option value="'+d[0]+'" '+((ind===0)?' selected':'')+'>'+n+', '+date.getDate()+' '+m+' '+date.getFullYear()+'</option>';

						if (html_opt_d.search('<option value="'+d[0]+'">'+n+', '+date.getDate()+' '+m+' '+date.getFullYear()+'</option>')=='-1') {
							html_opt_d+='<option value="'+d[0]+'">'+n+', '+date.getDate()+' '+m+' '+date.getFullYear()+'</option>';
						}

						// if (typeof html_opt_s[d[0]] == 'undefined') {
							// html_opt_s[d[0]]='<option value="'+schedId+'" data-time="'+d[1]+'"'+((ind===0)?' selected':'')+'>'+d[1]+' (GMT +2)</option>';
							html_opt_s[d[0]]='<option value="'+schedId+'" data-time="'+d[1]+'"'+((ind===0)?' selected':'')+'>'+d[1]+'</option>';
						// } else {
						// 	html_opt_s[d[0]]+='<option value="'+schedId+'" data-time="'+d[1]+'"'+((ind===0)?' selected':'')+'>'+d[1]+'</option>';
						// }

						// html_opt_so[d[0]]={value: s, label: s};

						// if (ind==0&&$('#registerForm .js-timeDd').length) {
						// 	// $('#registerForm .js-timeDd').empty().html(html_opt_s[d[0]]);
						// 	$('#registerForm .js-timeDd').empty().html('<option placeholder>Сперва выберите дату</option>');
						// }
						ind+=1;
					}
					// console.log(html_opt_d);
					// console.log(html_opt_s);
				// })
			// }
		});
		// console.log(html_opt_d);
		// console.log(html_opt_s);
		// console.log(html_opt_so);
		if(jsdate.length){
			jsdate.empty().html(html_opt_d);
			// console.log(jsdate.val());
			jstime.empty().html(html_opt_s[jsdate.val()]);
			// console.log(jstime);
			var dateChoice = new Choices(jsdate.get(0), {
					shouldSort: false,
				}),
				timeChoice = new Choices(jstime.get(0), {
					shouldSort: false,
					// callbackOnInit: function(){
					// 	this.disable();
					// }
				});
			// if (jstime.val()===0||jstime.val()===''||jstime.val()===null) {
			// if (isNaN(jstime.val())||jstime.val()===0) {
			// 	jstime.val('first');
			// }
			// console.log(jstime.val());
			dateChoice.passedElement.addEventListener('addItem', function(event){
				// do something creative here...
				// console.log(event.detail.id);
				// console.log(event.detail.value);
				// console.log(event.detail.label);
				// console.log(event.detail.groupValue);
				var selDate=event.detail.value;
				if(jstime.length){
					// console.log(html_opt_s[selDate]);
					// console.log('obj: ', html_opt_so[selDate]);
					if (jstime.parent().hasClass('choices__inner')) {
						// console.log('before slideUp');
						// jstime.parent().slideUp(300, function() {
							// console.log('setChoices: ', html_opt_s[selDate]);
							timeChoice.destroy();
							jstime.empty().html(html_opt_s[selDate]);
							timeChoice = new Choices(jstime.get(0), {
								shouldSort: false,
								// callbackOnInit: function(){
								// 	this.enable();
								// }
							});
						// });
						// console.log('before slideDown');
						// jstime.parent().parent().parent().slideDown(300, function() {
						// 	//Stuff to do *after* the animation takes place
						// })
					} else {
						// timeChoice.init();
						jstime.empty().html(html_opt_s[selDate]);
						timeChoice = new Choices(jstime.get(0), {
							shouldSort: false,
							// callbackOnInit: function(){
							// 	this.enable();
							// }
						});
						// timeChoice.disable();
						// jstime.parent().parent().parent().slideDown(500, function() {
						// 	//Stuff to do *after* the animation takes place
						// })
					}

				}
			}, false);
		}

		// // segment code
		// !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="4.0.0";
		// analytics.load("EXvIntfSLHco2KpNqC23karCFxcgBRnA");
		// analytics.page();
		// }}();

		// console.log(html_li);
		$('.js-cal').append(html_li);
		if ($($.parseHTML(html_li)).find('li').length<4) {
			$('.js-calendar-hidden').remove();
		}

		if ($('.js-successList').length) {
			var successData={
					register: JSON.parse(JSON.parse(getCookie('successReg')).register),
					webinar: JSON.parse(JSON.parse(getCookie('successReg')).getWebinar),
					// webinar: JSON.parse(JSON.parse(res.getWebinar).webinar),
				},
				// regDate=new Date(Date.parse(successData.register.user.date)),
				dstr=successData.register.user.date.split(' '),
				regDate;
			dstr[1]=dstr[1].replace(':', '-');
			dstr=dstr[0]+'-'+dstr[1];
			dstr=dstr.split('-');
			regDate=new Date(dstr[0],dstr[1]-1,dstr[2],dstr[3],dstr[4]);
			var	n = weekday[regDate.getDay()],
				m = month[regDate.getMonth()],
				dateHtml='<span class="custom_calendar_text"><span data-template-key="day_name">'+n+'</span>, <span data-template-key="day">'+regDate.getDate()+'</span> <span data-template-key="month_name">'+m+'</span> <span data-template-key="year">'+regDate.getFullYear()+'</span>, в <span data-template-key="time">'+((regDate.getHours()<10)?'0'+regDate.getHours():regDate.getHours())+':'+((regDate.getMinutes()<10)?'0'+regDate.getMinutes():regDate.getMinutes())+'</span></span>',
				presname='';
			// console.log('user.date:', successData.register.user.date);
			// console.log('dstr:', dstr);
			// console.log('regDate:', regDate);
			// console.log(successData);
			if (successData.webinar.webinar.presenters.length>1) {
				$(successData.webinar.webinar.presenters).each(function(index){
					presname+=(index==0)?this.name:', '+this.name;
				})
			} else {
				presname=successData.webinar.webinar.presenters[0].name;
			}
			$('.js-presenter-name').text(presname);
			$('.js-thanksDate').html(dateHtml);
			$('.js-webinarLink').html('<a href="'+successData.register.user.live_room_url+'" target="_blank">'+successData.register.user.live_room_url+'</a>');
			// $('.js-presenters')
			// opts.timeZoneName = 'long';
			// console.log(regDate.toLocaleTimeString('en-US', opts));
			// $('.js-timezone').text(successData.register.user.timezone+'!!!')
		}

	});

	// $(document).on('click', '.js-callModal', function(event){
	// 	// if (typeof res !== 'undefined') {
	// 	// 	$.ajax({
	// 	// 		type: 'post',
	// 	// 		dataType: 'json',
	// 	// 		data: {d: new Date()},
	// 	// 		url: 'php/wj.php',
	// 	// 		success: function(data){
	// 	// 			console.log('success: ', data);
	// 	// 			res= data;
	// 	// 		},
	// 	// 		error: function(data){
	// 	// 			console.log('error: ', data);
	// 	// 		}
	// 	// 	}).done(function(){
	// 	// 		console.log('res=', res);
	// 	// 		if (res.status=='ok') {
	// 				// $('.js-callModal').magnificPopup({
	// 				// 	type: 'inline',
	// 				// 	midClick: true,
	// 				// 	callbacks: {
	// 				// 	open: function() {
	// 				// 		// console.log('open');
	// 				//
	// 				// 	},
	// 				// 	close: function() {
	// 				// 	  // Will fire when popup is closed
	// 				// 	}
	// 				// 	// e.t.c.
	// 				//   }
	// 				// });
	// 				$.magnificPopup.open({
	// 					items: {
	// 						src: $(event.target.dataset.mfpSrc),
	// 						type: 'inline'
	// 					},
	// 					midClick: true,
	// 					fixedContentPos: 'true',
	// 					overflowY: 'scroll',
	// 					callbacks: {
	// 						open: function(){
	// 							document.body.classList.add('modal-open');
	// 						},
	// 						close: function(){
	// 							document.body.classList.remove('modal-open');
	// 						}
	// 					}
	// 				});
	// 	// 		}
	// 	// 	})
	// 	// }
	//
	// });

	/* viewport width */
	$(function() {
		// fix header for wp admin bar
		// if (typeof document.getElementById('wpadminbar')!=='undefined') {
		// if ($('#wpadminbar').length) {
		// 	$('header').css('top', document.getElementById('wpadminbar').clientHeight+'px');
		// }

		// Detect ios 11_0_x affected
		// NEED TO BE UPDATED if new versions are affected
		var ua = navigator.userAgent,
		iOS = /iPad|iPhone|iPod/.test(ua),
		iOS11 = /OS 10_0|OS 10_1|OS 10_2|OS 10_3|OS 11_0|OS 11_0_1|OS 11_0_2|OS 11_0_3|OS 11_1/.test(ua);
		// console.log('iOS='+iOS+';iOS11='+iOS11);

		// ios 11 bug caret position
		if ( iOS && iOS11 ) {

			// Add CSS class to body
			$("body").addClass("iosBugFixCaret");

		}
		/* placeholder*/
		$('.js-mbtn').click(function(event) {
			/* Act on the event */
			$(this).toggleClass('active');
			$('.js-hmenu-hidde').toggleClass('active');
		});
		// $('.js-ancor').click(function(event) {
		// 	/* Act on the event */
		// 	$(this).mgoto(event);
		// });

		// console.log(viewport);
		// if (viewport.width>1190) {
		// 	var h='calc(100vh - '+$('header').innerHeight()+'px)';
		// 	$('.js-topimg').get(0).style.height=h;
		// }


		if($('.js-courseBase').length){
			var cbase = new Choices('.js-courseBase', {
					shouldSort: false,
				});
			cbase.passedElement.addEventListener('choice', function(event) {
				// do something creative here...
				// console.log(event);
				if(event.detail.choice.value=='other'){
					$(event.target).parents('.js-formFinal').find('.js-otherdata').removeClass('hidden').fadeIn(300, function() {
						$(this).find('input').focus()
					})
				}
			}, false);
		}

		$('.collapse_item').click(function(event) {
			/* Act on the event */
			$(this).toggleClass('active');
		});
		$('.js-uncollapse').click(function(event) {
			/* Act on the event */
			$(this).toggleClass('active');
			if ($(this).hasClass('active')) {
				$('#' + this.dataset.colltarget).children().each(function(index, el) {
					$(this).addClass('active')
				});
			} else {
				$('#' + this.dataset.colltarget).children().each(function(index, el) {
					$(this).removeClass('active');
				});
			}
		});
		var auto = false,
			ap = 1000;
		$('.package_slider').slick({
			autoplay: auto,
			autoplaySpeed: ap,
			arrows: false,
			dots: true,
			swipe: true,
			swipeToSlide: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			responsive: [{
					breakpoint: 944,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						dots: true
					}
				},
				{
					breakpoint: 660,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
		$('.courses_slick').slick({
			autoplay: auto,
			autoplaySpeed: ap,
			arrows: false,
			dots: true,
			swipe: true,
			swipeToSlide: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			responsive: [{
					breakpoint: 944,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						dots: true
					}
				},
				{
					breakpoint: 660,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
		$('.star_holder').mystar({
			click: true
		});
		$('.js_hidde').each(function(index, el) {

			$(this).attr('data-h', $(this).height() + 'px');
			$(this).css('max-height', 0 + 'px');
		});
		$('.js_hidder').click(function(event) {
			/* Act on the event */
			event.preventDefault();
			el = $(this).parents('.js_hidder-parent').find('.js_hidde');
			el.toggleClass('active');
			if (el.hasClass('active')) {
				el.css({
					'max-height': el[0].dataset.h,
				});
			} else {
				el.css({
					'max-height': 0 + 'px',
				});
			}
		});
		$('button.js-calendar-hidden').click(function(event) {
			/* Act on the event */
			$('li.js-calendar-hidden').toggleClass('visible')
		});

		// $('.js-contdown').ptimer('09/13/2017');
		// $('.js-contdown').ptimer('12/12/2017');
	});

	var handler = function() {

		var height_footer = $('footer').innerHeight()
		var height_header = $('header').innerHeight()
		$('.content').css({
			'padding-bottom': height_footer,
			'padding-top': height_header
		});
		//
		//
		// var viewport_wid = viewport().width;
		//
		// if (viewport_wid <= 991) {
		//
		// }

	};
	// !function(e){"use strict";function t(e,t,n){e.addEventListener?e.addEventListener(t,n,!1):e.attachEvent&&e.attachEvent("on"+t,n)}function n(t,n){return e.localStorage&&localStorage[t+"_content"]&&localStorage[t+"_file"]===n}function a(t,a){if(e.localStorage&&e.XMLHttpRequest)n(t,a)?o(localStorage[t+"_content"]):l(t,a);else{var s=r.createElement("link");s.href=a,s.id=t,s.rel="stylesheet",s.type="text/css",r.getElementsByTagName("head")[0].appendChild(s),r.cookie=t}}function l(e,t){var n=new XMLHttpRequest;n.open("GET",t,!0),n.onreadystatechange=function(){4===n.readyState&&200===n.status&&(o(n.responseText),localStorage[e+"_content"]=n.responseText,localStorage[e+"_file"]=t)},n.send()}function o(e){var t=r.createElement("style");t.setAttribute("type","text/css"),r.getElementsByTagName("head")[0].appendChild(t),t.styleSheet?t.styleSheet.cssText=e:t.innerHTML=e}var r=e.document;e.loadCSS=function(e,t,n){var a,l=r.createElement("link");if(t)a=t;else{var o;o=r.querySelectorAll?r.querySelectorAll("style,link[rel=stylesheet],script"):(r.body||r.getElementsByTagName("head")[0]).childNodes,a=o[o.length-1]}var s=r.styleSheets;l.rel="stylesheet",l.href=e,l.media="only x",a.parentNode.insertBefore(l,t?a:a.nextSibling);var c=function(e){for(var t=l.href,n=s.length;n--;)if(s[n].href===t)return e();setTimeout(function(){c(e)})};return l.onloadcssdefined=c,c(function(){l.media=n||"all"}),l},e.loadLocalStorageCSS=function(l,o){n(l,o)||r.cookie.indexOf(l)>-1?a(l,o):t(e,"load",function(){a(l,o)})}}(this);
	// segment code
	// console.log(Date.now());
	var loadfunc=function(){
		!function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement("script");e.type="text/javascript";e.async=!0;e.src=("https:"===document.location.protocol?"https://":"http://")+"cdn.segment.com/analytics.js/v1/"+t+"/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION="4.0.0";
		analytics.load("EXvIntfSLHco2KpNqC23karCFxcgBRnA");
		analytics.page();
		// console.log(Date.now());
		// console.log(analytics);
		}}();
		// loadCSS( "https://fonts.googleapis.com/css?family=Source+Sans+Pro|Ubuntu:300,300i,400,400i,500,500i,700,700i&amp;subset=cyrillic", false, "screen" );
		// loadCSS( "/css/style.css", false, "screen" );
	},
	// firstscr=[
	// 	"../dist/custom.js",
	// 	"//fast.wistia.com/embed/medias/nsn69b29b3.jsonp",
	// 	"//fast.wistia.com/assets/external/E-v1.js",
	// 	"//widget.manychat.com/1337427002993175.js",
	// ],
	initscr=function () {
		var vidDefer = document.querySelectorAll('link');
		for (var i=0; i<vidDefer.length; i++) {
			// if(vidDefer[i].getAttribute('data-src')) {
			// 	vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
			// }
			// if(vidDefer[i].getAttribute('data-href')) {
			// 	vidDefer[i].setAttribute('href',vidDefer[i].getAttribute('data-href'));
			// }
			if(vidDefer[i].getAttribute('media')!=='all') {
				// console.log(vidDefer[i]);
				vidDefer[i].media='all';
			}
		}
	};
	// loadscripts(["../dist/custom.js","//fast.wistia.com/embed/medias/nsn69b29b3.jsonp","//fast.wistia.com/assets/external/E-v1.js"]);
	// document.onload=setTimeout(function(){loadfunc();initscr();}, 2000);
	// document.onload=setTimeout(function(){loadfunc();loadscripts(["//fast.wistia.com/embed/medias/nsn69b29b3.jsonp","//fast.wistia.com/assets/external/E-v1.js"])}, 2000);

	// window.onload = init;
	var mload = function() {
		if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
			$('body').addClass('ios');
		};
		$('body').removeClass('loaded');

		var _dcq = _dcq || [];
		var _dcs = _dcs || {};
		_dcs.account = '5349109';


		setTimeout(function(){
			// loadscripts(['//tag.getdrip.com/5349109.js', '//connect.facebook.net/ru_RU/sdk.js']);
			// loadscripts(['//tag.getdrip.com/5349109.js']);
			loadscripts([{id: 'drip-js', src: "//tag.getdrip.com/5349109.js"}]);
		}, 800);
		// setTimeout(function(){
		// 	loadscripts(['//widget.manychat.com/1337427002993175.js']);
		// }, 800);

		// (function() {
		//   var dc = document.createElement('script');
		//   dc.type = 'text/javascript'; dc.async = true;
		//   dc.src = '//tag.getdrip.com/5349109.js';
		//   var s = document.getElementsByTagName('script')[0];
		//   s.parentNode.insertBefore(dc, s);
		// })();

		// setTimeout(function(){
		// 	if ($('.mcwidget-embed').length) {
		// 		$('.mcwidget-embed>div').get(0).style.setProperty('max-width', '100%', 'important')
		// 	}
		// }, 1000)


		setTimeout(function(){
			// loadscripts(["//fast.wistia.com/embed/medias/nsn69b29b3.jsonp","//fast.wistia.com/assets/external/E-v1.js"]);
			loadfunc();
			initscr();
		}, 300)


		// console.log('mload')
		handler();
	};
	var mresize = function() {
		// console.log('mresize')
		handler();
	};
	var mscroll = function() {
		var top = document.documentElement.scrollTop || document.body.scrollTop;
		if (top + window.innerHeight >= $('.circle').offset().top && !$('.circle').hasClass('loaded')) {
			$('.circle').addClass('loaded')
			$('.circle').each(function(index, el) {
				$(this).circleProgress({
					startAngle: Math.PI / 2,
					thickness: 1,
					value: (Number(this.dataset.cerclevalue) / 5),
					size: 118,
					emptyFill: "rgba(0, 0, 0, 0)",
					fill: {
						color: '#FF6400'
					}
				});
			}).on('circle-animation-progress', function(event, progress, stepValue) {
				$(this).find('.circle_digit').text((stepValue * 5).toFixed(1));
			});
		}
	};
	$(window).bind('load', mload);
	$(window).bind('resize', mresize);
	$(window).bind('scroll', mscroll);
	$(document).on('mousewheel DOMMouseScroll', function(event) {
		$('body,html').stop(true, false);
	});


	// $.prototype.mgoto = function(event) {
	// 	var el = this[0].hash,
	// 		t = 1500;
	// 	console.log($(el));
	// 	if (!el) {
	// 		return false;
	// 	}
	// 	event.preventDefault();
	// 	$('body,html').animate({
	// 		scrollTop: ($(el).offset().top - $('.head').innerHeight())
	// 	}, t);
	// };

	$(document).on('click', '.js-phone', function(event){
		var th=$(this),
			detaild=th.closest('.details_div').find('.js-addinfo'),
			tr='triggered';
		// th.val('+380');
		if (!detaild.hasClass(tr)) {
			detaild.slideDown(300, function() {
				$(this).addClass(tr);
			})
		}
	});
	// $(document).on('submit', '.js-form', function(event){
	// 	event.preventDefault();
	// 	var date = new Date(),
	// 		form=$(this),
	// 		msg=new Object(),
	// 		inputs=form.find('input, select');
	// 	if (form.find('.form-group').hasClass('error')) {
	// 		form.find('.form-group').removeClass('error');
	// 		form.find('.js-errorbox-all').slideUp(300, function() {
	// 			$(this).empty();
	// 		})
	// 	}
	// 	// $(this).find('.js-time').val(date.getDate()+'.'+date.getMonth()+'.'+date.getFullYear());
	// 	// console.log(inputs);
	// 	inputs.each(function(){
	// 		var th=$(this);
	// 		msg[th.attr('name')]=th.val();
	// 	})
	//
	// 	// console.log(msg);
	// 	// url=String($(this).attr('action'));
	//
	// 	// console.log(msg);
	// 	// console.log(JSON.stringify(msg));
	//
	// 	//console.log($.ajaxSettings);
	// 	// $.ajax({
	// 	// 	type: 'post',
	// 	// 	//dataType: 'json',
	// 	// 	data: msg,
	// 	// 	crossDomain: true,
	// 	// 	beforeSend: function (xhr){
	// 	// 		console.log(xhr);
	// 	// 		//xhr.setRequestHeader("X-CSRF-Token", null);
	// 	// 	},
	// 	// 	url: 'https://hooks.zapier.com/hooks/catch/2331074/izi26f/',
	// 	// 	success: function(data){
	// 	// 		console.log(data);
	// 	//
	// 	// 		// $.ajax({
	// 	// 		// 	type: 'POST',
	// 	// 		// 	url: 'php/mail.php',
	// 	// 		// 	data: msg,
	// 	// 		// 	dataType: 'json',
	// 	// 		// 	success: function(data){
	// 	// 		// 		console.log(data);
	// 	// 		// 		if(data.error!==1){
	// 	// 		// 			setCookie('sData', JSON.stringify(data), {path: '/', expires: 600});
	// 	// 		// 			// window.location.href='/thanks.html';
	// 	// 		// 			// $('.js-successbox').show();
	// 	// 		// 			// form.find('.js-errorbox-all.shown').hide();
	// 	// 		// 			// form.find('input,button,textarea').hide();
	// 	// 		// 		} else {
	// 	// 		// 			form.find('.js-errorbox-all').html(data.msg).show(300).addClass('shown');
	// 	// 		// 		}
	// 	// 		// 	}
	// 	// 		// });
	// 	// 	},
	// 	// 	error: function(data){
	// 	// 		console.log(data);
	// 	// 	}
	// 	// });
	// 	msg.w=(typeof document.body.dataset.webinar!=='undefined')?document.body.dataset.webinar:'';
	// 	msg.f='register';
	// 	if (msg.time===0||msg.time==='0') {
	// 		msg.time='first';
	// 	}
	//
	// 	// $.ajax({
	// 	// 	type: 'post',
	// 	// 	dataType: 'json',
	// 	// 	data: msg,
	// 	// 	url: '/php/wj.php',
	// 	// 	success: function(data){
	// 	// 		// form.find('.js-errorbox-all').append('ajax success start<br>').slideDown(300);
	// 	// 		console.log('success: ', data);
	// 	// 		var cdata=JSON.stringify(data);
	// 	// 		console.log('cdata: ', cdata);
	// 	// 		setTimeout(function(){
	// 	// 			// form.find('.js-errorbox-all').append('ajax status check<br>').slideDown(300);
	// 	// 			if (data.status=='error') {
	// 	// 				// form.find('.js-errorbox-all').append('ajax error<br>').slideDown(300);
	// 	// 				form.find('.js-errorbox-all').append(data.msg).slideDown(300, function() {
	// 	// 					$(data.names).each(function(){
	// 	// 						form.find('[name="'+this+'"]').closest('.form-group').addClass('error')
	// 	// 					})
	// 	// 				})
	// 	// 			} else {
	// 	// 				// form.find('.js-errorbox-all').append('ajax ok<br>').slideDown(300);
	// 	// 				data.getWebinar=res.getWebinar;
	// 	// 				// console.log(cdata);
	// 	// 				// setCookie('successReg', cdata, {path: '/', expires: 6000});
	// 	// 				// form.find('.js-errorbox-all').append('pre create cookie<br>').slideDown(300);
	// 	// 				try {
	// 	// 					console.log('first cookie attempt');
	// 	// 					setCookie('successReg', cdata, {path: '/', expires: 6000});
	// 	// 					console.log(getCookie('successReg'));
	// 	// 				} catch (e) {
	// 	// 					console.log(e);
	// 	// 				}
	// 	//
	// 	// 				if (typeof getCookie('successReg')=='undefined') {
	// 	// 					console.log('second cookie attempt');
	// 	// 					createCookie('successReg',cdata,100);
	// 	// 					console.log(getCookie('successReg'));
	// 	// 				}
	// 	//
	// 	// 				setTimeout(function(){
	// 	// 					console.log('check cookie');
	// 	// 					console.log(getCookie('successReg'));
	// 	// 					// form.find('.js-errorbox-all').append('check cookie<br>').slideDown(300);
	// 	// 					// form.find('.js-errorbox-all').append(getCookie('successReg')).slideDown(300);
	// 	// 					if (typeof getCookie('successReg')!=='undefined') {
	// 	// 						// window.location.href+='thanks.html';
	// 	// 						// form.find('.js-errorbox-all').append('cookie intact<br>').slideDown(300);
	// 	// 						var findGet=window.location.href.indexOf('?')
	// 	// 						// form.find('.js-errorbox-all').append('start redirect<br>').slideDown(300);
	// 	// 						if (findGet!=-1) {
	// 	// 							// form.find('.js-errorbox-all').append('redirect to<br>'+window.location.href.substring(0, findGet)+'thanks').slideDown(300);
	// 	// 							window.location.href=window.location.href.substring(0, findGet)+'thanks';
	// 	// 						} else {
	// 	// 							// form.find('.js-errorbox-all').append('redirect to<br>'+window.location.href+'thanks').slideDown(300);
	// 	// 							window.location.href=window.location.href+'thanks';
	// 	// 						}
	// 	// 					}
	// 	// 				}, 1000);
	// 	// 			}
	// 	// 		}, 500);
	// 	// 	},
	// 	// 	error: function(data){
	// 	// 		console.log('error: ', data);
	// 	// 	}
	// 	// })
	//
	// 	// console.log('submit:', msg);
	// 	// console.log(MC);
	//
	//
	// });



	// $(document).on('submit', '.js-formFinal', function(event){
	// 	event.preventDefault();
	// 	$(this).find('.js-oldData').val(getCookie('sData'));
	// 	var form=$(this),
	// 		msg=$(this).serialize();
	// 		// url=String($(this).attr('action'));
	//
	// 	// console.log(msg);
	// 	// console.log(JSON.stringify(msg));
	// 	$.ajax({
	// 		type: 'POST',
	// 		url: '/php/mail.php',
	// 		data: msg,
	// 		dataType: 'json',
	// 		success: function(data){
	// 			// console.log(data);
	// 			if(data.error!==1){
	// 				// window.location.href+='success.html';
	// 				window.location.href+='success';
	// 				// $('.js-successbox').show();
	// 				// form.find('.js-errorbox-all.shown').hide();
	// 				// form.find('input,button,textarea').hide();
	// 			}
	// 		}
	// 	});
	// });




	if ($('.js-addtocal').length>0) {
		!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://plugins.eventable.com/eventable.js";fjs.parentNode.insertBefore(js, fjs);}}(document, "script", "eventable-script");
	}
	//
	// $('[name="user[agreed_to_terms]"]').on('change', function(event) {
	// 	if ($(this).is(':checked')){
	// 		$(this).val(true);
	// 	} else {
	// 		$(this).val(false);
	// 	}
	// 	// console.log($(this).val());
	// });
	// $('.js-customreg').on('submit', function(event){
	// 	event.preventDefault();
	// 	var th=$('.js-customreg'),
	// 		prel=th.find('.js-formpreloader'),
	// 		answ=th.parent().find('.js-answer'),
	// 		inputs=th.find('input'),
	// 		rdata=$('.js-customreg').serialize(),
	// 		check='true',
	// 		pass=inputs.filter('#user_password').val();
	//
	// 	if (answ.hasClass('error')) {
	// 		answ.slideUp(200, function() {
	// 			// console.log('slideup');
	// 			$(this).removeClass('error success').empty();
	// 		})
	// 	}
	// 	setTimeout(function(){
	// 		// console.log(answ);
	// 		// console.log(inputs);
	// 		// console.log(inputs.filter('#user_password'));
	// 		// console.log(rdata);
	// 		if (inputs.filter('#user_name').val()=='') {
	// 			answ.toggleClass('error').html('<p>Вы не ввели свое имя! Представьтесь нам чтобы продолжить регистрацию.</p>').slideToggle(300)
	// 			check = false;
	// 		}
	// 		if (check==='true'&&inputs.filter('#user_email').val()=='') {
	// 			answ.toggleClass('error').html('<p>Вы не указали email! Мы не можем зарегистрировать без действующего email. Потребуется подтверждение!</p>').slideToggle(300)
	// 			check = false;
	// 		}
	// 		if (check==='true'&&(typeof pass == 'undefined'||pass==''||pass.length<=5)) {
	// 			answ.toggleClass('error').html('<p>Ваш пароль не соответствует требованиям! Укажите пароль длиной 6 и более символов латинского алфавита и/или цифр.</p>').slideToggle(300)
	// 			check = false;
	// 		}
	// 		// console.log(inputs.filter('#user_password').val());
	// 		// console.log(inputs.filter('#user_password_confirmation').val());
	// 		if (check==='true'&&(pass!==inputs.filter('#user_password_confirmation').val())) {
	// 			answ.toggleClass('error').append('<p>Пароль, указанный в поле "Пароль" не соотвествует тексту, указанному в поле "Проверка пароля"! Укажите пароль длиной 6 и более символов латинского алфавита и/или цифр и проверьте правильность ввода пароля в оба поля.</p>').slideToggle(300)
	// 			check = false;
	// 		}
	// 		// console.log(inputs.filter('#user_agreed_to_terms').val());
	// 		if (check==='true'&&inputs.filter('#user_agreed_to_terms').val()!=='true') {
	// 			answ.toggleClass('error').append('<p>Вы не приняли наши условия предоставления услуг! Мы не можем Вас зарегистрировать пока Вы не примете наши условия (не прочитаете и не подтвердите согласие со всеми условиями).</p>').slideToggle(300)
	// 			check = false;
	// 		}
	//
	// 		if (check==='true') {
	// 			$.ajax({
	// 				// dataType: 'json',
	// 				beforeSend: function(){
	// 					prel.addClass('active');
	// 				},
	// 				data: rdata,
	// 		        type: 'GET',
	// 				crossDomain: true,
	// 				url: "https://hooks.zapier.com/hooks/catch/2331074/8bqtae/"
	// 			}).always(function(){
	// 				answ.toggleClass('success').append('<p>Вы успешно зарегистрированы! Теперь мы перенаправим Вас на страницу вебинара.</p>').slideToggle(300, function() {
	// 					prel.removeClass('active');
	// 					setTimeout(function(){
	// 						window.location.href='https://heartbeat.education/p/5012';
	// 					}, 2000)
	// 				})
	// 			});
	// 		} else {
	// 			return false;
	// 		}
	// 	}, 400)
	//
	// });

	// }




} ( this, jQuery ));
