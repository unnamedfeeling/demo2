<?php global $post, $options, $tpl, $p, $pmeta;
// test
?>
		</main>
		<header>
			<div class="container">
				<div class="dfc aic">
					<div class="<?=(strpos($post->post_name, 'thanks')!==false)?'col-xs-12 col-sm-11 col-sm-offset-1':'col-xs-6 col-sm-5 col-sm-offset-1 col-md-offset-0 col-md-4 col-lg-3'?>">
						<div class="logo-holder">
							<a class="logo-link" href="//heartbeat.education" target="_blank">
								<div class="logo-large">
									<i class="logo_headicon icon-Logo-Znak"></i>
									<p class="logo-text"><b>HeartBeat</b><br>  education</p>
								</div>
							</a>
						</div>
					</div>
					<?php if (strpos($post->post_name, 'thanks')===false): ?>
					<div class="col-xs-6 col-sm-5 col-md-8 col-lg-9 poss text-right">
						<button class="package_btn hbtn js-callModal" data-mfp-src="#registerForm">Зарегистрироваться</button>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</header>

		<footer id="footer">
			<div class="container ta-center">
				<div class="footer">
					<div class="footer_logo">
						<i class="icon-Logo-Znak"></i>
					</div>
					<ul class="footer_links">
						<li class="footer_item">
							<a href="//heartbeat.education/p/about-us" target="_blank" class="footer_link">
								О нас
							</a>
						</li>
						<li class="footer_item">
							<a href="//heartbeat.education/p/authors" target="_blank" class="footer_link">
								Стать автором
							</a>
						</li>
						<li class="footer_item">
							<a href="//help.heartbeat.education" target="_blank" class="footer_link">
								Поддержка
							</a>
						</li>
					</ul>
					<ul class="footer_links footer_links-light">
						<li class="footer_item">
							<a href="//heartbeat.education/p/terms" target="_blank" class="footer_link footer_link-light">Условия использования</a>
						</li>
						<li class="footer_item">
							<a href="//heartbeat.education/p/privacy" target="_blank" class="footer_link footer_link-light">Политика конфиденциальности</a>
						</li>
						<li class="footer_item">
							<a href="//heartbeat.education/p/intellectual" target="_blank" class="footer_link footer_link-light">Интеллектуальная собственность</a>
						</li>
					</ul>
					<p class="footer_link footer_link-light">Copyright © 2017 - <?=date('Y')?> Heartbeat, Inc.</p>
				</div>
			</div>
		</footer>
		</div>
		<div class="icon-load"></div>
		<?php if (is_page()): ?>
		<div id="registerForm" class="mfp-hide registerForm mfp-medium js-registerForm">
			<div class="modal-content ">
				<div class="modal-header">
					<h4 class="modal-title text-uppercase text-center">Регистрация на событие</h4>
				</div>
				<div class="modal-body">
					<?php if (!empty($pmeta[$p.'regform'][0])): ?>
						<?=do_shortcode('[hf_form slug="'.$pmeta[$p.'regform'][0].'" html_class="modal_form"]')?>
					<?php else: ?>
						<form action="/wp-json/heartweb/v1/webinarData" class="modal-form js-form" onsubmit="if (typeof analytics  != 'undefined') {analytics.identify({Email: this.email.value.toString()}); analytics.track('Вебинар - Регистрация на событие',{'Email': this.email.value.toString()});}" method="post">
							<input type="hidden" name="postId" value="<?=(!empty($post->ID))?$post->ID:null?>">
							<input type="hidden" class="js-reqFields" name="reqFields" value="name,email,date,time">
							<input type="hidden" class="js-webinarType" name="webinarType" value="">
							<input type="hidden" class="js-webinarId" name="webinarId" value="">
							<input type="hidden" class="js-webinarName" name="webinarName" value="">
							<input type="hidden" class="js-countryData" name="countryData" value="">
							<input type="hidden" class="js-phonedata" name="phonedata" value="">
							<div class="details_div">
								<div class="form-group">
									<input type="text" class="form-control js-name" placeholder="Введите ваше имя" name="name" id="name">
									<i class="gd-ico-person-1"></i>
									<span class="asterisk">*</span>
								</div>
								<div class="form-group">
									<input type="email" class="form-control js-email" placeholder="Введите ваш e-mail" name="email" id="email">
									<i class="gd-ico-mail"></i>
									<span class="asterisk">*</span>
								</div>
								<div class="form-group">
									<input type="tel" class="form-control js-phone" name="phone" id="phone" >
									<i class="gd-ico-mail"></i>
								</div>
								<p class="addinfo js-addinfo" style="display:none;margin:-10px auto 20px">Звонить вам не будем, лишь отправим уведомления о вебинаре<br></p>
								<div class="form-group hidden">
									<select class="form-control selector-instance js-dateSel js-selector" name="date" id="date"></select>
								</div>
								<div class="form-group hidden">
									<select class="form-control selector-instance js-timeDd js-selector" name="time" id="time"></select>
								</div>
							</div>
							<button id="register_btn" type="submit" class="btn btn-blue_new btn-huge btn-block btn_register">
								<span id="original-button">
									<span class="title">Зарегистрироваться</span>
								</span>
							</button>
							<div class="errorbox-all js-errorbox-all" style="display:none"></div>
						</form>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php endif;
		wp_footer();
		if(!empty($options['advd']['custom_js'])||!empty($pmeta[$p.'custom_js'][0])){
			echo '<script>'.((!empty($options['advd']['custom_js']))?htmlspecialchars_decode($options['advd']['custom_js']):null).PHP_EOL.((!empty($pmeta[$p.'custom_js'][0]))?$pmeta[$p.'custom_js'][0]:null).'</script>';
		}
		if(!empty($options['advd']['custom_js_tag'])||!empty($pmeta[$p.'custom_js_tag'][0])){
			echo ($options['advd']['custom_js_tag']!==' ') ? str_replace('\"', '"', $options['advd']['custom_js_tag']) : null;
			echo ($pmeta[$p.'custom_js_tag'][0]!==' ') ? str_replace('\"', '"', $pmeta[$p.'custom_js_tag'][0]) : null;
		}
		if(!empty($options['advd']['custom_noscript'])||!empty($pmeta[$p.'custom_noscript'][0])){
			echo ($options['advd']['custom_noscript']!==' ') ? '<noscript>'.str_replace('\\\'', '\'', $options['advd']['custom_noscript']).'</noscript>' : null;
			echo (!empty($pmeta[$p.'custom_noscript'][0])) ? '<noscript>'.str_replace('\\\'', '\'', $pmeta[$p.'custom_noscript'][0]).'</noscript>' : null;
		}
		?>
	</body>
</html>
