<?php get_header();
global $options, $tpl, $pmeta, $p;
?>

<section id="page-content" class="page-content-section">
	<div class="container-fluid">
		<article class="win">
			<h1>404. Это ошибка</h1>
			<p class="author_prof">Это значит, что ссылка, по которой вы перешли ведет на страницу на нашем сайте, которой больше нет или никогда не существовало. Нам очень жаль :(</p>
			<p class="author_prof">Такая ссылка называется "битая" и на любом сайте есть такие ссылки.</p>
			<p class="author_prof">Мы уже знаем об этой проблеме и работаем над ее устранением.</p>
		</article>
	</div>
</section>

<?php get_footer();
