<?php /* Template Name: Регистрация на вебинар */
global $options, $tpl, $pmeta, $p, $design;
switch (true) {
	case (!empty($design)):
		get_header($design);
		break;

	default:
		get_header();
		break;
}


$pmeta=(empty($pmeta))?get_post_meta( $post->ID, '', false ):$pmeta;
$author=$ameta=null;
if (!empty($pmeta[$p.'webauthor'][0])&&$pmeta[$p.'webauthor'][0]!==0&&$pmeta[$p.'webauthor'][0]!=='none') {
	$author=get_post( $pmeta[$p.'webauthor'][0], OBJECT, 'raw' );
	$ameta=get_post_meta( $pmeta[$p.'webauthor'][0], '', false );
}

	if (have_posts()): while (have_posts()) : the_post();

		get_template_part( '/assets/php/blocks/acf/mid2018/block', 'registrationForm' );

		if (!empty($post->post_content)) {
			get_template_part( '/assets/php/blocks/acf/mid2018/block', 'content' );
		}

	endwhile;
	else: ?>
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'heartweb' ); ?></h2>
	</article>
	<?php endif;

switch (true) {
	case (!empty($design)):
		get_footer($design);
		break;

	default:
		get_footer();
		break;
}
