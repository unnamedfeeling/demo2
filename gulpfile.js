// npm install -g gulp gulp-less gulp-error-notifier gulp-rename gulp-concat gulp-minify-css gulp-uglify-es
// npm link gulp gulp-less gulp-error-notifier gulp-rename gulp-concat gulp-minify-css gulp-uglify-es

var gulp = require('gulp'),
	less = require('gulp-less'),
	errorNotifier = require('gulp-error-notifier'),
	// spritesmith = require('gulp.spritesmith'),
	rename = require('gulp-rename'),
	concat = require('gulp-concat'),
	// sourcemaps = require('gulp-sourcemaps'),
	minify = require('gulp-minify-css'),
	uglify = require('gulp-uglify-es').default;

var jsListLate2017 = [
		// './node_modules/jquery/dist/jquery.min.js',
		// './node_modules/jquery-migrate/dist/jquery-migrate.min.js',
		// './node_modules/slick-carousel/slick/slick.min.js',
		'./node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
		'./node_modules/jquery-circle-progress/dist/circle-progress.min.js',
		'./node_modules/flipclock/compiled/flipclock.js',
		// './node_modules/choices.js/assets/scripts/dist/choices.min.js',
		'./node_modules/intl-tel-input/build/js/intlTelInput.min.js',
		// './node_modules/intl-tel-input/build/js/utils.js',
		'./assets/js/helpers.js',
		'./assets/js/essentials.js',
		'./assets/late2017/js/app.js'
	];

gulp.task('less-late2017', function() {
	return gulp.src('./assets/late2017/css/style.less')
	.pipe(errorNotifier())
	.pipe(less())
	.pipe(concat('app.min.css'))
	.pipe(minify())
	.pipe(gulp.dest('./assets/late2017/dist'))
});

gulp.task('scripts-late2017', function() {
	return gulp.src(jsListLate2017)
		.pipe(errorNotifier())
		.pipe(concat('app.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./assets/late2017/dist'));
});

gulp.task('watch-late2017', function() {
	gulp.watch([
		'./assets/late2017/src/style/*/**.less',
		'./assets/late2017/src/style/*/**.css',
		'./assets/late2017/src/style/*.less',
		'./assets/late2017/src/style/*.css'
	], gulp.parallel(['less-late2017']));
	gulp.watch(jsListLate2017, gulp.parallel(['scripts-late2017']));
});

var jsListMid2018 = [
		// './node_modules/jquery/dist/jquery.min.js',
		// './node_modules/jquery-migrate/dist/jquery-migrate.min.js',
		// './node_modules/slick-carousel/slick/slick.min.js',
		'./node_modules/magnific-popup/dist/jquery.magnific-popup.min.js',
		'./node_modules/jquery-circle-progress/dist/circle-progress.min.js',
		'./node_modules/easytimer.js/dist/easytimer.min.js',
		// './node_modules/flipclock/compiled/flipclock.js',
		// './node_modules/choices.js/assets/scripts/dist/choices.min.js',
		'./node_modules/intl-tel-input/build/js/intlTelInput.min.js',
		// './node_modules/intl-tel-input/build/js/utils.js',
		'./assets/js/helpers.js',
		'./assets/js/essentials.js',
		'./assets/mid2018/src/js/custom.js'
	];

gulp.task('less-mid2018', function() {
	return gulp.src('./assets/mid2018/src/style/style.less')
	.pipe(errorNotifier())
	.pipe(less())
	.pipe(concat('app.min.css'))
	.pipe(minify())
	.pipe(gulp.dest('./assets/mid2018/dist'))
});

gulp.task('scripts-mid2018', function() {
	return gulp.src(jsListMid2018)
		.pipe(errorNotifier())
		.pipe(concat('app.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./assets/mid2018/dist'));
});

gulp.task('watch-mid2018', function() {
	gulp.watch([
		'./assets/mid2018/src/style/*/**.less',
		'./assets/mid2018/src/style/*/**.css',
		'./assets/mid2018/src/style/*.less',
		'./assets/mid2018/src/style/*.css'
	], gulp.parallel(['less-mid2018']));
	gulp.watch(jsListMid2018, gulp.parallel(['scripts-mid2018']));
});

gulp.task('default', gulp.parallel(['watch-late2017', 'watch-mid2018']));
