<?php  global $post, $options, $tpl, $p, $pmeta, $design;
$tpl=$options['tpld'];
$p=$options['prfx'];

$bodyData='';
switch (!empty($pmeta['heartweb_webinarPlatform'][0])) {
	case true:
		$platform=$pmeta['heartweb_webinarPlatform'][0];
		$bodyData=' data-type="'.$platform.'" data-webid="'.$pmeta['heartweb_webid-'.$platform][0].'"';
		break;

	default:
		$bodyData=' data-type="undefined" data-webid="undefined"';
		break;
}
$bodyData.=' data-pid="'.$post->ID.'" data-design="'.$design.'"';
?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title('', true); ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="apple-touch-icon" sizes="57x57" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/apple-icon-57x57.png?v=1">
		<link rel="apple-touch-icon" sizes="60x60" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/apple-icon-60x60.png?v=1">
		<link rel="apple-touch-icon" sizes="72x72" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/apple-icon-72x72.png?v=1">
		<link rel="apple-touch-icon" sizes="76x76" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/apple-icon-76x76.png?v=1">
		<link rel="apple-touch-icon" sizes="114x114" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/apple-icon-114x114.png?v=1">
		<link rel="apple-touch-icon" sizes="120x120" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/apple-icon-120x120.png?v=1">
		<link rel="apple-touch-icon" sizes="144x144" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/apple-icon-144x144.png?v=1">
		<link rel="apple-touch-icon" sizes="152x152" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/apple-icon-152x152.png?v=1">
		<link rel="apple-touch-icon" sizes="180x180" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/apple-icon-180x180.png?v=1">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?=$tpl?>/assets/<?=$design?>/img/favicon/android-icon-192x192.png?v=1">
		<link rel="icon" type="image/png" sizes="32x32" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/favicon-32x32.png?v=1">
		<link rel="icon" type="image/png" sizes="96x96" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/favicon-96x96.png?v=1">
		<link rel="icon" type="image/png" sizes="16x16" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/favicon-16x16.png?v=1">
		<link rel="manifest" href="<?=$tpl?>/assets/<?=$design?>/img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?=$tpl?>/assets/<?=$design?>/img/favicon/ms-icon-144x144.png?v=1">
		<meta name="theme-color" content="#FF6400">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?>" href="<?php bloginfo('rss2_url'); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
		<meta name="description" content="<?php bloginfo('description'); ?>">
		<?php wp_head();
		if(!empty($options['advd']['custom_css'])){
			echo ($options['advd']['custom_css']!==' ') ? '<style>'.htmlspecialchars_decode($options['advd']['custom_css']).'</style>' : null;
		}
		?>
	</head>
	<body <?php body_class(); ?><?=$bodyData?>>
		<div class="main-wrapper">
			<main class="content content-full">
