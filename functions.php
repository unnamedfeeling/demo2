<?php
/**
 * Author: Todd Motto | @toddmotto
 * URL: html5blank.com | @html5blank
 * Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
    External Modules/Files
\*------------------------------------*/

// // CMB2
// if ( file_exists( dirname( __FILE__ ) . '/assets/php/cmb2/init.php' ) ) {
//   require_once dirname( __FILE__ ) . '/assets/php/cmb2/init.php';
// } elseif ( file_exists(  dirname( __FILE__ ) . '/assets/php/CMB2/init.php' ) ) {
//   require_once dirname( __FILE__ ) . '/assets/php/CMB2/init.php';
// }
// // CMB2 attached posts
// if ( file_exists( dirname( __FILE__ ) . '/assets/php/cmb2-attached-posts/cmb2-attached-posts-field.php' ) ) {
//   require_once dirname( __FILE__ ) . '/assets/php/cmb2-attached-posts/cmb2-attached-posts-field.php';
// }
//
// // CMB2 attached posts ajax
// if ( file_exists( dirname( __FILE__ ) . '/assets/php/cmb2-field-post-search-ajax/cmb-field-post-search-ajax.php' ) ) {
//   require_once dirname( __FILE__ ) . '/assets/php/cmb2-field-post-search-ajax/cmb-field-post-search-ajax.php';
// }

use Heartweb\Webinar;

// theme composer autoloader require
if ( file_exists( dirname( __FILE__ ) . '/assets/php/extends/vendor/autoload.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/extends/vendor/autoload.php';
}

// metaboxes include
if ( file_exists( dirname( __FILE__ ) . '/assets/php/metaboxes.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/metaboxes.php';
}

// tabbed admin menu include
if ( file_exists( dirname( __FILE__ ) . '/assets/php/tabbed-admin-menu.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/tabbed-admin-menu.php';
}

// registration code include
if ( file_exists( dirname( __FILE__ ) . '/assets/php/registrationFunctions.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/registrationFunctions.php';
}

// menu code include
if ( file_exists( dirname( __FILE__ ) . '/assets/php/menuFunctions.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/menuFunctions.php';
}

// shortcode code include
if ( file_exists( dirname( __FILE__ ) . '/assets/php/shortcodeFunctions.php' ) ) {
  require_once dirname( __FILE__ ) . '/assets/php/shortcodeFunctions.php';
}

/*------------------------------------*\
    Theme Support
\*------------------------------------*/

if (!isset($content_width)){
    $content_width = 900;
}

if (function_exists('add_theme_support')){

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('authorImg', 150, 150, true);

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
    'default-color' => 'FFF',
    'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
    'default-image'          => get_template_directory_uri() . '/img/headers/default.jpg',
    'header-text'            => false,
    'default-text-color'     => '000',
    'width'                  => 1000,
    'height'                 => 198,
    'random-default'         => false,
    'wp-head-callback'       => $wphead_cb,
    'admin-head-callback'    => $adminhead_cb,
    'admin-preview-callback' => $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Enable HTML5 support
    add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));

    // Localisation Support
    load_theme_textdomain('heartweb', get_template_directory() . '/assets/languages');

    add_theme_support( 'woocommerce' );
}

/*------------------------------------*\
    Functions
\*------------------------------------*/

// Global options Define
function generic_globals(){
	global $options;
	$options=[
		'prfx'=>'heartweb_',
		'tpld'=>get_template_directory_uri(),
		'brnc'=>get_option('generic_options'),
		'gnrl'=>get_option('general_options'),
		'socl'=>get_option('social_options'),
		'advd'=>get_option('advanced_options'),
		'translate'=>[
			"am" => _x("am", 'global', 'heartweb'),
			"pm" => _x("pm", 'global', 'heartweb'),
			"AM" => _x("AM", 'global', 'heartweb'),
			"PM" => _x("PM", 'global', 'heartweb'),
			"Monday" => _x("Monday", 'global', 'heartweb'),
			"Mon" => _x("Mon", 'global', 'heartweb'),
			"Tuesday" => _x("Tuesday", 'global', 'heartweb'),
			"Tue" => _x("Tue", 'global', 'heartweb'),
			"Wednesday" => _x("Wednesday", 'global', 'heartweb'),
			"Wed" => _x("Wed", 'global', 'heartweb'),
			"Thursday" => _x("Thursday", 'global', 'heartweb'),
			"Thu" => _x("Thu", 'global', 'heartweb'),
			"Friday" => _x("Friday", 'global', 'heartweb'),
			"Fri" => _x("Fri", 'global', 'heartweb'),
			"Saturday" => _x("Saturday", 'global', 'heartweb'),
			"Sat" => _x("Sat", 'global', 'heartweb'),
			"Sunday" => _x("Sunday", 'global', 'heartweb'),
			"Sun" => _x("Sun", 'global', 'heartweb'),
			"January" => _x("January", 'global', 'heartweb'),
			"Jan" => _x("Jan", 'global', 'heartweb'),
			"February" => _x("February", 'global', 'heartweb'),
			"Feb" => _x("Feb", 'global', 'heartweb'),
			"March" => _x("March", 'global', 'heartweb'),
			"Mar" => _x("Mar", 'global', 'heartweb'),
			"April" => _x("April", 'global', 'heartweb'),
			"Apr" => _x("Apr", 'global', 'heartweb'),
			"May" => _x("May", 'global', 'heartweb'),
			"June" => _x("June", 'global', 'heartweb'),
			"Jun" => _x("Jun", 'global', 'heartweb'),
			"July" => _x("July", 'global', 'heartweb'),
			"Jul" => _x("Jul", 'global', 'heartweb'),
			"August" => _x("August", 'global', 'heartweb'),
			"Aug" => _x("Aug", 'global', 'heartweb'),
			"September" => _x("September", 'global', 'heartweb'),
			"Sep" => _x("Sep", 'global', 'heartweb'),
			"October" => _x("October", 'global', 'heartweb'),
			"Oct" => _x("Oct", 'global', 'heartweb'),
			"November" => _x("November", 'global', 'heartweb'),
			"Nov" => _x("Nov", 'global', 'heartweb'),
			"December" => _x("December", 'global', 'heartweb'),
			"Dec" => _x("Dec", 'global', 'heartweb'),
			"st" => _x("st", 'global', 'heartweb'),
			"nd" => _x("nd", 'global', 'heartweb'),
			"rd" => _x("rd", 'global', 'heartweb'),
			"th" => _x("th", 'global', 'heartweb')
		],
		'translatejs'=>[
      "am" => _x("am", 'frontend', 'heartweb'),
      "pm" => _x("pm", 'frontend', 'heartweb'),
      "AM" => _x("AM", 'frontend', 'heartweb'),
      "PM" => _x("PM", 'frontend', 'heartweb'),
      "Monday" => _x("Monday", 'frontend', 'heartweb'),
      "Mon" => _x("Mon", 'frontend', 'heartweb'),
      "Tuesday" => _x("Tuesday", 'frontend', 'heartweb'),
      "Tue" => _x("Tue", 'frontend', 'heartweb'),
      "Wednesday" => _x("Wednesday", 'frontend', 'heartweb'),
      "Wed" => _x("Wed", 'frontend', 'heartweb'),
      "Thursday" => _x("Thursday", 'frontend', 'heartweb'),
      "Thu" => _x("Thu", 'frontend', 'heartweb'),
      "Friday" => _x("Friday", 'frontend', 'heartweb'),
      "Fri" => _x("Fri", 'frontend', 'heartweb'),
      "Saturday" => _x("Saturday", 'frontend', 'heartweb'),
      "Sat" => _x("Sat", 'frontend', 'heartweb'),
      "Sunday" => _x("Sunday", 'frontend', 'heartweb'),
      "Sun" => _x("Sun", 'frontend', 'heartweb'),
      "January" => _x("January", 'frontend', 'heartweb'),
      "Jan" => _x("Jan", 'frontend', 'heartweb'),
      "February" => _x("February", 'frontend', 'heartweb'),
      "Feb" => _x("Feb", 'frontend', 'heartweb'),
      "March" => _x("March", 'frontend', 'heartweb'),
      "Mar" => _x("Mar", 'frontend', 'heartweb'),
      "April" => _x("April", 'frontend', 'heartweb'),
      "Apr" => _x("Apr", 'frontend', 'heartweb'),
      "May" => _x("May", 'frontend', 'heartweb'),
      "June" => _x("June", 'frontend', 'heartweb'),
      "Jun" => _x("Jun", 'frontend', 'heartweb'),
      "July" => _x("July", 'frontend', 'heartweb'),
      "Jul" => _x("Jul", 'frontend', 'heartweb'),
      "August" => _x("August", 'frontend', 'heartweb'),
      "Aug" => _x("Aug", 'frontend', 'heartweb'),
      "September" => _x("September", 'frontend', 'heartweb'),
      "Sep" => _x("Sep", 'frontend', 'heartweb'),
      "October" => _x("October", 'frontend', 'heartweb'),
      "Oct" => _x("Oct", 'frontend', 'heartweb'),
      "November" => _x("November", 'frontend', 'heartweb'),
      "Nov" => _x("Nov", 'frontend', 'heartweb'),
      "December" => _x("December", 'frontend', 'heartweb'),
      "Dec" => _x("Dec", 'frontend', 'heartweb'),
      "st" => _x("st", 'frontend', 'heartweb'),
      "nd" => _x("nd", 'frontend', 'heartweb'),
      "rd" => _x("rd", 'frontend', 'heartweb'),
      "th" => _x("th", 'frontend', 'heartweb')
		]
	];
	return $options;
}

function generic_postmeta(){
	global $post, $pmeta, $design;
	$pmeta=null;
	$design='mid2018';
	if (is_page()||is_single()) {
		$pmeta=get_post_meta( $post->ID, '', false );
		$design=(!empty($pmeta['heartweb_design'][0]))?mb_strtolower(preg_replace('/[\s-]/m', '', $pmeta['heartweb_design'][0])):$design;
	}
}

// Load HTML5 Blank scripts (header.php)
function generic_header_scripts(){
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
		wp_deregister_script( 'jquery' );
		wp_deregister_script( 'jquery-migrate' );
		wp_enqueue_script('jquery', '//cdn.jsdelivr.net/combine/npm/jquery@3.3.1,npm/jquery-migrate@3.0.1,npm/blazy@1.8.2,npm/magnific-popup@1.1.0,npm/slick-carousel@1.8.1,npm/intl-tel-input@12.1.15/build/js/utils.min.js,npm/choices.js@3.0.4', array(), '3.3.1');
    }
}

// Load HTML5 Blank conditional scripts
function generic_conditional_scripts(){
    if (is_page('pagenamehere')) {
        // Conditional script(s)
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0');
        wp_enqueue_script('scriptname');
    }
}

// Load HTML5 Blank styles
function generic_styles(){
	global $pmeta, $design;

	wp_enqueue_style('gfonts', '//fonts.googleapis.com/css?family=Lato:400,700|Open+Sans:300,400,700|Playfair+Display|Roboto:400,700|Lobster|Roboto+Condensed:400,700|Ubuntu|Source+Sans+Pro&subset=cyrillic,latin,cyrillic-ext', array(), '1.0', 'all');
	wp_enqueue_style('mainstyle', get_template_directory_uri().'/assets/'.$design.'/dist/app.min.css', array(), '1.0', 'all');

}

// footer scripts
function generic_footer_scripts(){
	global $pmeta, $design, $options;

	wp_deregister_script( 'wp-embed' );
	// wp_enqueue_script('essentials', get_template_directory_uri() . '/assets/js/essentials.js', array(), '1.0.0');
	wp_enqueue_script('genericscripts-min', get_template_directory_uri() . '/assets/'.$design.'/dist/app.min.js', array(), '1.0.0');

	$userLoginArr=[];
	if (is_user_logged_in()) {
		$userData=wp_get_current_user()->data;
		$userLoginArr=[
			'user'=>wp_get_current_user(),
			'id'=>$userData->ID,
			'email'=>$userData->user_email,
			'name'=>$userData->display_name
		];
		wp_localize_script(
			'osetin-functions',
			'heartblog_data',
			$userLoginArr
		);
	}
	// wp_enqueue_script('footer-scripts', '//cdn.jsdelivr.net/g/jquery.magnific-popup@1.0.0,blazy@1.8.2', array('jquery'), '1.0');
	// wp_enqueue_script('slickslider', get_template_directory_uri() . '/assets/js/libs/slick/slick/slick.min.js', array('jquery'), '1.6.1');
	// wp_enqueue_script('genericscripts-min', get_template_directory_uri() . '/assets/dist/app.min.js', array(), '1.0.0');
	wp_localize_script( 'genericscripts-min', 'ajax_func', array(
		'ajax_url' => admin_url( 'admin-ajax.php' ),
		'tpld' => get_template_directory_uri(),
		'datestrings' => $options['translatejs'],
	));
}

//cleanup version tag
function remove_cssjs_ver( $src ) {
	$src = remove_query_arg( array('v', 'ver', 'rev', 'id'), $src );
	return $src;
}

// remove emoji
function disable_wp_emojicons() {
  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  // add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = ''){
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var){
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist){
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes){
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// Remove the width and height attributes from inserted images
function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}


// If Dynamic Sidebar Exists
// if (function_exists('register_sidebar')){
//     // Define Sidebar Widget Area 1
//     register_sidebar(array(
//         'name' => __('Widget Area 1', 'heartweb'),
//         'description' => __('Description for this widget-area...', 'heartweb'),
//         'id' => 'widget-area-1',
//         'before_widget' => '<div id="%1$s" class="%2$s">',
//         'after_widget' => '</div>',
//         'before_title' => '<h3>',
//         'after_title' => '</h3>'
//     ));
//
//     // Define Sidebar Widget Area 2
//     register_sidebar(array(
//         'name' => __('Widget Area 2', 'heartweb'),
//         'description' => __('Description for this widget-area...', 'heartweb'),
//         'id' => 'widget-area-2',
//         'before_widget' => '<div id="%1$s" class="%2$s">',
//         'after_widget' => '</div>',
//         'before_title' => '<h3>',
//         'after_title' => '</h3>'
//     ));
// }

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style(){
    global $wp_widget_factory;

    if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
        remove_action('wp_head', array(
            $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
            'recent_comments_style'
        ));
    }
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination(){
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// set custom number for 'posts_per_page' per posttype
function posts_per_page_func( $query ) {
	if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'project' ) ) {
		$query->set( 'posts_per_page', '100' );
	}
	if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'post' ) ) {
	// if ( !is_admin() && $query->is_main_query() && is_page( 'novosti' ) ) {
		// $query->set( 'posts_per_page', '-1' );
		$query->set( 'orderby', 'date' );
		$query->set( 'order', 'asc' );
	}
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length){
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = ''){
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function generic_blank_view_article($more){
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'heartweb') . '</a>';
}

// Remove Admin bar
function remove_admin_bar(){
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function generic_style_remove($tag){
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html ){
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults){
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments(){
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth){
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);

    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
    <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
    <?php endif; ?>
    <div class="comment-author vcard">
    <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['avatar_size'] ); ?>
    <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
    </div>
<?php if ($comment->comment_approved == '0') : ?>
    <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
    <br />
<?php endif; ?>

    <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
        <?php
            printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
        ?>
    </div>

    <?php comment_text() ?>

    <div class="reply">
    <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </div>
    <?php if ( 'div' != $args['style'] ) : ?>
    </div>
    <?php endif; ?>
<?php }

// likes function
function generic_news_like() {
	// global $post;
	$id=$_POST['params']['like_id'];
	$liked=0;
	$exp=time()+60*60*24*30;
	// if($_COOKIE['liked']!==$id){
		// exit(json_encode($_POST));
	    $love = get_post_meta( $id, 'generic_post_likes', true );
	    $love++;
	    if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
	        update_post_meta( $id, 'generic_post_likes', $love );
	        echo $love;
			$liked=1;
	    }
		// setcookie('liked', $id, $exp);
	// }
	$response=[
		'id' => $id,
		'time' => $exp,
		'liked' => $liked
	];
    die(json_encode($response));
}

// ajax get posts for main page
function generic_ajax_posts(){
	global $options, $tpl;
	$tpl=$options['tpld'];

	$params=$_POST;
	// die(json_encode( $params ));
	$offset=$params['params']['p']*((!empty($params['params']['q'])) ? $params['params']['q'] : 4);
	$perpage=(!empty($params['params']['ppg'])) ? $params['params']['ppg'] : 4;
	$post_type=$params['params']['pt'];
	$catid=(!empty($params['params']['cat']))?$params['params']['cat']:null;
	$excl=(!empty($params['params']['e']))?explode(',', $params['params']['e']):array();
	$args=array(
		'post_type'=>$post_type,
		'posts_per_page'=>$perpage,
		'offset'=>(empty($excl))?$offset:0,
		'post__not_in'=> $excl
	);
	if(!empty($catid)){
		$args['tax_query']=array(
			array(
				'taxonomy'=>'product_cat',
				'terms'=>array($catid)
			)
		);
	}
	$posts=new WP_Query($args);
	ob_start();
	if (!empty($excl)) {
		$ret_ids='';
	}
	if ($posts->have_posts()): while ($posts->have_posts()) : $posts->the_post();
		$ret_ids.=(!empty($excl))?get_the_ID().',':null;
		get_template_part( 'assets/php/parts/loop', 'course' );
	endwhile;
	else : ?>
		<div class="noposts grid-item design fullwidth" style="width:100%;font-size:2rem;font-weight:bold;"><?=(empty($offset)||$offset==1)?__( 'No swimsuits!', 'heartweb' ):__( 'No more swimsuits!', 'heartweb' )?></div>
	<?php endif;
	// die(json_encode($args));
	$cont=ob_get_clean();
	// $cont=str_replace('data-src', 'src', $cont);
	// $response=[
	// 	'offset' => $offset,
	// 	'total_posts' => wp_count_posts('post'),
	// 	'content' => $cont
	// ];
	if (!empty($excl)) {
		$response['pids']=$ret_ids;
	}
	$response['offset']=$offset;
	$response['total_posts']=wp_count_posts($post_type)->publish;
	$response['content']=$cont;
	// $response['params']=$params;
	die(json_encode($response));
}

// ajax get posts for main page
function generic_ajax_filterPosts(){
	// global $post;
	// wp_reset_query();
	$params=$_POST;
	$post_type=$params['params']['pt'];
	// $catid=(!empty($params['params']['cat']))?$params['params']['cat']:$params['params']['c'];
	// $tagid=(!empty($params['params']['tag']))?$params['params']['tag']:$params['params']['t'];
	$catid=(empty($params['params']['c']))?null:(int) $params['params']['c'];
	$tagid=(empty($params['params']['t']))?null:(int) $params['params']['t'];
	$args=array(
		'post_type'=>$post_type,
		'posts_per_page'=>12,
		'post_status'=>'publish',
		// 'offset'=>0
	);
	if(!empty($catid)){
		$response['tax_type']='cat';
		$args['tax_query']=array(
			array(
				'taxonomy'=>'product_cat',
				'field' => 'id',
				'terms'=>array((int) $catid)
			)
		);
	}
	if(!empty($tagid)){
		$response['tax_type']='tag';
		$args['tax_query']=array(
			array(
				'taxonomy'=>'product_tag',
				'field' => 'id',
				'terms'=>(int) $tagid
			)
		);
		// $args['product_tag']=$tagid;
	}
	$posts=new WP_Query($args);
	ob_start();
	if ($posts->have_posts()): while ($posts->have_posts()) : $posts->the_post();
		get_template_part('loop', 'swim_grid-gen');
	endwhile;
	else : ?>
		<div class="noposts grid-item design fullwidth" style="width:100%;font-size:2rem;font-weight:bold;"><?=(empty($offset)||$offset==1)?__( 'No swimsuits!', 'heartweb' ):__( 'No more swimsuits!', 'heartweb' )?></div>
	<?php endif;
	// die(json_encode($args));
	$cont=ob_get_clean();
	// $cont=str_replace('data-src', 'src', $cont);
	// $response=[
	// 	'offset' => $offset,
	// 	'total_posts' => wp_count_posts('post'),
	// 	'content' => $cont
	// ];
	$response['offset']=$offset;
	$response['total_posts']=wp_count_posts($post_type)->publish;
	$response['content']=$cont;
	// $response['query']=$posts;
	$response['args']=$args;
	$response['post']=$params;
	$response['type']='generic_ajax_filterPosts';
	die(json_encode($response));
}

// ajax get cart content
function generic_ajax_cart(){
	global $woocommerce;
	ob_start();
	echo WC()->cart->get_cart_contents_count();
	$response['content']=ob_get_clean();
	die(json_encode($response));
}

// callback for diltering html tags in admin input
function unfilter_html_tags( $value, $field_args, $field ) {
	/*
	 * Do your custom sanitization.
	 * strip_tags can allow whitelisted tags
	 * http://php.net/manual/en/function.strip-tags.php
	 */
	$value = strip_tags( $value, '<p><a><br><br/><span><b><i>' );

	return $value;
}

// callback for diltering all html tags in admin input
function unfilter_all( $value, $field_args, $field ) {
	/*
	 * Do your custom sanitization.
	 * strip_tags can allow whitelisted tags
	 * http://php.net/manual/en/function.strip-tags.php
	 */
	// $value = strip_tags( $value, '<p><a><br><br/><span><iframe><div>' );
	$value=str_replace('"', '\'', $value);

	return $value;
}
// check if this $post->ID has children
function has_children($pid) {
	$children = get_children( array(
		'post_parent' => $pid,
		'post_type'   => 'any',
		'numberposts' => -1,
		'post_status' => 'publish'
	) );
    if( count( $children ) > 0 ) {
        return true;
    } else {
        return false;
    }
}

// unwrap wpcf7 fields
// add_filter('wpcf7_form_elements', function($content) {
//     $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
//     return $content;
// });

// search only by title
function title_like_posts_where( $where, $wp_query ) {
    global $wpdb;
    if ( $post_title_like = $wp_query->get( 'post_title_like' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'' . esc_sql( $wpdb->esc_like( $post_title_like ) ) . '%\'';
    }
    return $where;
}

function add_my_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
          case 'RUB': $currency_symbol = 'руб'; break;
     }
     return $currency_symbol;
}


/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
function generic_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'generic_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}

	/*
	 * Nonce verification
	 */
	if ( !isset( $_GET['duplicate_nonce'] ) || !wp_verify_nonce( $_GET['duplicate_nonce'], basename( __FILE__ ) ) )
		return;
 	/*
	 * get current paged
	 */
	$paged = (isset($_GET['paged']) ? absint( $_GET['paged'] ) : absint( $_POST['paged'] ) ); 	/*
	 * get current post type (for flexibility)
	 */
	$post_type=((!empty($_GET['post_type'])||!empty($_POST['post_type']))?(isset($_GET['post_type']) ? 'post_type='.$_GET['post_type'].'&' : 'post_type='.$_POST['post_type'].'&' ):null);
	/*
	 * get the original post id
	 */
	$post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
	 /*
	 * and all the original post data then
	 */
	$post = get_post( $post_id );

	/*
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;

	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset( $post ) && $post != null) {

		/*
		 * new post data array
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);

		/*
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );

		/*
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}

		/*
		 * duplicate all post meta just in two SQL queries
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		// $post_meta_infos = get_post_meta($post_id, '', false);
		// print_r($post_meta_infos);
		if (count($post_meta_infos[0])!=0) {
			// $sql_query_sel=array();
			// $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			// foreach ($post_meta_infos as $meta_info) {
			foreach ($post_meta_infos as $meta_data) {
				$meta_key = $meta_data->meta_key;
				if( $meta_key == '_wp_old_slug' ) continue;
				$meta_value = addslashes($meta_data->meta_value);
				// $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
				update_post_meta( $new_post_id, $meta_key, $meta_value );
			}
			// $sql_query.= implode(" UNION ALL ", $sql_query_sel);
			// $wpdb->query($sql_query);
		}


		/*
		 * finally, redirect to the edit post screen for the new draft
		 */
		// wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		wp_redirect( admin_url( 'edit.php?'.$post_type.'paged='.$paged ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}


/*
 * Add the duplicate link to action list for post_row_actions
 */
function generic_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		 /*
		 * get current paged
		 */
		$paged = ((!empty($_GET['paged'])||!empty($_POST['paged']))?(isset($_GET['paged']) ? absint( $_GET['paged'] ) : absint( $_POST['paged'] ) ):'1');
		$post_type=((!empty($_GET['post_type'])||!empty($_POST['post_type']))?(isset($_GET['post_type']) ? 'post_type='.$_GET['post_type'].'&' : 'post_type='.$_POST['post_type'].'&' ):null);

		$actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?'.$post_type.'paged='.$paged.'&action=generic_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce' ) . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
	}
	return $actions;
}

// function wds_setup_theme() {
// 	global $tpl;
//     add_theme_support( 'wds-simple-page-builder' );
//     wds_page_builder_theme_support( array(
//         'hide_options'    => false,          // set to true to hide them completely
//         'parts_dir'       => $tpl.'assets/php/blocks',       // directory the template parts are saved in
//         'parts_prefix'    => 'part',              // the template part prefix, e.g. part-template.php
//         'use_wrap'        => false,                // Whether to use a wrapper container. 'on' is TRUE
//         'container'       => 'section',           // HTML container for Page Builder template parts
//         'container_class' => 'pagebuilder-part',  // can use multiple classes, separated by a space
//         'post_types'      => array( 'page', ),    // add any other supported post types here
//     ) );
// }

/**
 * Display different options depending on post category
 * @param  object $field      Current field object
 * @return array              Array of field options
 */
function generic_authors_dropdown_cb( $field ) {
	$authors=get_posts( [
		'post_type'=>'author',
		'posts_per_page'=>-1
	] );
	$options=[];
	if (count($authors)>0) {
		foreach ($authors as $p) {
			$options[$p->ID]=$p->post_title;
		}
	} else {
		$options[0]='No authors!';
	}
	return $options;
}

function generic_add_routes(){
	RoutingWP\add_frontend_route('^thanks/([^/]+)$', function($matches) {
		$page = get_page_by_path( $matches[1] );
		$pages = get_posts([
			'post_type' => 'page',
		    'post_parent' => $page->ID,
		    'number'   => 1,
		]);
		if ( $pages ) {
		    $thanksPage = $pages[0];
		}
	    return [
	        'page_id' => $thanksPage->ID,
	    ];
	});
}

// extend HTML_Forms
// function sp_extend_HTML_Forms(){
// 	if (is_plugin_active('html-forms/html-forms.php')) {
// 		//get the base class
// 		if(!class_exists('HTML_Forms')) {
// 			require_once plugin_dir_path( 'html-forms/html-forms.php' ) . '/src/Actions/Action.php';
// 		}
// 		include_once('assets/php/extends/WebinarRegistration.php');
//
// 		// hook actions
// 		$webinar_action = new Actions\Webinar();
// 		$webinar_action->hook();
//
// 	}
// }

function heartweb_contentFilter( $content ) {
	global $design;
	switch ($design) {
		case 'late2017':
			$content=preg_replace('/<p([^>]+)?>/', '<p$1 class="win_p">', $content);
			break;

		default:
			/* $content=preg_replace('/<p([^>]+)?>/', '<p$1 class="win_p">', $content);*/
			break;
	}

	return $content;
}


/**
 * Get human readable time difference between 2 dates
 *
 * Return difference between 2 dates in year, month, hour, minute or second
 * The $precision caps the number of time units used: for instance if
 * $time1 - $time2 = 3 days, 4 hours, 12 minutes, 5 seconds
 * - with precision = 1 : 3 days
 * - with precision = 2 : 3 days, 4 hours
 * - with precision = 3 : 3 days, 4 hours, 12 minutes
 *
 * From: http://www.if-not-true-then-false.com/2010/php-calculate-real-differences-between-two-dates-or-timestamps/
 *
 * @param mixed $time1 a time (string or timestamp)
 * @param mixed $time2 a time (string or timestamp)
 * @param integer $precision Optional precision
 * @return string time difference
 */
function get_date_diff( $time1, $time2, $precision = 2 ) {
	// $strings=[
	// 	'year',
	// 	'month',
	// 	'day',
	// 	'hour',
	// 	'minute',
	// 	'second'
	// ];
	// If not numeric then convert timestamps
	if( !is_int( $time1 ) ) {
		$time1 = strtotime( $time1 );
	}
	if( !is_int( $time2 ) ) {
		$time2 = strtotime( $time2 );
	}
	// If time1 > time2 then swap the 2 values
	if( $time1 > $time2 ) {
		list( $time1, $time2 ) = array( $time2, $time1 );
	}
	// Set up intervals and diffs arrays
	$intervals = [ 'year', 'month', 'day', 'hour', 'minute', 'second' ];
	$diffs = [];
	foreach( $intervals as $interval ) {
		// Create temp time from time1 and interval
		$ttime = strtotime( '+1 ' . $interval, $time1 );
		// Set initial values
		$add = 1;
		$looped = 0;
		// Loop until temp time is smaller than time2
		while ( $time2 >= $ttime ) {
			// Create new temp time from time1 and interval
			$add++;
			$ttime = strtotime( "+" . $add . " " . $interval, $time1 );
			$looped++;
		}
		$time1 = strtotime( "+" . $looped . " " . $interval, $time1 );
		$diffs[ $interval ] = $looped;
	}
	$count = 0;
	$times = [];
	foreach( $diffs as $interval => $value ) {
		// Break if we have needed precission
		if( $count >= $precision ) {
			break;
		}
		// Add value and interval if value is bigger than 0
		if( $value > 0 ) {
			if( $value != 1 ){
				$interval .= "s";
			}
			// Add value and interval to times array
			// $times[] = $value . " " . $interval;

			switch ($interval) {
				case 'months':
					$times[$interval] = sprintf( _n( '%s month', '%s months', $value, 'heartweb' ), $value );
					break;
				case 'days':
					$times[$interval] = sprintf( _n( '%s day', '%s days', $value, 'heartweb' ), $value );
					break;
				case 'hours':
					$times[$interval] = sprintf( _n( '%s hour', '%s hours', $value, 'heartweb' ), $value );
					break;
				case 'minutes':
					$times[$interval] = sprintf( _n( '%s minute', '%s minutes', $value, 'heartweb' ), $value );
					break;
				case 'seconds':
					$times[$interval] = sprintf( _n( '%s second', '%s seconds', $value, 'heartweb' ), $value );
					break;

				default:
					$times[$interval] = sprintf( _n( '%s year', '%s years', $value, 'heartweb' ), $value );
					break;
			}
			// echo $interval;
			$count++;
		}
	}
	// Return string with times
	return implode( ", ", $times );
}
// Usage:
// $t  = '2013-12-29T00:43:11+00:00';
// $t2 = '2013-11-24 19:53:04 +0100';
//
// var_dump( get_date_diff( $t, $t2, 1 ) ); // string '1 month' (length=7)
// var_dump( get_date_diff( $t, $t2, 2 ) ); // string '1 month, 4 days' (length=15)
// var_dump( get_date_diff( $t, $t2, 3 ) ); // string '1 month, 4 days, 5 hours' (length=24)


// add custom title formats
function heartweb_mce_formats( $init ) {

	$formats = array(
		'p'          => __( 'Paragraph' ),
		'h1'         => __( 'Heading 1' ),
		'h2'         => __( 'Heading 2' ),
		'h3'         => __( 'Heading 3' ),
		'h4'         => __( 'Heading 4' ),
		'h5'         => __( 'Heading 5' ),
		'h6'         => __( 'Heading 6' ),
		'pre'        => __( 'Preformatted' ),
		'slyadnev-ttl-1' => __( 'Masterclass block heading 1', 'heartweb' ),
		'slyadnev-ttl-2' => __( 'Masterclass block heading 2', 'heartweb' ),
		'slyadnev-ttl-3' => __( 'Masterclass block heading 3', 'heartweb' ),
		'slyadnev-ttl-4' => __( 'Masterclass block heading 4', 'heartweb' ),
		'slyadnev-ttl-5' => __( 'Masterclass block heading 5', 'heartweb' ),
		'slyadnev-ttl-6' => __( 'Masterclass block heading 6', 'heartweb' ),
	);

    // concat array elements to string
	array_walk( $formats, function ( $key, $val ) use ( &$block_formats ) {
		$block_formats .= esc_attr( $key ) . '=' . esc_attr( $val ) . ';';
	}, $block_formats = '' );

	$init['block_formats'] = $block_formats;

  $font_sizes=[];
  for ($i=8; $i < 72; $i++) {
    $font_sizes[]=$i.'px';
  }
  $init['fontsize_formats'] = implode(' ', $font_sizes);

	return $init;
}

function heartweb_tinymce_admin_script( $hook ) {
  global $options;

  wp_enqueue_script( 'tinymce-custom-config', $options['tpld'].'/assets/js/tinymce-custom-config.js', array(), '1.0' );
}

function heartweb_register_buttons( $buttons ) {
   array_push( $buttons, 'separator', 'heartwebVideo' );
   array_push( $buttons, 'separator', 'heartwebRegButton' );
   return $buttons;
}

function heartweb_register_tinymce_javascript( $plugin_array ) {
  global $options;

   $plugin_array['heartwebVideo'] = $options['tpld'].'/assets/js/tinymce-plugins/video-btn.js';
   $plugin_array['heartwebRegButton'] = $options['tpld'].'/assets/js/tinymce-plugins/regbtn-btn.js';
   return $plugin_array;
}

// validate form request
add_filter( 'hf_validate_form', function( $error_code, $form, $data ) {
	if( !empty($data['postId']) ) {
		global $formResponce;
		if (empty($data['reqFields'])) {
			$params=[
				'reqFields'=>'userName,email'
			];
			foreach ($data as $key => $val) {
				$params[$key]=$val;
			}
		} else {
			$params=$data;
		}
    $webinar=new Webinar($params);
		$formResponce=$webinar->checkRegistrationData($params);
		// return wp_send_json($responce);
		if (!$formResponce['check']) {
			$error_code = 'formInvalid';
		}
	}
	return $error_code;
}, 10, 3 );

add_filter( 'hf_form_message_formInvalid', function( $message ) {
	global $formResponce;
    return $formResponce['data'];
});

add_filter( 'hf_template_tags', function( $tags ) {
	// $tags['hb_name'] = (!empty($_GET['hb_name']))?$_GET['hb_name']:'';
	// $tags['hb_email'] = (!empty($_GET['hb_email']))?$_GET['hb_email']:'';
	// $tags['hb_phone'] = (!empty($_GET['hb_phone']))?$_GET['hb_phone']:'';
  $arr=['hb_name', 'hb_email', 'hb_phone'];

  foreach ($arr as $val) {
    $str='';
    switch (true) {
      case (!empty($_GET[$val])):
        $str=$_GET[$val];
        break;
      case (!empty($_GET['om_name'])):
        $str=$_GET['om_name'];
        break;
    }

    $tags[$val]=$str;
  }
	return $tags;
});



/*------------------------------------*\
    Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('cmb2_admin_init', 'generic_metaboxes' );
add_action('wp_enqueue_scripts', 'generic_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'generic_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'generic_styles'); // Add Theme Stylesheet
add_action('init', 'register_generic_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination
add_action('init', 'generic_globals');
add_action('template_redirect', 'generic_postmeta');
add_action('init', 'disable_wp_emojicons');
add_action('wp_footer', 'generic_footer_scripts');
// add_action( 'wp_ajax_nopriv_generic_news_like', 'generic_news_like' );
// add_action( 'wp_ajax_generic_news_like', 'generic_news_like' );
// add_action( 'wp_ajax_nopriv_generic_ajax_posts', 'generic_ajax_posts' );
// add_action( 'wp_ajax_generic_ajax_posts', 'generic_ajax_posts' );
// add_action( 'wp_ajax_nopriv_generic_ajax_cart', 'generic_ajax_cart' );
// add_action( 'wp_ajax_generic_ajax_cart', 'generic_ajax_cart' );
add_action( 'pre_get_posts', 'posts_per_page_func' );
// add_action( 'admin_action_generic_duplicate_post_as_draft', 'generic_duplicate_post_as_draft' );
// add_action( 'after_setup_theme', 'wds_setup_theme' );
add_action('after_setup_theme', 'generic_add_routes');
// add_action('plugins_loaded', 'sp_extend_HTML_Forms');
add_action( 'rest_api_init', function () {
	register_rest_route( 'heartweb/v1', '/webinarData', [
		'methods' => 'POST',
		'callback' => 'heartweb_webinarData',
	] );
} );
add_action( 'hf_form_success', 'heartweb_registerForWebinar' );

add_action( 'admin_enqueue_scripts', 'heartweb_tinymce_admin_script' );
// add_action( 'rest_api_init', function() {
//
// 	remove_filter( 'rest_pre_serve_request', 'rest_send_cors_headers' );
// 	add_filter( 'rest_pre_serve_request', function( $value ) {
// 		header( 'Access-Control-Allow-Origin: *' );
// 		header( 'Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE' );
// 		header( 'Access-Control-Allow-Credentials: true' );
//
// 		return $value;
//
// 	});
// }, 15 );

// page builder fields
// add_filter( 'wds_page_builder_fields_header', 'generic_header_fields' );
// add_filter( 'wds_page_builder_fields_video', 'generic_video_fields' );
// add_filter( 'wds_page_builder_fields_block-content', 'generic_block_cont_fields' );
// add_filter( 'wds_page_builder_fields_block-content-list', 'generic_block_list_fields' );
// add_filter( 'wds_page_builder_fields_parallax', 'generic_parallax_fields' );
// add_filter( 'wds_page_builder_fields_specialoffer', 'generic_specialoffer_fields' );
// add_filter( 'wds_page_builder_fields_block-content-wide', 'generic_block_cont_wide_fields' );
// add_filter( 'wds_page_builder_fields_block-content-fullwidth', 'generic_block_cont_fullwidth_fields' );
// add_filter( 'wds_page_builder_fields_author-fullwidth', 'generic_author_fullwidth_fields' );
// add_filter( 'wds_page_builder_fields_reviews', 'generic_reviews_fields' );

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'generic_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
// add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'generic_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('post_thumbnail_html', 'remove_width_attribute', 10 ); // Remove width and height dynamic attributes to post images
add_filter('image_send_to_editor', 'remove_width_attribute', 10 ); // Remove width and height dynamic attributes to post images
add_filter( 'script_loader_src', 'remove_cssjs_ver', 15, 1 );
add_filter( 'style_loader_src', 'remove_cssjs_ver', 15, 1 );
// add_filter( 'wpcf7_load_css', '__return_false' );
add_filter( 'emoji_svg_url', '__return_false' );
// add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
add_filter( 'posts_where', 'title_like_posts_where', 10, 2 );
// add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);
// add_filter( 'woocommerce_get_price_html', 'wpa83367_price_html', 100, 2 );
// add_filter( 'post_row_actions', 'generic_duplicate_post_link', 10, 2 );
// add_filter( 'page_row_actions', 'generic_duplicate_post_link', 10, 2 );
add_filter( 'the_content', 'heartweb_contentFilter' );
// tinymce
add_filter( 'tiny_mce_before_init', 'heartweb_mce_formats' );
// Load the TinyMCE plugin : editor_plugin.js (wp2.5)
add_filter( 'mce_external_plugins', 'heartweb_register_tinymce_javascript' );
// add new buttons
add_filter( 'mce_buttons', 'heartweb_register_buttons' );


// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes above would be nested like this -
// [generic_shortcode_demo] [generic_shortcode_demo_2] Here's the page title! [/generic_shortcode_demo_2] [/generic_shortcode_demo]
add_shortcode( 'hbvideo', 'heartweb_videoSC' );
add_shortcode( 'hbregbutton', 'heartweb_regbuttonSC' );

/*------------------------------------*\
    Custom Post Types
\*------------------------------------*/

function create_post_type_html5(){
	if ( file_exists( dirname( __FILE__ ) . '/assets/php/post-types.php' ) ) {
		require_once dirname( __FILE__ ) . '/assets/php/post-types.php';
	}
}
